﻿using ControlProperties;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ValuesConverter;
using ValuesCorrectLib;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void InitSave()
        {
            if (pLibrary.Mode == 3 || pLibrary.Mode == 4 || pLibrary.Mode == 6 || pLibrary.Mode == 5)
            {
                TableSave.mode = pLibrary.Mode;
                if (basicProperties.Local.Common.FileType == FileType.Word)
                    TableSave.AddToWord(null, true);
                else if (basicProperties.Local.Common.FileType == FileType.Txt)
                    TableSave.AddToTxt(null, true);
            }
        }

        private void VoiceListFormer(bool regim)
        {
            try
            {
                if (basicProperties.Local.Common.AutoReport == true)
                {
                    if (regim)
                    {
                        int ownId = -1;

                        for (int i = 0; i < lASP.Count; i++)
                        {
                            if (lASP[i].ISOwn == true)
                                ownId = lASP[i].Id;
                        }


                        int FrequencykHz = (int)(VoiceDisturb.Frequency * 1000);
                        byte DeviationCode = 0;
                        if (VoiceDisturb.Deviation == 1)
                        {
                            DeviationCode = 3;
                        }
                        else if (VoiceDisturb.Deviation == 2)
                        {
                            DeviationCode = 6;
                        }
                        else if (VoiceDisturb.Deviation == 3)
                        {
                            DeviationCode = 12;
                        }
                        else if (VoiceDisturb.Deviation == 4)
                        {
                            DeviationCode = 100;
                        }
                        var VoiceJammingMode = (VoiceDisturb.TakeFileIndex == 0) ? DspDataModel.RadioJam.VoiceJammingMode.FromFile : DspDataModel.RadioJam.VoiceJammingMode.RealtimeCapture;
                        string FilePath = (VoiceDisturb.FileName != null) ? VoiceDisturb.FileName : "—";
                        string[,] value = new string[2, 4];
                        if (basicProperties.Local.Common.Language == Languages.Rus)
                        {
                            value[0, 0] = "Частота, кГц";
                            value[0, 1] = "Девиация частоты, кГц";
                            value[0, 2] = "Режим";
                            value[0, 3] = "Путь";
                            if (VoiceJammingMode == 0)
                                value[1, 2] = "Из файла";
                            else
                                value[1, 2] = "Запись с микрофона";
                        }
                        else
                        {
                            value[0, 0] = "Frequency, kHz";
                            value[0, 1] = "Frequency deviation, kHz";
                            value[0, 2] = "Mode";
                            value[0, 3] = "Path";
                            if (VoiceJammingMode == 0)
                                value[1, 2] = "From file";
                            else
                                value[1, 2] = "Real time capture";
                        }

                        value[1, 0] = FrequencykHz.ToString();
                        value[1, 1] = DeviationCode.ToString();
                        value[1, 3] = FilePath;
                        if (basicProperties.Local.Common.FileType == FileType.Word)
                            TableSave.AddToWord(value, false);
                        else if (basicProperties.Local.Common.FileType == FileType.Txt)
                            TableSave.AddToTxt(value, false);

                    }
                    else
                    {
                        if (basicProperties.Local.Common.FileType == FileType.Word)
                            TableSave.AddToWord(null, false);
                        else if (basicProperties.Local.Common.FileType == FileType.Txt)
                            TableSave.AddToTxt(null, false);
                    }
                }
            }
            catch
            {

            }
        }

        private void ListFormer(List<TableSuppressFWS> listFWS, List<TableSuppressFHSS> listFHSS)
        {
            try
            {
                if (basicProperties.Local.Common.AutoReport == true)
                {
                    int ownId = -1;
                    List<TableSuppressFWS> tableFWS = new List<TableSuppressFWS>();
                    List<TableSuppressFHSS> tableFHSS = new List<TableSuppressFHSS>();

                    List<TableSuppressFWS> tableFWSOld = new List<TableSuppressFWS>();
                    List<TableSuppressFHSS> tableFHSSOld = new List<TableSuppressFHSS>();

                    if (listFWS != null)
                    {
                        if (tableFWSOld.SequenceEqual(listFWS) == true)
                            return;
                        else
                        {
                            tableFWSOld.Clear();
                            tableFWSOld.AddRange(listFWS);
                        }
                    }
                    if (listFHSS != null)
                    {
                        if (tableFHSSOld.SequenceEqual(listFHSS) == true)
                            return;
                        else
                        {
                            tableFHSSOld.Clear();
                            tableFHSSOld.AddRange(listFHSS);
                        }
                    }

                    for (int i = 0; i < lASP.Count; i++)
                    {
                        if (lASP[i].ISOwn == true)
                            ownId = lASP[i].Id;
                    }

                    if (listFWS != null)
                    {
                        for (int i = 0; i < listFWS.Count; i++)
                        {
                            if (listFWS[i].NumberASP == ownId)
                            {
                                tableFWS.Add(listFWS[i]);
                            }
                        }

                        tableFWS = (from u in tableFWS
                                    orderby u.FreqKHz
                                    select u).ToList();

                        int Rows = tableFWS.Count + 1;

                        string[,] Table = new string[Rows, 9];

                        Table[0, 0] = SHeaders.headerNumAJS;
                        Table[0, 1] = SHeaders.headerFreq;
                        Table[0, 2] = SHeaders.headerBearing;
                        Table[0, 3] = SHeaders.headerLetters;
                        Table[0, 4] = SHeaders.headerJammerParams;
                        Table[0, 5] = SHeaders.headerLevel;
                        Table[0, 6] = SHeaders.headerPriority;
                        Table[0, 7] = SHeaders.headerLatLon;
                        Table[0, 8] = SHeaders.headerAlt;

                        for (int i = 1; i < Rows; i++)
                        {
                            Table[i, 0] = tableFWS[i - 1].NumberASP.ToString();
                            Table[i, 1] = ((int)tableFWS[i - 1].FreqKHz).ToString("#,0.##########");
                            Table[i, 2] = tableFWS[i - 1].Bearing == -1 ? "—" : ((int)tableFWS[i - 1].Bearing).ToString();
                            Table[i, 3] = tableFWS[i - 1].Letter.ToString();
                            Table[i, 4] = ParamsNoiseConverter.ParamHindrance(tableFWS[i - 1].InterferenceParam.Modulation, tableFWS[i - 1].InterferenceParam.Deviation, tableFWS[i - 1].InterferenceParam.Manipulation, tableFWS[i - 1].InterferenceParam.Duration);
                            Table[i, 5] = tableFWS[i - 1].Threshold.ToString();
                            Table[i, 6] = tableFWS[i - 1].Priority.ToString();
                            string latitude = tableFWS[i - 1].Coordinates.Latitude == -1 ? "—" : Math.Round(tableFWS[i - 1].Coordinates.Latitude, 6).ToString();
                            string longitude = tableFWS[i - 1].Coordinates.Longitude == -1 ? "—" : Math.Round(tableFWS[i - 1].Coordinates.Longitude, 6).ToString();
                            if (latitude == "—" && longitude == "—")
                                Table[i, 7] = "—";
                            else
                                Table[i, 7] = latitude + " " + longitude;
                            Table[i, 8] = tableFWS[i - 1].Coordinates.Altitude == -1 ? "—" : ((int)tableFWS[i - 1].Coordinates.Altitude).ToString();
                        }
                        TableSave.Language = basicProperties.Local.Common.Language == Languages.Eng ? "ENG" : "RU";
                        if (basicProperties.Local.Common.FileType == FileType.Word)
                            TableSave.AddToWord(Table , false);
                        else if (basicProperties.Local.Common.FileType == FileType.Txt)
                            TableSave.AddToTxt(Table, false);
                    }
                    if (listFHSS != null)
                    {
                        for (int i = 0; i < listFHSS.Count; i++)
                        {
                            if (listFHSS[i].NumberASP == ownId)
                            {
                                tableFHSS.Add(listFHSS[i]);
                            }
                        }

                        tableFHSS = (from u in tableFHSS
                                     orderby u.FreqMinKHz
                                     select u).ToList();

                        int Rows = tableFHSS.Count + 1;

                        string[,] Table = new string[Rows, 9];

                        Table[0, 0] = SHeaders.headerNumAJS;
                        Table[0, 1] = SHeaders.headerFreqMin;
                        Table[0, 2] = SHeaders.headerFreqMax;
                        Table[0, 3] = SHeaders.headerCount;
                        Table[0, 4] = SHeaders.headerStep;
                        Table[0, 5] = SHeaders.headerLetters;
                        Table[0, 6] = SHeaders.headerLevel;
                        Table[0, 7] = SHeaders.headerJammerParams;
                        Table[0, 8] = SHeaders.headerSSR;

                        for (int i = 1; i < Rows; i++)
                        {
                            Table[i, 0] = tableFHSS[i - 1].NumberASP.ToString();
                            Table[i, 1] = ((int)tableFHSS[i - 1].FreqMinKHz).ToString("#,0.##########");
                            Table[i, 2] = ((int)tableFHSS[i - 1].FreqMaxKHz).ToString("#,0.##########");
                            Table[i, 3] = FreqCountConverter.FreqCount((int)tableFHSS[i - 1].FreqMinKHz, (int)tableFHSS[i - 1].FreqMaxKHz, tableFHSS[i - 1].StepKHz);
                            Table[i, 4] = tableFHSS[i - 1].StepKHz.ToString();
                            Table[i, 5] = LettersFHSSConverter.LettersFHSS(tableFHSS[i - 1].Letters);
                            Table[i, 6] = tableFHSS[i - 1].Threshold.ToString();
                            Table[i, 7] = ParamsNoiseConverter.ParamHindrance(tableFHSS[i - 1].InterferenceParam.Modulation, tableFHSS[i - 1].InterferenceParam.Deviation, tableFHSS[i - 1].InterferenceParam.Manipulation, tableFHSS[i - 1].InterferenceParam.Duration);
                            Table[i, 8] = EPOConverter.EPO(tableFHSS[i - 1].EPO);
                        }
                        TableSave.Language = basicProperties.Local.Common.Language == Languages.Eng ? "ENG" : "RU";
                        if (basicProperties.Local.Common.FileType == FileType.Word)
                            TableSave.AddToWord(Table, false);
                        else if (basicProperties.Local.Common.FileType == FileType.Txt)
                            TableSave.AddToTxt(Table, false);
                    }
                }
            }
            catch { }
        }
    }

    public class TableSave
    {
        public static int count = -1;
        public static int mode = 0;
        public static string Language = string.Empty;
        private static string tableStop = string.Empty;
        private static string tableIRI = string.Empty;
        private static List<string> RU = new List<string>() { "Старт", "Изменение таблицы", "Стоп", "ФРЧ", "ППРЧ", "АПРЧ", "РП ИРИ", "ИРИ РП", "РП", "Речеподобная помеха", "Загрузка программы" };
        private static List<string> ENG = new List<string>() { "Start", "Change Table", "Stop", "Jamming FWS", "Jamming FHSS", "Jamming AFRS", "Jamming Voice", "Load Program" };

        public static void Mode(int regim, byte format)
        {
            // Получаем текущий режим 3,4,6 (3 - ФРЧ, 4 - АПРЧ, 6 - ППРЧ, 5 - Голос)
            mode = regim;
            if (format == 1)
                AddToWord(null, false);
            else if (format == 0)
                AddToTxt(null, false);
        }

        public static void AddToWord(string[,] value, bool load)
        {
            string tableAction = string.Empty;
            string tableName = string.Empty;
            string path = Language == "ENG" ? $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Report\Jamming\" : $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Отчет\РП\";
            List<string> saveLanguage = Language == "ENG" ?  ENG : RU;

            // Присвоение названия таблицы 3 - ФРЧ, 4 - АПРЧ, 6 - ППРЧ
            if (mode == 3)
            {
                tableName = saveLanguage[3];
                tableStop = tableName;
            }
            else if (mode == 4)
            {
                tableName = saveLanguage[5];
                tableStop = tableName;
            }
            else if (mode == 5)
            {
                tableName = saveLanguage[9];
                tableStop = tableName;
            }
            else if (mode == 6)
            {
                tableName = saveLanguage[4];
                tableStop = tableName;
            }

            if (load)
            {
                count = 1;
                // Проверка на наличие папки
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                // Проверка на наличие файла с сегодняшней датой
                if (!(File.Exists(path + $@"{DateTime.Today.ToLongDateString()}.docx")))
                {
                    using (var document = DocX.Create(path + $@"{DateTime.Today.ToLongDateString()}"))
                    {
                        // Добавление заголовка и отступа после
                        document.InsertParagraph($"{DateTime.Now.ToString(@"HH:mm:ss")} {saveLanguage[10]} {saveLanguage[6]} {tableName}").FontSize(15).Font("Times New Roman").SpacingAfter(0).Alignment = Alignment.left;
                        document.InsertParagraph();
                        // Сохранение документа
                        document.Save();
                    }
                }
                else
                {
                    using (var document = DocX.Load(path + $@"{DateTime.Today.ToLongDateString()}.docx"))
                    {
                        // Добавление заголовка и отступа после
                        document.InsertParagraph($"{DateTime.Now.ToString(@"HH:mm:ss")} {saveLanguage[10]} {saveLanguage[6]} {tableName}").FontSize(15).Font("Times New Roman").SpacingAfter(0).Alignment = Alignment.left;
                        document.InsertParagraph();
                        // Сохранение документа
                        document.Save();
                    }
                }
                return;
            }

            // Проверяем на режим, в else проверяем выполнялся ли хоть раз наш цикл, если выполнялся(count==1), значит мы вышли из режима подавления. 
            if ((mode == 3 || mode == 4 || mode == 6 || mode == 5) && value != null)
            {
                // Если это первое попадание в if(count == 0), будет "старт", остальные разы будет "изменение"  
                tableAction = count == -1 ? saveLanguage[0] : saveLanguage[1];
                count = 1;

                if (tableAction == "Старт")
                    tableIRI = saveLanguage[6];
                else if (tableAction == "Изменение таблицы")
                    tableIRI = saveLanguage[7];

                
                try
                {
                    // Проверка на наличие папки
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    // Проверка на наличие файла с сегодняшней датой
                    if (!(File.Exists(path + $@"{DateTime.Today.ToLongDateString()}.docx")))
                    {
                        using (var document = DocX.Create(path + $@"{DateTime.Today.ToLongDateString()}"))
                        {
                            // Добавление заголовка и отступа после
                            document.InsertParagraph($"{DateTime.Now.ToString(@"HH:mm:ss")} {tableAction} {tableIRI} {tableName}").FontSize(15).Font("Times New Roman").SpacingAfter(0).Alignment = Alignment.left;
                            document.InsertParagraph();
                            //Вычисление сколько строк и столбцов будет в таблице
                            int rows = value.GetUpperBound(0) + 1;
                            int columns = value.Length / rows;
                            // Создание таблицы
                            var table = document.AddTable(rows, columns);
                            table.Design = TableDesign.TableGrid;
                            // Заполнение таблицы
                            for (int i = 0; i < rows; i++)
                            {
                                for (int j = 0; j < columns; j++)
                                {
                                    table.Rows[i].Cells[j].Paragraphs[0].Append(value[i, j]).Font("Times New Roman").Alignment = Alignment.center;
                                }
                            }
                            // Установка размеров таблицы автоматически
                            table.AutoFit = AutoFit.Contents;
                            // Добавление таблицы и отступа после
                            document.InsertTable(table);
                            document.InsertParagraph();
                            // Сохранение документа
                            document.Save();
                        }
                    }
                    else
                    {
                        using (var document = DocX.Load(path + $@"{DateTime.Today.ToLongDateString()}.docx"))
                        {
                            // Добавление заголовка и отступа после
                            document.InsertParagraph($"{DateTime.Now.ToString(@"HH:mm:ss")} {tableAction} {tableIRI} {tableName}").FontSize(15).Font("Times New Roman").SpacingAfter(0).Alignment = Alignment.left;
                            document.InsertParagraph();
                            //Вычисление сколько строк и столбцов будет в таблице
                            int rows = value.GetUpperBound(0) + 1;
                            int columns = value.Length / rows;
                            // Создание таблицы
                            var table = document.AddTable(rows, columns);
                            table.Design = TableDesign.TableGrid;
                            // Заполнение таблицы
                            for (int i = 0; i < rows; i++)
                            {
                                for (int j = 0; j < columns; j++)
                                {
                                    table.Rows[i].Cells[j].Paragraphs[0].Append(value[i, j]).Font("Times New Roman").Alignment = Alignment.center;
                                }
                            }

                            // Установка размеров таблицы автоматически
                            table.AutoFit = AutoFit.Contents;
                            // Добавление таблицы и отступа после
                            document.InsertTable(table);
                            document.InsertParagraph();
                            // Сохранение документа
                            document.Save();
                        }
                    }
                }
                catch { }
            }
            else if (count == 1)
            {
                tableAction = saveLanguage[2];
                count = -1;

                if (tableAction == "Стоп")
                    tableIRI = saveLanguage[8];

                try
                {
                    using (var document = DocX.Load(path + $@"{DateTime.Today.ToLongDateString()}.docx"))
                    {
                        document.InsertParagraph($"{DateTime.Now.ToString(@"HH:mm:ss")} {tableAction} {tableIRI} {tableStop}").FontSize(15).Font("Times New Roman").SpacingAfter(0).Alignment = Alignment.left;
                        document.InsertParagraph();
                        document.InsertParagraph("—————————————————————————————————————————").FontSize(11).Font("Times New Roman");
                        document.InsertParagraph();
                        document.Save();
                    }
                }
                catch
                {

                }
            }
            tableIRI = string.Empty;
        }
        
        public static void AddToTxt(string[,] value, bool load)
        {
            try
            {
                string tableAction = string.Empty;
                string tableName = string.Empty;
                string tableJ = string.Empty;
                string path = Language == "ENG" ? $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Report\Jamming\" : $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Отчет\РП\";
                List<string> saveLanguage = Language == "ENG" ? ENG : RU;

                // Присвоение названия таблицы 3 - ФРЧ, 4 - АПРЧ, 6 - ППРЧ
                if (mode == 3)
                {
                    tableName = saveLanguage[3];
                    tableStop = tableName;
                }
                else if (mode == 4)
                {
                    tableName = saveLanguage[5];
                    tableStop = tableName;
                }
                else if (mode == 5)
                {
                    tableName = saveLanguage[9];
                    tableStop = tableName;
                }
                else if (mode == 6)
                {
                    tableName = saveLanguage[4];
                    tableStop = tableName;
                }

                if(load)
                {
                    count = 1;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    using (StreamWriter wr = new StreamWriter(path + $@"{DateTime.Today.ToLongDateString()}.txt", true))
                    {
                        wr.WriteLine($"{DateTime.Now.ToString(@"HH:mm:ss")} {saveLanguage[10]} {saveLanguage[6]} {tableName}");
                        wr.WriteLine();
                    }
                    return;
                }

                // Проверяем на режим, в else проверяем выполнялся ли хоть раз наш цикл, если выполнялся(count==1), значит мы вышли из режима подавления. 
                if ((mode == 3 || mode == 4 || mode == 6 || mode == 5) && value != null)
                {
                    // Если это первое попадание в if(count == 0), будет "старт", остальные разы будет "изменение"  
                    tableAction = count == -1 ? saveLanguage[0] : saveLanguage[1];
                    count = 1;

                    if (tableAction == "Старт")
                        tableIRI = saveLanguage[6];
                    else if (tableAction == "Изменение таблицы")
                        tableIRI = saveLanguage[7];
                          
                    // Проверка на наличие папки
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    using (StreamWriter wr = new StreamWriter(path + $@"{DateTime.Today.ToLongDateString()}.txt", true))
                    { 
                        wr.WriteLine($"{DateTime.Now.ToString(@"HH:mm:ss")} {tableAction} {tableIRI} {tableName}");
                        wr.WriteLine();

                        List<int> maxLength = new List<int>();
                        int length = 0;

                        for (int i = 0; i < value.GetLength(1); i++)
                        {
                            for (int j = 0; j < value.GetLength(0); j++)
                            {
                                length = value[j, i].Length > length ? value[j, i].Length : length;
                            }
                            maxLength.Add(length);
                            length = 0;
                        }

                        for (int i = 0; i < value.GetLength(0); i++)
                        {
                            for (int j = 0; j < value.GetLength(1); j++)
                            {
                                value[i, j] = value[i, j].PadRight(maxLength[j]);
                                wr.Write(value[i, j] + " | ");
                            }
                            wr.WriteLine();
                        }
                        wr.WriteLine();
                    }
                }
                else if (count == 1)
                {
                    tableAction = saveLanguage[2];
                    count = -1;

                    if (tableAction == "Стоп")
                        tableIRI = saveLanguage[8];

                    using (StreamWriter wr = new StreamWriter(path + $@"{DateTime.Today.ToLongDateString()}.txt", true))
                    {
                        wr.WriteLine($"{DateTime.Now.ToString(@"HH:mm:ss")} {tableAction} {tableIRI} {tableStop}");
                        wr.WriteLine();
                        wr.WriteLine("——————————————————————————————————————————————————————————————————————————————————————————————————————————");
                        wr.WriteLine();
                    }
                }
                tableIRI = string.Empty;
            }
            catch { }
        }
    }
}
