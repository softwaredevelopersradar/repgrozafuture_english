﻿using AwsToConsoleTransferLib;
using BPSS_DLL;
using SpoofingControl;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using UsrpLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private SpoofGenerator _spoofGen;
        public bool IsSpoofConnected { get; set; } = false;

        #region Commands
        public bool SpoofManagerConnect(int generatorPort, string generatorIp)
        {
            _spoofGen = new SpoofGenerator(generatorPort, generatorIp, 2, 3);

            if (IsSpoofConnected)
                return true;
            SubscribeToLibEvents();

            return _spoofGen.Connect(generatorPort, generatorIp);
        }

        public void SpoofManagerDisconnect()
        {
            if (!IsSpoofConnected)
                return;
            
            _spoofGen.Disconnect();
        }


        public void GetGeneratorState()
        {
            if (!IsSpoofConnected)
                return;

            _spoofGen.GetGeneratorState();
        }

        //public void SetParams(CoordinatesReal data) //после отправки, при error = 0 в ответе, автоматом отправит SpoofingOn
        //{
        //    if (!IsSpoofConnected)
        //        return;

        //    _spoofGen.SetParams(data);

        //}

        public void SetCoordinates(CoordinatesParameters data)
        {
            if (!IsSpoofConnected)
                return;

            _spoofGen.SetStartCoord(data);
        }

        public void SetControlParams(ControlParameters data)
        {
            if (!IsSpoofConnected)
                return;
            _spoofGen.SetControlParams(data);
        }

        public void SpoofingOff()
        {
            if (!IsSpoofConnected)
                return;

            isSpoofOn = false;

            _spoofGen.SpoofingOff();
        }


        public void SpoofingOn()
        {
            if (!IsSpoofConnected)
                return;

            isSpoofOn = true;

            _spoofGen.SpoofingOn();
        }

        public void GetParams()
        {
            if (!IsSpoofConnected)
                return;

            _spoofGen.GetParams();
        }

        public void GetFakeCoords()
        {
            if (!IsSpoofConnected)
                return;

            _spoofGen.GetFakeCoords();
        }
        #endregion


        public void SubscribeToLibEvents()
        {
            _spoofGen.ConnectNet += OnConnected;
            _spoofGen.DisconnectNet += OnDisconnected;
            _spoofGen.ConnectionFailed += OnConnectionFailed;
            _spoofGen.LostGenerator += OnLostGenerator;
            _spoofGen.SendCmd += OnSendCmd;
            _spoofGen.ReplyCommand += OnReplyCommand;
            //_spoofGen.ReplyTrueCoords += OnReplyTrueCoords;
            //_spoofGen.ReplyFakeCoords += OnReplyFakeCoords;
            _spoofGen.ReplySpoofModeDetailed += this._spoofGen_ReplySpoofModeDetailed;
            _spoofGen.ReplySimulatedSats += _spoofGen_ReplySimulatedSats;
            _spoofGen.ReplySpoofMode += OnReplySpoofMode;
            _spoofGen.ReplyTrueCoords += _spoofGen_ReplyTrueCoords;
            _spoofGen.ReplyFakeCoords += _spoofGen_ReplyFakeCoords;
            
        }

        private void _spoofGen_ReplyFakeCoords(object sender, ParamsFakeEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
            });

            DispatchIfNecessary(() =>
            {
                spoof.UpdateFakeCoordinates(e.Coords.LatitudeDeg, e.Coords.LongitudeDeg);
            });

            //if (spoofingWindow.Mode != EStatus.Stop)
            //{
            //    SimulationParam simulationParam = ConvertToSimulatationParam((float)e.Coords.LatitudeDeg, (float)e.Coords.LongitudeDeg,
            //                                                                 e.Coords.HeadingDeg, e.Coords.Speed,
            //                                                                 e.Coords.Elevation, 0);



            //    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //    {
            //        spoofingWindow.SimulationParamOutput = simulationParam;
            //    });

            //    trackSpoofingDrone.Coordinate.Add(new Coord()
            //    {
            //        Latitude = simulationParam.Coordinate.Latitude,
            //        Longitude = simulationParam.Coordinate.Longitude

            //    });

            //    trackSpoofingDrone.Angle = simulationParam.Angle;
            //    UpdateSpoofingTrack(trackSpoofingDrone);
            //}
        }

        private void _spoofGen_ReplyTrueCoords(object sender, ParamsRealEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
            });

            //SimulationParam simulationParam = ConvertToSimulatationParam((float)e.Coords.LatitudeDeg, (float)e.Coords.LongitudeDeg,
            //                                                             e.Coords.HeadingDeg, e.Coords.Speed,
            //                                                             e.Coords.Elevation, CodeRangeBack(e.Coords.RangeGate));


            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    spoofingWindow.SimulationParamInput = simulationParam;
            //});
        }

        private void _spoofGen_ReplySimulatedSats(object sender, SimulatedSatsEventArgs e)
        {
            //string sat = GetStringSatelites(e.Satellites);

            //switch (e.Type)
            //{
            //    case CodesGnssTypes.GPS:
            //        sat = sat.Replace('T', 'G');
            //        spoofingWindow.SatelitesGPS = "";
            //        spoofingWindow.SatelitesGPS = sat;
            //        break;

            //    case CodesGnssTypes.GLONASS:
            //        sat = sat.Replace('T', 'R');
            //        spoofingWindow.SatelitesGLONASS = "";
            //        spoofingWindow.SatelitesGLONASS = sat;
            //        break;

            //    default:
            //        break;
            //}
        }

        private void _spoofGen_ReplySpoofModeDetailed(object sender, DetailedStateReplyEventArgs e)
        {
            //_spoofGen.Mode = (Modes.)e.State;


            //string sat = GetStringSatelites(e.Satellites);
            //sat = sat.Replace('T', 'G');

            //spoofingWindow.SatelitesGPS = "";
            //spoofingWindow.SatelitesGPS = (spoofingWindow.Mode == Modes.STOP) ? "" : sat;


            //AnalizeSpoofMode();
        }


        public void UnsubscribeFromLibEvents()
        {
            _spoofGen.ConnectNet -= OnConnected;
            _spoofGen.DisconnectNet -= OnDisconnected;
            _spoofGen.ConnectionFailed -= OnConnectionFailed;
            _spoofGen.LostGenerator -= OnLostGenerator;
            _spoofGen.SendCmd -= OnSendCmd;
            _spoofGen.ReplyCommand -= OnReplyCommand;
            //_spoofGen.ReplyTrueCoords -= OnReplyTrueCoords;
            //_spoofGen.ReplyFakeCoords -= OnReplyFakeCoords;
            _spoofGen.ReplySpoofModeDetailed -= _spoofGen_ReplySpoofModeDetailed;
            _spoofGen.ReplySimulatedSats -= _spoofGen_ReplySimulatedSats;
            _spoofGen.ReplySpoofMode -= OnReplySpoofMode;
            _spoofGen.ReplyTrueCoords -= _spoofGen_ReplyTrueCoords;
            _spoofGen.ReplyFakeCoords -= _spoofGen_ReplyFakeCoords;
        }

        private string GetStringSatelites(List<byte> Satellites)
        {
            string sat = "";

            if (Satellites != null && Satellites.Count > 0)
            {
                foreach (var s in Satellites)
                    sat += "T" + s.ToString() + "  ";
            }

            return sat;

        }


        private void AnalizeSpoofMode()
        {          

            //AnalizeMode(spoofingWindow.Mode);

            //StopGenerationClear();
        }

        private void StopGenerationClear()
        {
            //if (spoofingWindow.Mode == EStatus.Stop)
            //{
            //    trackSpoofingDrone.Coordinate.Clear();

            //    trackSpoofingDrone.Angle = -1;
            //    UpdateSpoofingTrack(trackSpoofingDrone);
            //}
        }

        //private void AnalizeMode(EStatus mode)
        //{
        //    switch (mode)
        //    {
        //        case EStatus.Stop:
        //            DestructorTimerSpoofing();

        //            break;

        //        case EStatus.Synchronization:
        //            break;


        //        case EStatus.Generation:
        //            InitializeTimerSpoofing();


        //            break;

        //        default:
        //            break;

        //    }
        //}

        //private SimulationParam ConvertToSimulatationParam(float latitude, float longitude,
        //                                                   short angle, float speed,
        //                                                   short elevation, byte range)
        //{
        //    SimulationParam simulationParam = new SimulationParam();
        //    try
        //    {
        //        simulationParam.Coordinate.Latitude = latitude;
        //        simulationParam.Coordinate.Longitude = longitude;

        //        simulationParam.Angle = angle;
        //        simulationParam.Speed = speed;
        //        simulationParam.Elevation = elevation;

        //        simulationParam.Range = range;
        //    }
        //    catch { }

        //    return simulationParam;
        //}

        #region EventHandlers

        private void OnConnected(object sender, EventArgs e)
        {
            IsSpoofConnected = true;
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowConnect();
            });
        }

        private bool isSpoofOn = false;
        public int PoolingTimer { get; set; } = 1000;

        private async Task SpoofPoolingAsync()
        {
            while (IsSpoofConnected && isSpoofOn)
            {
                GetFakeCoords();
                await Task.Delay(1000);
                GetGeneratorState();
                await Task.Delay(PoolingTimer);
            }
        }

        private void OnDisconnected(object sender, EventArgs e)
        {
            IsSpoofConnected = false;
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowDisconnect();
            });
            UnsubscribeFromLibEvents();
        }


        private void OnConnectionFailed(object sender, EventArgs e)
        {
            IsSpoofConnected = false;
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowDisconnect();
            });
            UnsubscribeFromLibEvents();
        }


        private void OnLostGenerator(object sender, EventArgs e)
        {
            //OnMessToMain(sender, "USRP server is lost");
        }


        private void OnSendCmd(object sender, byte code)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowWrite();
            });
        }


        private void OnReplySpoofMode(object sender, StateReplyEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
                AnalizeSpoofMode();
            });
            //OnMessToMain(sender, "Current mode: " + ((Modes)e.State).ToString());
            //OnSpoofMode(sender, args);  //КИДАТЬ ИВЕНТ ДЛЯ СЕРВЕРА, А ТОТ КОНТРОЛУ СПУФИНГА ПОШЛЕТ ДАННЫЕ
        }

        /*

        private void OnReplyTrueCoords(object sender, CoordReplyEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
            });
            //OnMessToMain(sender, $"Get request on " + (CodesUsrp)e.Code + ", " +
            //        $" LatitudeDeg : {e.Coords.LatitudeDeg}," +
            //        $" LongitudeDeg : {e.Coords.LongitudeDeg}," +
            //        $" Elevation : {e.Coords.Elevation}," +
            //        $" Speed : {e.Coords.Speed}," +
            //        $" HeadingDeg : {e.Coords.HeadingDeg}," +
            //        $" RangeGate : {e.Coords.RangeGate},"
            //        );
            //OnTrueParams(sender, args);  //КИДАТЬ ИВЕНТ ДЛЯ СЕРВЕРА, А ТОТ КОНТРОЛУ СПУФИНГА ПОШЛЕТ ДАННЫЕ
        }

        private void OnReplyFakeCoords(object sender, CoordReplyFakeEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
            });
            //OnMessToMain(sender, $"Get request on " + (CodesUsrp)e.Code + ", " +
            //        $" LatitudeDeg : {e.Coords.LatitudeDeg}," +
            //        $" LongitudeDeg : {e.Coords.LongitudeDeg}," +
            //        $" Elevation : {e.Coords.Elevation}," +
            //        $" Speed : {e.Coords.Speed}," +
            //        $" HeadingDeg : {e.Coords.HeadingDeg}"
            //        );


            DispatchIfNecessary(() =>
            {
                spoof.State1 = new StateModel1()
                {
                    Latitude = e.Coords.LatitudeDeg,
                    Longitude = e.Coords.LongitudeDeg,
                    Time = DateTime.Now
                };
            });

            
        }

        */

        private void OnReplyCommand(object sender, BaseReplyEventArgs e)
        {

            DispatchIfNecessary(() =>
            {
                SpoofingControlConnection.ShowRead();
            });

            //OnMessToMain(sender, "Get reply on " + e.Code + ", Error = " + e.Error); //TODO: Убрать Error, когда появится анализ ошибок

            //if (e.Code == (byte)CodesUsrp.SPOOF_ON && e.Error == 0)
            //{
            //    OnMessToMain(sender, "Starting spoofing... ");
            //}
        }
        #endregion
    }

}
