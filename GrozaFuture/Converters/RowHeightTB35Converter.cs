﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace GrozaFuture
{
    public class RowHeightTB35Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((byte)value)
            {
                case 1:
                    return new GridLength(0);
                case 2:
                    return new GridLength(35);
                default:
                    return new GridLength(35);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var gl = (GridLength)value;
            if (gl.Value == 35) return (byte)2;
            return (byte)1;
        }
    }
}
