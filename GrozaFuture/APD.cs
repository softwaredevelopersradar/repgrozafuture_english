﻿using APDTransfer;
using DataTransferModel.DataTransfer;
using GeoCalculator;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using TableOperations;
using UserControl_Chat;

using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        APDTransferClass transferClass = new APDTransferClass();

        private bool _isADPConnected = false;
        private string _ActiveAPDCOM = "COM0";

        private void InitAPD(int ComNumber)
        {
            transferClass.InitRole(APDTransferClass.WhoIsWho.Jammer);
            transferClass.OpenCOM(Convert.ToByte(ComNumber));

            Events.OnSendStationsMessage += Events_OnSendStationsMessage;
        }

        public byte EjectByteNumberFromCOMString(string ComNumber)
        {
            byte result = 0;

            if (ComNumber.Length > 3)
            {
                string s = ComNumber.Substring(3, ComNumber.Length - 3);
                Byte.TryParse(s, out result);
            }

            return result;
        }

        private void InitAPD(string ComNumber, bool isManual = false)
        {
            if (isManual)
            {
                transferClass.InitRole(APDTransferClass.WhoIsWho.Jammer);

                _ActiveAPDCOM = ComNumber;

                var comNumber = EjectByteNumberFromCOMString(ComNumber);

                if (comNumber > 0)
                {
                    InitAPDConnectionEvents();

                    transferClass.OpenCOM(comNumber);

                    _isADPConnected = true;

                    InitSpecificEventsForAPD();
                    InitAPDEvents();
                }
            }
        }

        private void DeInitAPD()
        {
            _ActiveAPDCOM = "COM0";

            CloseAPD();

            _isADPConnected = false;

            DeInitAPDEvents();
            DeInitSpecificEventsForAPD();
        }

        private void ReInitAPD(LocalProperties arg, bool isManual = false)
        {
            if (arg.PCProperties.IsGarantVisible)
            {
                if (arg.PCProperties.State == true)
                {
                    if (_isADPConnected == true)
                    {
                        if (_ActiveAPDCOM != arg.PCProperties.ComPortAPD)
                        {
                            DeInitAPD();
                            InitAPD(arg.PCProperties.ComPortAPD, isManual);
                        }
                        if (isManual)
                            DeInitAPD();
                    }
                    else
                    {
                        InitAPD(arg.PCProperties.ComPortAPD, isManual);
                    }
                }
                else
                {
                    if (_isADPConnected == true)
                    {
                        DeInitAPD();
                    }
                    else
                    {

                    }
                }
            }
        }

        private void CloseAPD()
        {
            transferClass.CloseCOM();
        }

        private void InitAPDEvents()
        {
            transferClass.RequestDataEvent += TransferClass_RequestDataEvent;
            transferClass.SynchronizeEvent += TransferClass_SynchronizeEvent;
            transferClass.TextMessageEvent += TransferClass_TextMessageEvent;
            transferClass.RegimeWorkEvent += TransferClass_RegimeWorkEvent;
            transferClass.ForbidRangeFreqEvent += TransferClass_ForbidRangeFreqEvent;
            transferClass.ReconSectorRangeEvent += TransferClass_ReconSectorRangeEvent;
            transferClass.SupressSectorRangeEvent += TransferClass_SupressSectorRangeEvent;
            transferClass.SupressFWSEvent += TransferClass_SupressFWSEvent;
            transferClass.SupressFHSSEvent += TransferClass_SupressFHSSEvent;
            transferClass.RequestBearingEvent += TransferClass_RequestBearingEvent;


            transferClass.TextMessageJEvent += TransferClass_TextMessageJEvent;
            transferClass.ReceptionEvent += TransferClass_ReceptionEvent;
            transferClass.CoordJammerEvent += TransferClass_CoordJammerEvent;
            transferClass.DataFWS1Event += TransferClass_DataFWS1Event;
            transferClass.DataFWS2Event += TransferClass_DataFWS2Event;
            transferClass.DataFHSS1Event += TransferClass_DataFHSS1Event;
            transferClass.DataFHSS2Event += TransferClass_DataFHSS2Event;
            transferClass.StateSupressFreqEvent += TransferClass_StateSupressFreqEvent;
            transferClass.ExecuteBearingEvent += TransferClass_ExecuteBearingEvent;
            transferClass.TSimultanBearingEvent += TransferClass_TSimultanBearingEvent;
            transferClass.TestChannelEvent += TransferClass_TestChannelEvent;
        }

        private void InitAPDConnectionEvents()
        {
            transferClass.COMisOpen += TransferClass_COMisOpen;
            transferClass.OnWriteByte += TransferClass_OnWriteByte;
            transferClass.OnReadByte += TransferClass_OnReadByte;
        }

        private void InitSpecificEventsForAPD()
        { 
            Events.OnSendStationsMessage += Events_OnSendStationsMessage;
        }

        private void DeInitAPDEvents()
        {
            DeInitAPDConnectionEvents();

            transferClass.RequestDataEvent -= TransferClass_RequestDataEvent;
            transferClass.SynchronizeEvent -= TransferClass_SynchronizeEvent;
            transferClass.TextMessageEvent -= TransferClass_TextMessageEvent;
            transferClass.RegimeWorkEvent -= TransferClass_RegimeWorkEvent;
            transferClass.ForbidRangeFreqEvent -= TransferClass_ForbidRangeFreqEvent;
            transferClass.ReconSectorRangeEvent -= TransferClass_ReconSectorRangeEvent;
            transferClass.SupressSectorRangeEvent -= TransferClass_SupressSectorRangeEvent;
            transferClass.SupressFWSEvent -= TransferClass_SupressFWSEvent;
            transferClass.SupressFHSSEvent -= TransferClass_SupressFHSSEvent;
            transferClass.RequestBearingEvent -= TransferClass_RequestBearingEvent;


            transferClass.TextMessageJEvent -= TransferClass_TextMessageJEvent;
            transferClass.ReceptionEvent -= TransferClass_ReceptionEvent;
            transferClass.CoordJammerEvent -= TransferClass_CoordJammerEvent;
            transferClass.DataFWS1Event -= TransferClass_DataFWS1Event;
            transferClass.DataFWS2Event -= TransferClass_DataFWS2Event;
            transferClass.DataFHSS1Event -= TransferClass_DataFHSS1Event;
            transferClass.DataFHSS2Event -= TransferClass_DataFHSS2Event;
            transferClass.StateSupressFreqEvent -= TransferClass_StateSupressFreqEvent;
            transferClass.ExecuteBearingEvent -= TransferClass_ExecuteBearingEvent;
            transferClass.TSimultanBearingEvent -= TransferClass_TSimultanBearingEvent;
            transferClass.TestChannelEvent -= TransferClass_TestChannelEvent;
        }

        private void DeInitAPDConnectionEvents()
        {
            transferClass.COMisOpen -= TransferClass_COMisOpen;
            transferClass.OnWriteByte -= TransferClass_OnWriteByte;
            transferClass.OnReadByte -= TransferClass_OnReadByte;
        }

        private void DeInitSpecificEventsForAPD()
        {
            Events.OnSendStationsMessage -= Events_OnSendStationsMessage;
        }

        private void TransferClass_COMisOpen(bool isOpen)
        {
            DispatchIfNecessary(() =>
            {
                if (isOpen)
                    PCControlConnection.ShowConnect();
                else
                    PCControlConnection.ShowDisconnect();
            });
        }

        private void TransferClass_OnWriteByte()
        {
            if (PCControlConnection.LedTransVisible)
                DispatchIfNecessary(() =>
                {
                    PCControlConnection.ShowWrite();
                });
        }

        private void TransferClass_OnReadByte()
        {
            if (PCControlConnection.LedTransVisible)
                DispatchIfNecessary(() =>
                {
                    PCControlConnection.ShowRead();
                });
        }

        private void TransferClass_RequestDataEvent(APDTransferClass.TRequestData RequestDataRead)
        {
            switch (RequestDataRead.bSign)
            {
                case 0:
                    {
                        // АСП lASP
                        var index = 0;
                        for (int i = 0; i < lASP.Count; i++)
                        {
                            if (lASP[i].ISOwn == true)
                            {
                                index = i;
                            }
                            break;
                        }

                        int LatitudeDegree = 0;
                        int LatitudeMinutes = 0;
                        double LatitudeSeconds = 0;

                        ClassGeoCalculator.f_Grad_GMS(lASP[index].Coordinates.Latitude, ref LatitudeDegree, ref LatitudeMinutes, ref LatitudeSeconds);

                        int LongitudeDegree = 0;
                        int LongitudeMinutes = 0;
                        double LongitudeSeconds = 0;

                        ClassGeoCalculator.f_Grad_GMS(lASP[index].Coordinates.Longitude, ref LongitudeDegree, ref LongitudeMinutes, ref LongitudeSeconds);

                        transferClass.SendJStationCoordinates(1,
                        0, 0,
                           (byte)LatitudeDegree, (byte)LatitudeMinutes, (byte)LatitudeSeconds,
                            (byte)LongitudeDegree, (byte)LongitudeMinutes, (byte)LongitudeSeconds);

                    }
                    break;
                case 1:
                    {
                        //Информация об ИРИ ФЧ
                        // ИРИ ФРЧ ЦР    lReconFWS
                        for (int i = 0; i < lReconFWS.Count; i++)
                        {
                            APDTransferClass.TDataFWSPartOne p1 = new APDTransferClass.TDataFWSPartOne()
                            {
                                bTempSource = (byte)i,     // номер текущего ИРИ
                                iFreq = (int)(lReconFWS[i].FreqKHz * 10),           // частота           
                                bCodeType = lReconFWS[i].Type,       // код вида            
                                bCodeWidth = transferClass.ConvertFromSignalWidthToWidthCode(lReconFWS[i].Deviation),      // код ширины
                                bHour = (byte)lReconFWS[i].Time.Hour,           // часы
                                bMin = (byte)lReconFWS[i].Time.Minute,            // минуты
                                bSec = (byte)lReconFWS[i].Time.Second,            // секунды
                                bCodeRate = 0,       // скорость ТЛГ
                            };

                            int LatitudeDegree = 0;
                            int LatitudeMinutes = 0;
                            double LatitudeSeconds = 0;

                           ClassGeoCalculator.f_Grad_GMS(lReconFWS[i].Coordinates.Latitude, ref LatitudeDegree, ref LatitudeMinutes, ref LatitudeSeconds);

                            int LongitudeDegree = 0;
                            int LongitudeMinutes = 0;
                            double LongitudeSeconds = 0;

                           ClassGeoCalculator.f_Grad_GMS(lReconFWS[i].Coordinates.Longitude, ref LongitudeDegree, ref LongitudeMinutes, ref LongitudeSeconds);

                            ushort BearAd = 0;
                            try
                            {
                                BearAd = (ushort)lReconFWS[i].ListJamDirect[1].JamDirect.Bearing;
                            }
                            catch { }
                            APDTransferClass.TDataFWSPartTwo p2 = new APDTransferClass.TDataFWSPartTwo()
                            {
                                bTempSource = (byte)i,      // номер текущего ИРИ
                                bSignLong = 0,        // знак долготы           
                                bLongDegree = (byte)LongitudeDegree,     // градусы долготы
                                bLongMinute = (byte)LongitudeMinutes,      // минуты долготы
                                bLongSecond = (byte)LongitudeSeconds,      // секунды долготы
                                bSignLat = 0,         // знак широты
                                bLatDegree = (byte)LatitudeDegree,       // градусы широты
                                bLatMinute = (byte)LatitudeMinutes,       // минуты широты
                                bLatSecond = (byte)LatitudeSeconds,       // секунды широты
                                wBearMain = (ushort)lReconFWS[i].ListJamDirect[0].JamDirect.Bearing,        // пеленг от ведущей
                                wBearAdd = BearAd,         // пеленг от ведомой
                            };

                            transferClass.SendJFRSInformation(1, p1, p2);
                        }

                        //clientDB.Tables[NameTable.TableReconFWS].Clear();
                    }
                    break;
                case 2:
                    {
                        //Информация об ИРИ ППРЧ
                        // ИРИ ФРЧ РП  lReconFHSS 

                        for (int i = 0; i < lReconFHSS.Count(); i++)
                        {
                            APDTransferClass.TDataFHSSPartOne p3 = new APDTransferClass.TDataFHSSPartOne()
                            {
                                bTempSource = (byte)i,     // номер текущего ИРИ
                                iFreqBegin = (int)(lReconFHSS[i].FreqMinKHz * 10),      // начальная частота
                                iFreqEnd = (int)(lReconFHSS[i].FreqMaxKHz * 10),        // конечная частота
                                bHour = (byte)(lReconFHSS[i].Time.Hour),           // часы
                                bMin = (byte)(lReconFHSS[i].Time.Minute),            // минуты
                                bSec = (byte)(lReconFHSS[i].Time.Second),            // секунды
                                bCodeWidth = transferClass.ConvertFromFHSSWidthToWidthCode(lReconFHSS[i].Deviation),      // код ширины
                                bCodeStep = transferClass.ConvertFromFHSSStepToStepCode(lReconFHSS[i].StepKHz),      // код шага
                            };

                            transferClass.ConvertFromFHSSDurationmstoCode(15.0f, out byte bGroupDuration, out byte bDuration);

                            TableSourceFHSS tempTableSourceFHSS = new TableSourceFHSS();

                            for (int j = 0; j < lSourceFHSS.Count(); j++)
                            {
                                if (lSourceFHSS[j].Id == lReconFHSS[i].Id)
                                {
                                    tempTableSourceFHSS = lSourceFHSS[j];
                                    break;
                                }
                            }

                            int LatitudeDegree = 0;
                            int LatitudeMinutes = 0;
                            double LatitudeSeconds = 0;

                            ClassGeoCalculator.f_Grad_GMS(tempTableSourceFHSS.Coordinates.Latitude, ref LatitudeDegree, ref LatitudeMinutes, ref LatitudeSeconds);

                            int LongitudeDegree = 0;
                            int LongitudeMinutes = 0;
                            double LongitudeSeconds = 0;

                            ClassGeoCalculator.f_Grad_GMS(tempTableSourceFHSS.Coordinates.Longitude, ref LongitudeDegree, ref LongitudeMinutes, ref LongitudeSeconds);

                            APDTransferClass.TDataFHSSPartTwo p4 = new APDTransferClass.TDataFHSSPartTwo()
                            {
                                bTempSource = (byte)i,     // номер текущего ИРИ

                                bGroupDuration = bGroupDuration,  // длительность
                                bDuration = bDuration,       // длительность
                                wBearMain = (byte)tempTableSourceFHSS.ListJamDirect[0].JamDirect.Bearing,       // пеленг от ведущей
                                wBearAdd = (byte)tempTableSourceFHSS.ListJamDirect[1].JamDirect.Bearing,        // пеленг от ведомой
                                bSignLong = 0,       // знак долготы           
                                bLongDegree = (byte)LongitudeDegree,     // градусы долготы
                                bLongMinute = (byte)LongitudeMinutes,     // минуты долготы
                                bLongSecond = (byte)LongitudeSeconds,     // секунды долготы
                                bSignLat = 0,        // знак широты
                                bLatDegree = (byte)LatitudeDegree,      // градусы широты
                                bLatMinute = (byte)LatitudeMinutes,      // минуты широты
                                bLatSecond = (byte)LatitudeSeconds,      // секунды широты   
                                bCodeType = lReconFHSS[i].Modulation,       // код вида модуляции     
                            };

                            transferClass.SendJFHSSInformation(1, p3, p4);
                        }
                    }
                    break;
                case 3:
                    {
                        //Квитанция о подавляемых частотах lSuppressFWS

                        List<List<TableSuppressFWS>> LL = new List<List<TableSuppressFWS>>();

                        for (int i = 0; i < 7; i++)
                        {
                            List<TableSuppressFWS> l = new List<TableSuppressFWS>();
                            LL.Add(l);
                        }

                        for (int i = 0; i < lSuppressFWS.Count; i++)
                        {
                            LL[(int)lSuppressFWS[i].Letter-1].Add(lSuppressFWS[i]);
                        }

                        var lTempSuppressFWS = LoadTempSuppressFWS().Result;

                        for (int i = 0; i < LL.Count; i++)
                        {
                            APDTransferClass.TStateSupressFreq p = new APDTransferClass.TStateSupressFreq();
                            p.Init();
                            p.bTempSubRange = (byte)(i + 1);   // номер поддиапазона
                            p.bSighF = 0;          // признак ФЧ/ППРЧ
                            p.bStateFHSS = 0;      // инф-ция ППРЧ
                            for (int j = 0; j < LL[i].Count(); j++)
                            {
                                int id = LL[i][j].Id;

                                TempSuppressFWS tempSuppressFWS = new TempSuppressFWS();
                                for (int w = 0; w < lTempSuppressFWS.Count; w++)
                                {
                                    if (lTempSuppressFWS[w].Id == id)
                                    {
                                        tempSuppressFWS = lTempSuppressFWS[w];
                                        break;
                                    }
                                }

                                p.bStateWork[j] = (byte)(tempSuppressFWS.Suppress);   // инф-ция о работе    
                                p.bStateLongTerm[j] = (byte)(tempSuppressFWS.Control);  // инф-ция о долговременности           
                                p.bStateSupress[j] = (byte)(tempSuppressFWS.Radiation);   // инф-ция о подвлении  
                            }
                            if (LL[i].Count() !=0)
                                transferClass.SendJReceiptJammingFrequencies(1, p);
                        }

                        //APDTransferClass.TStateSupressFreq p = new APDTransferClass.TStateSupressFreq();
                        //p.Init();

                        //p.bStateWork;   // инф-ция о работе    
                        //p.bStateLongTerm;  // инф-ция о долговременности           
                        //p.bStateSupress;   // инф-ция о подвлении           
                        //p.bTempSubRange = 1;   // номер поддиапазона
                        //p.bSighF = 0;          // признак ФЧ/ППРЧ
                        //p.bStateFHSS = 0;      // инф-ция ППРЧ

                        //transferClass.SendJReceiptJammingFrequencies(1, p);
                    }
                    break;
                default:
                    break;
            }
        }

        private async Task<List<TempSuppressFWS>> LoadTempSuppressFWS()
        {
             var tempSuppressFWS = await clientDB.Tables[NameTable.TempSuppressFWS].LoadAsync<TempSuppressFWS>();
             return tempSuppressFWS;
        }


        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetSystemTime([In] ref SYSTEMTIME st);

        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }

        private void TransferClass_SynchronizeEvent(APDTransferClass.TSynchronize SynchronizeRead)
        {
            var shift = DateTime.Now - DateTime.UtcNow;
            var hour = shift.Hours;
            var minute = shift.Minutes;
            SYSTEMTIME st = new SYSTEMTIME();
            st.wYear = Convert.ToInt16(DateTime.UtcNow.Year);
            st.wMonth = Convert.ToInt16(DateTime.UtcNow.Month);
            st.wDay = Convert.ToInt16(DateTime.UtcNow.Day);
            st.wHour = Convert.ToInt16(SynchronizeRead.bHour - hour);
            st.wMinute = Convert.ToInt16(SynchronizeRead.bMin - minute);
            st.wSecond = Convert.ToInt16(SynchronizeRead.bSec);
            st.wMilliseconds = Convert.ToInt16(DateTime.UtcNow.Millisecond);
            SetSystemTime(ref st);
        }

        private void TransferClass_TextMessageEvent(APDTransferClass.TTextMessage TextMessageRead)
        {
            if (TextMessageRead.bTempPack == TextMessageRead.bCountPack)
            {
                //DrawMessageToChat(new MessageArgs(0, TextMessageRead.strText));
                List<UserControl_Chat.Message> messages = new List<UserControl_Chat.Message>();
                UserControl_Chat.Message message = new UserControl_Chat.Message()
                {
                    Id = 0,
                    MessageFiled = TextMessageRead.strText,
                    IsSendByMe = Roles.Received,
                    IsTransmited = true,
                };
                messages.Add(message);
                DispatchIfNecessary(() =>
                {
                    newWindow.curChat.DrawMessageToChat(messages);
                });
            }
        }

        private void TransferClass_RegimeWorkEvent(APDTransferClass.TRegimeWork RegimeWorkRead)
        {
            MainPanel.MPanel.Buttons buttons = MainPanel.MPanel.Buttons.Preparation;
            switch (RegimeWorkRead.bRegime)
            {
                case 0:
                    buttons = MainPanel.MPanel.Buttons.Preparation;
                    break;
                case 1:
                    buttons = MainPanel.MPanel.Buttons.RadioIntelligence;
                    break;
                case 2:
                    buttons = MainPanel.MPanel.Buttons.RadioSuppression;
                    break;
                default:
                    buttons = MainPanel.MPanel.Buttons.Preparation;
                    break;
            }
            DispatchIfNecessary(() =>
            {
                MPanel_OnEnumButtonClick(this, buttons);
            });
        }

        private void TransferClass_ForbidRangeFreqEvent(APDTransferClass.TForbidRangeFreq ForbidRangeFreqRead)
        {
            try
            {
                TableFreqForbidden tableFreqForbidden = new TableFreqForbidden()
                {
                    NumberASP = PropNumberASP.SelectedNumASP,
                    FreqMinKHz = ForbidRangeFreqRead.iFreqBegin / 10d,
                    FreqMaxKHz = ForbidRangeFreqRead.iFreqEnd / 10d,
                    Note = "ПУ"
                };

                switch (ForbidRangeFreqRead.bAction)
                {
                    //Добавить
                    case 0:
                        clientDB.Tables[NameTable.TableFreqForbidden].Add(tableFreqForbidden);
                        break;
                    //Заменить
                    case 1:
                        var change = lSpecFreqForbidden[ForbidRangeFreqRead.bTempRow - Convert.ToByte(1)];
                        tableFreqForbidden.Id = change.Id;
                        clientDB.Tables[NameTable.TableFreqForbidden].Change(tableFreqForbidden);
                        break;
                    //Удалить
                    case 2:
                        var isContains = ForbidContains(tableFreqForbidden);
                        if (isContains != null)
                        {
                            clientDB.Tables[NameTable.TableFreqForbidden].Delete(isContains);
                        }
                        break;
                    //Очистить
                    case 4:
                        clientDB.Tables[NameTable.TableFreqForbidden].Clear();
                        break;
                    default:
                        break;
                }
            }
            catch { }            
        }

        private TableFreqForbidden ForbidContains(TableFreqForbidden tableFreqForbidden)
        {
            for (int i = 0; i < lSpecFreqForbidden.Count; i++)
            {
                if (lSpecFreqForbidden[i].FreqMinKHz == tableFreqForbidden.FreqMinKHz && lSpecFreqForbidden[i].FreqMinKHz == tableFreqForbidden.FreqMinKHz)
                    return lSpecFreqForbidden[i].ToFreqForbidden();
            }
            return null;
        }

        private void TransferClass_ReconSectorRangeEvent(APDTransferClass.TReconSectorRange ReconSectorRangeRead)
        {
            TableSectorsRangesRecon tableSectorsRangesRecon = new TableSectorsRangesRecon()
            {
                NumberASP = PropNumberASP.SelectedNumASP,
                FreqMinKHz = ReconSectorRangeRead.iFreqBegin / 10d,
                FreqMaxKHz = ReconSectorRangeRead.iFreqEnd / 10d,
                AngleMin = (short)ReconSectorRangeRead.wAngleBegin,
                AngleMax = (short)ReconSectorRangeRead.wAngleEnd,
                IsCheck = true,
                Note = "ПУ"
            };

            switch (ReconSectorRangeRead.bAction)
            {
                //Добавить
                case 0:
                    clientDB.Tables[NameTable.TableSectorsRangesRecon].Add(tableSectorsRangesRecon);
                    break;
                //Заменить
                case 1:
                    var change = lSRangeRecon[ReconSectorRangeRead.bTempRow];
                    tableSectorsRangesRecon.Id = change.Id;
                    clientDB.Tables[NameTable.TableSectorsRangesRecon].Change(tableSectorsRangesRecon);
                    break;
                //Удалить
                case 2:
                    var isContains = SectorsRangesReconContains(tableSectorsRangesRecon);
                    if (isContains != null)
                    {
                        clientDB.Tables[NameTable.TableSectorsRangesRecon].Delete(isContains);
                    }
                    break;
                //Очистить
                case 4:
                    clientDB.Tables[NameTable.TableSectorsRangesRecon].Clear();
                    break;
                default:
                    break;
            }
        }

        private TableSectorsRangesRecon SectorsRangesReconContains(TableSectorsRangesRecon tableSectorsRangesRecon)
        {
            for (int i = 0; i < lSRangeRecon.Count; i++)
            {
                if (lSRangeRecon[i].FreqMinKHz == tableSectorsRangesRecon.FreqMinKHz && lSRangeRecon[i].FreqMinKHz == tableSectorsRangesRecon.FreqMinKHz)
                    return lSRangeRecon[i].ToRangesRecon();
            }
            return null;
        }

        private void TransferClass_SupressSectorRangeEvent(APDTransferClass.TSupressSectorRange SupressSectorRangeRead)
        {
            TableSectorsRangesSuppr tableSectorsRangesSuppr = new TableSectorsRangesSuppr()
            {
                NumberASP = PropNumberASP.SelectedNumASP,
                FreqMinKHz = SupressSectorRangeRead.iFreqBegin / 10d,
                FreqMaxKHz = SupressSectorRangeRead.iFreqEnd / 10d,
                AngleMin = (short)SupressSectorRangeRead.wAngleBegin,
                AngleMax = (short)SupressSectorRangeRead.wAngleEnd,
                IsCheck = true,
                Note = "ПУ"
            };

            switch (SupressSectorRangeRead.bAction)
            {
                //Добавить
                case 0:
                    clientDB.Tables[NameTable.TableSectorsRangesSuppr].Add(tableSectorsRangesSuppr);
                    break;
                //Заменить
                case 1:
                    var change = lSRangeSuppr[SupressSectorRangeRead.bTempRow];
                    tableSectorsRangesSuppr.Id = change.Id;
                    clientDB.Tables[NameTable.TableSectorsRangesSuppr].Change(tableSectorsRangesSuppr);
                    break;
                //Удалить
                case 2:
                    var isContains = SectorsRangesSupprContains(tableSectorsRangesSuppr);
                    if (isContains != null)
                    {
                        clientDB.Tables[NameTable.TableSectorsRangesSuppr].Delete(isContains);
                    }
                    break;
                //Очистить
                case 4:
                    clientDB.Tables[NameTable.TableSectorsRangesSuppr].Clear();
                    break;
                default:
                    break;
            }
        }

        private TableSectorsRangesSuppr SectorsRangesSupprContains(TableSectorsRangesSuppr tableSectorsRangesSuppr)
        {
            for (int i = 0; i < lSRangeSuppr.Count; i++)
            {
                if (lSRangeSuppr[i].FreqMinKHz == tableSectorsRangesSuppr.FreqMinKHz && lSRangeSuppr[i].FreqMinKHz == tableSectorsRangesSuppr.FreqMinKHz)
                    return lSRangeSuppr[i].ToRangesSuppr();
            }
            return null;
        }

        private void TransferClass_SupressFWSEvent(APDTransferClass.TSupressFWS SupressFWSRead)
        {
            TableSuppressFWS tableSuppressFWS = new TableSuppressFWS()
            {
                Sender = SignSender.PC,
                Bearing = -1,
                NumberASP = PropNumberASP.SelectedNumASP,
                FreqKHz = SupressFWSRead.iFreq / 10d,
                Letter = TableOperations.DefinitionParams.DefineLetter(SupressFWSRead.iFreq / 10d),
                Threshold = -120,
                Priority = SupressFWSRead.bPrior,
                Coordinates = new Coord
                {
                    Latitude = -1,
                    Longitude = -1,
                    Altitude = -1
                },
                InterferenceParam = new InterferenceParam
                {
                    Manipulation = SupressFWSRead.bCodeManipulation,
                    Modulation = SupressFWSRead.bCodeModulation,
                    Deviation = SupressFWSRead.bCodeDeviation,
                    Duration = 1
                }
            };

            switch (SupressFWSRead.bAction)
            {
                //Добавить
                case 0:
                    clientDB.Tables[NameTable.TableSuppressFWS].Add(tableSuppressFWS);
                    break;
                //Заменить
                case 1:
                    var change = lSuppressFWS[SupressFWSRead.bTempRow];
                    tableSuppressFWS.Id = change.Id;
                    clientDB.Tables[NameTable.TableSuppressFWS].Change(tableSuppressFWS);
                    break;
                //Удалить
                case 2:
                    var isContains = SuppressFWSContains(tableSuppressFWS);
                    if (isContains != null)
                    {
                        clientDB.Tables[NameTable.TableSuppressFWS].Delete(isContains);
                    }
                    break;
                //Очистить
                case 4:
                    clientDB.Tables[NameTable.TableSuppressFWS].Clear();
                    break;
                default:
                    break;
            }
        }

        private TableSuppressFWS SuppressFWSContains(TableSuppressFWS tableSuppressFWS)
        {
            for (int i = 0; i < lSuppressFWS.Count; i++)
            {
                if (lSuppressFWS[i].FreqKHz == tableSuppressFWS.FreqKHz)
                    return lSuppressFWS[i];
            }
            return null;
        }

        private void TransferClass_SupressFHSSEvent(APDTransferClass.TSupressFHSS SupressFHSSRead)
        {
            TableSuppressFHSS tableSuppressFHSS = new TableSuppressFHSS()
            {
                NumberASP = PropNumberASP.SelectedNumASP,
                EPO = DefinitionParams.DefineEPO(SupressFHSSRead.iFreqBegin / 10d, SupressFHSSRead.iFreqEnd / 10d),
                FreqMinKHz = SupressFHSSRead.iFreqBegin / 10d,
                FreqMaxKHz = SupressFHSSRead.iFreqEnd / 10d,
                Letters = DefinitionParams.DefineLetters(SupressFHSSRead.iFreqBegin / 10d, SupressFHSSRead.iFreqEnd / 10d),
                StepKHz = (short)transferClass.ConvertFromFHSSStepCodeToStepFHSSkHz(SupressFHSSRead.bCodeStep),
                Threshold = -80,
                InterferenceParam = new InterferenceParam()
                {
                    Deviation = SupressFHSSRead.bCodeDeviation,
                    Duration = SupressFHSSRead.bDuration,
                    Manipulation = SupressFHSSRead.bCodeManipulation,
                    Modulation = SupressFHSSRead.bCodeModulation
                },
            };

            switch (SupressFHSSRead.bAction)
            {
                //Добавить
                case 0:
                    clientDB.Tables[NameTable.TableSuppressFHSS].Add(tableSuppressFHSS);
                    break;
                //Заменить
                case 1:
                    var change = lSuppressFWS[SupressFHSSRead.bTempRow];
                    tableSuppressFHSS.Id = change.Id;
                    clientDB.Tables[NameTable.TableSuppressFHSS].Change(tableSuppressFHSS);
                    break;
                //Удалить
                case 2:
                    var isContains = SuppressFHSSContains(tableSuppressFHSS);
                    if (isContains != null)
                    {
                        clientDB.Tables[NameTable.TableSuppressFHSS].Delete(isContains);
                    }
                    break;
                //Очистить
                case 4:
                    clientDB.Tables[NameTable.TableSuppressFHSS].Clear();
                    break;
                default:
                    break;
            }
        }

        private TableSuppressFHSS SuppressFHSSContains(TableSuppressFHSS tableSuppressFHSS)
        {
            for (int i = 0; i < lSuppressFHSS.Count; i++)
            {
                if (lSuppressFHSS[i].FreqMinKHz == tableSuppressFHSS.FreqMinKHz && lSuppressFHSS[i].FreqMinKHz == tableSuppressFHSS.FreqMinKHz)
                    return lSuppressFHSS[i];
            }
            return null;
        }


        private async void TransferClass_RequestBearingEvent(APDTransferClass.TRequestBearing RequestBearingRead)
        {
            switch (RequestBearingRead.bSign)
            {
                case 0:

                    var qfreqWidthMHz = 0.0036 * 10000d;

                    var qMinBandX = RequestBearingRead.iFreq - qfreqWidthMHz;
                    var qMaxBandX = RequestBearingRead.iFreq + qfreqWidthMHz;

                    var qPhAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? 3 : basicProperties.Global.NumberAveragingPhase;
                    var qPlAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? 3 : basicProperties.Global.NumberAveragingBearing;

                    var qanswer = await dsp.QuasiSimultaneouslyDFX10((int)(qMinBandX), (int)(qMaxBandX), (byte)qPhAvCount, (byte)qPlAvCount);

                    if (qanswer?.Header.ErrorCode == 0)
                    {
                        if (qanswer.Source.Direction != -1)

                        {
                            int LatitudeDegree = 0;
                            int LatitudeMinutes = 0;
                            double LatitudeSeconds = 0;

                            ClassGeoCalculator.f_Grad_GMS(qanswer.Source.Latitude, ref LatitudeDegree, ref LatitudeMinutes, ref LatitudeSeconds);

                            int LongitudeDegree = 0;
                            int LongitudeMinutes = 0;
                            double LongitudeSeconds = 0;

                            ClassGeoCalculator.f_Grad_GMS(qanswer.Source.Longitude, ref LongitudeDegree, ref LongitudeMinutes, ref LongitudeSeconds);

                            short Direction2 = -1;
                            if (qanswer.LinkedStationResults.Count() > 0)
                            {
                                Direction2 = (short)(qanswer.LinkedStationResults[0].Direction * 10);
                            }
                            transferClass.SendJQuasiBearingAnswer(1, (ushort)qanswer.Source.Direction, (ushort)Direction2,
                                0, 0,
                               (byte)LatitudeDegree, (byte)LatitudeMinutes, (byte)LatitudeSeconds,
                                (byte)LongitudeDegree, (byte)LongitudeMinutes, (byte)LongitudeSeconds);
                        }
                    }
                    break;
                case 1:

                    var freqWidthMHz = 0.0036;

                    var MinBandX = RequestBearingRead.iFreq / 10000d - freqWidthMHz;
                    var MaxBandX = RequestBearingRead.iFreq / 10000d + freqWidthMHz;

                    var PhAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? 3 : basicProperties.Global.NumberAveragingPhase;
                    var PlAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? 3 : basicProperties.Global.NumberAveragingBearing;

                    var answer = await dsp.ExecutiveDF(MinBandX, MaxBandX, PhAvCount, PlAvCount);

                    if (answer?.Header.ErrorCode == 0)
                    {
                        ushort bearing = (ushort)(answer.Direction / 10);
                        transferClass.SendJExBearingAnswer(1, (ushort)answer.Direction);
                    }

                    break;
                default:
                    break;
            }
        }


        private void TransferClass_TextMessageJEvent(APDTransferClass.TTextMessage TextMessageJRead)
        {
        }

        private void TransferClass_ReceptionEvent(APDTransferClass.TAnsReception AnsReceptionRead)
        {
        }

        private void TransferClass_CoordJammerEvent(APDTransferClass.TCoordJammer CoordJammerRead)
        {
        }

        private void TransferClass_DataFWS1Event(APDTransferClass.TDataFWSPartOne DataFWSPartOneRead)
        {
        }

        private void TransferClass_DataFWS2Event(APDTransferClass.TDataFWSPartTwo DataFWSPartTwoRead)
        {
        }

        private void TransferClass_DataFHSS1Event(APDTransferClass.TDataFHSSPartOne DataFHSSPartOneRead)
        {
        }

        private void TransferClass_DataFHSS2Event(APDTransferClass.TDataFHSSPartTwo DataFHSSPartTwoRead)
        {
        }

        private void TransferClass_StateSupressFreqEvent(APDTransferClass.TStateSupressFreq StateSupressFreqRead)
        {
        }

        private void TransferClass_ExecuteBearingEvent(APDTransferClass.TExecuteBearing ExecuteBearingRead)
        {
        }

        private void TransferClass_TSimultanBearingEvent(APDTransferClass.TSimultanBearing SimultanBearingRead)
        {
        }

        private void TransferClass_TestChannelEvent(APDTransferClass.TTestChannel TestChannelRead)
        {
        }


        private void Button_ClickOpenJ(object sender, RoutedEventArgs e)
        {
            transferClass.InitRole(APDTransferClass.WhoIsWho.Jammer);
            transferClass.OpenCOM(Convert.ToByte(1));
        }

        private void Button_ClickCloseJ(object sender, RoutedEventArgs e)
        {
            transferClass.CloseCOM();
        }

        private void Events_OnSendStationsMessage(List<UserControl_Chat.Message> stationsMessages)
        {
            for (int i = 0; i < stationsMessages.Count(); i++)
            {
                if (stationsMessages[i].Id == 0)
                {
                    transferClass.SendJTextMessage(1, stationsMessages[i].MessageFiled);
                    return;
                }
            }
            //transferClass.SendJTextMessage(1, "Hello");
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            transferClass.SendJTextMessage(1, "Hello");
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            transferClass.SendJReceiptConfirmation(1, 1, 1, 1);
        }

        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            transferClass.SendJStationCoordinates(1, 0, 0, 29, 38, 26, 59, 36, 45);
        }

        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TDataFWSPartOne p1 = new APDTransferClass.TDataFWSPartOne();
            APDTransferClass.TDataFWSPartTwo p2 = new APDTransferClass.TDataFWSPartTwo();

            p1.bTempSource = 1;     // номер текущего ИРИ
            p1.iFreq = 1324005;           // частота           
            p1.bCodeType = 0;       // код вида            
            p1.bCodeWidth = transferClass.ConvertFromSignalWidthToWidthCode(25d);      // код ширины
            p1.bHour = 14;           // часы
            p1.bMin = 58;            // минуты
            p1.bSec = 04;            // секунды
            p1.bCodeRate = 0;       // скорость ТЛГ

            p2.bTempSource = 1;      // номер текущего ИРИ
            p2.bSignLong = 0;        // знак долготы           
            p2.bLongDegree = 56;     // градусы долготы
            p2.bLongMinute = 23;      // минуты долготы
            p2.bLongSecond = 14;      // секунды долготы
            p2.bSignLat = 0;         // знак широты
            p2.bLatDegree = 29;       // градусы широты
            p2.bLatMinute = 36;       // минуты широты
            p2.bLatSecond = 51;       // секунды широты
            p2.wBearMain = 200;        // пеленг от ведущей
            p2.wBearAdd = 30;         // пеленг от ведомой

            transferClass.SendJFRSInformation(1, p1, p2);
        }

        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TDataFHSSPartOne p1 = new APDTransferClass.TDataFHSSPartOne();
            APDTransferClass.TDataFHSSPartTwo p2 = new APDTransferClass.TDataFHSSPartTwo();

            p1.bTempSource = 1;     // номер текущего ИРИ
            p1.iFreqBegin = 562006;      // начальная частота
            p1.iFreqEnd = 622008;        // конечная частота
            p1.bHour = 14;           // часы
            p1.bMin = 58;            // минуты
            p1.bSec = 04;            // секунды
            p1.bCodeWidth = transferClass.ConvertFromFHSSWidthToWidthCode(25);      // код ширины
            p1.bCodeStep = transferClass.ConvertFromFHSSStepToStepCode(5);       // код шага

            p2.bTempSource = 1;     // номер текущего ИРИ
            transferClass.ConvertFromFHSSDurationmstoCode(15.0f, out byte bGroupDuration, out byte bDuration);
            p2.bGroupDuration = bGroupDuration;  // длительность
            p2.bDuration = bDuration;       // длительность
            p2.wBearMain = 200;       // пеленг от ведущей
            p2.wBearAdd = 30;        // пеленг от ведомой
            p2.bSignLong = 0;       // знак долготы           
            p2.bLongDegree = 55;     // градусы долготы
            p2.bLongMinute = 42;     // минуты долготы
            p2.bLongSecond = 28;     // секунды долготы
            p2.bSignLat = 0;        // знак широты
            p2.bLatDegree = 28;      // градусы широты
            p2.bLatMinute = 06;      // минуты широты
            p2.bLatSecond = 13;      // секунды широты   
            p2.bCodeType = 0;       // код вида модуляции     

            transferClass.SendJFHSSInformation(1, p1, p2);
        }

        private void Button_Click6(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TStateSupressFreq p = new APDTransferClass.TStateSupressFreq();
            p.Init();

            //p.bStateWork;   // инф-ция о работе    
            //p.bStateLongTerm;  // инф-ция о долговременности           
            //p.bStateSupress;   // инф-ция о подвлении           
            p.bTempSubRange = 1;   // номер поддиапазона
            p.bSighF = 0;          // признак ФЧ/ППРЧ
            p.bStateFHSS = 0;      // инф-ция ППРЧ

            transferClass.SendJReceiptJammingFrequencies(1, p);
        }

        private void Button_Click7(object sender, RoutedEventArgs e)
        {
            transferClass.SendJExBearingAnswer(1, 45);
        }

        private void Button_Click8(object sender, RoutedEventArgs e)
        {
            transferClass.SendJQuasiBearingAnswer(1, 200, 30, 0, 0, 28, 33, 21, 59, 15, 52);
        }

        private void Button_Click9(object sender, RoutedEventArgs e)
        {
            transferClass.SendJChanelTesting(1, 1, 0, 0, new byte[7], new byte[7]);
        }

        private void Button_ClickOpenPU(object sender, RoutedEventArgs e)
        {
            transferClass.InitRole(APDTransferClass.WhoIsWho.CommandCentre);
            transferClass.OpenCOM(Convert.ToByte(1));
        }

        private void Button_ClickClosePU(object sender, RoutedEventArgs e)
        {
            transferClass.CloseCOM();
        }

        private void Button_Click10(object sender, RoutedEventArgs e)
        {
            transferClass.SendССDataRequest(0, APDTransferClass.TypeComand.Квитанция);
        }

        private void Button_Click11(object sender, RoutedEventArgs e)
        {
            transferClass.SendССSync(0, DateTime.Now);
        }

        private void Button_Click12(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCTextMessage(0, "Helloooo");
        }

        private void Button_Click13(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCWorkMode(0, APDTransferClass.Mode.Подготовка);
        }

        private void Button_Click14(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCForbiddenRanges(0, APDTransferClass.Action.Добавить, 1, 1550000, 1575000);
        }

        private void Button_Click15(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCSectorsAndRangesRI(0, APDTransferClass.Action.Добавить, 1, 1452003, 1975001, 45, 95);
        }

        private void Button_Click16(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCSectorsAndRangesRS(0, APDTransferClass.Action.Добавить, 1, 1, 1452003, 1975001, 45, 95);
        }

        private void Button_Click17(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TSupressFWS p = new APDTransferClass.TSupressFWS();

            p.bAction = (byte)APDTransferClass.Action.Добавить;    // действие
            p.bTempRow = 1;            // номер строки            
            p.iFreq = 1579004;               // частота
            p.iCodeHind = 3;           // код помехи
            p.bPrior = 1;              // приоритет
            p.bCodeModulation = (byte)APDTransferClass.TypeRS.ЧМ;     // код вида
            p.bCodeDeviation = (byte)APDTransferClass.Deviation1.dev1_75;      // код ширины
            p.bCodeManipulation = 0;   // код скорости

            transferClass.SendCCFRSonRS(0, p);
        }

        private void Button_Click18(object sender, RoutedEventArgs e)
        {
            APDTransferClass.TSupressFHSS p = new APDTransferClass.TSupressFHSS();

            p.bAction = (byte)APDTransferClass.Action.Добавить;         // действие
            p.bTempRow = 1;            // номер строки            
            p.iFreqBegin = 1605003;          // начальная частота
            p.iFreqEnd = 1718002;            // конечная частота 

            transferClass.ConvertFromFHSSDurationmstoCode(15.0f, out byte bGroupDuration, out byte bDuration);
            p.bGroupDuration = bGroupDuration;      // длительность
            p.bDuration = bDuration;           // длительность            

            p.bCodeStep = transferClass.ConvertFromFHSSStepToStepCode(5);       // код шага;           // код шага

            p.bCodeModulation = (byte)APDTransferClass.TypeRS.ЧМ; ;     // код вида
            p.bCodeDeviation = (byte)APDTransferClass.Deviation1.dev1_75;     // код ширины
            p.bCodeManipulation = 0;   // код ширины

            transferClass.SendCCFHSSonRS(0, p);
        }

        private void Button_Click19(object sender, RoutedEventArgs e)
        {
            transferClass.SendCCExOrQuasiBearingRequest(0, APDTransferClass.TypeReqBearing.Исполнительное, 1590000, 15);
        }
    }
}
