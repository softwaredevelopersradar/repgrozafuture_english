﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GrozaBerezinaDLL;
using StructTypeGR_BRZ;

using System.Drawing;
using System.IO.Ports;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using ModelsTablesDBLib;
using ValuesCorrectLib;
using TableOperations;
using TableEvents;
using UserControl_Chat;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        ///Обработка связи с Пунктом Управления Березина

        [DllImport("kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true)]
        public static extern bool GetSystemTime(ref SYSTEMTIME time);


        (int NumPacket, int TimePeriod) varPU = (50, 100); //DefaultParameters

        #region Groza
        static public IExGRZ_BRZ ClientPu;

        short TIME_INDICATE = 300;
        const Int32 FreqMin_RR_Bel = 250000;
        const Int32 FreqMax_RR_Bel = 30250000;
        const Int32 FreqMin_RP_Bel = 300000;
        const Int32 FreqMax_RP_Bel = 12150000;

        const Int32 FreqMin_RR_BRZ = 250000;
        const Int32 FreqMax_RR_BRZ = 60250000;
        const Int32 FreqMin_RP_BRZ = 300000;
        const Int32 FreqMax_RP_BRZ = 30000000;


        #region диапазоны литер
        public struct RangesLetters
        {
            public static int FREQ_START_LETTER_1 = 300000;
            public static int FREQ_START_LETTER_2 = 500000;
            public static int FREQ_START_LETTER_3 = 900000;
            public static int FREQ_START_LETTER_4 = 1600000;
            public static int FREQ_START_LETTER_5 = 2900000;
            public static int FREQ_START_LETTER_6 = 5120000;
            public static int FREQ_START_LETTER_7 = 8600000;
            public static int FREQ_START_LETTER_8 = 10000000;
            public static int FREQ_START_LETTER_9 = 20000000;
            public static int FREQ_STOP_LETTER_9 = 30000000;
        }
        #endregion

        private void ReadByte(object sender, byte[] bData)
        {
            if (sender == ClientPu)
            {
                if (PCControlConnection.LedTransVisible)
                    DispatchIfNecessary(() =>
                    {
                        PCControlConnection.ShowRead();
                    });
            }
        }

        private void WriteByte(object sender, byte[] bData)
        {
            if (sender == ClientPu)
            {
                if (PCControlConnection.LedTransVisible)
                    DispatchIfNecessary(() =>
                    {
                        PCControlConnection.ShowWrite();
                    });
            }
        }
        private void Connect(object sender)
        {
            if (sender == ClientPu)
            {
                DispatchIfNecessary(() =>
                {
                    PCControlConnection.ShowConnect();
                });
            }
        }
        private void Disconnect(object sender)
        {
            if (sender == ClientPu)
            {
                DispatchIfNecessary(() =>
                {
                    PCControlConnection.ShowDisconnect();
                });
            }
            _isBerezinaConnected = false;
            if (ClientPu != null)
                ClientPu.OnDisconnect -= new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Disconnect);
            ClientPu = null;
        }

        private bool _isBerezinaConnected = false;
        private TypeOfConection _BerezinaConnectionType = TypeOfConection.APD;

        private string _ActiveBerezinaCom = "";
        private string _ActiveBerezinaRRSIPPort = "";
        private string _ActiveBerezina34GIPPort = "";
        private string _ActiveBerezinaRouterIPPort = "";




        private void ReInitBRZ(LocalProperties arg, bool isManual = false)
        {
            if (arg.PCProperties.IsBerezinaVisible == true)
            {
                if (arg.PCProperties.State == true)
                {
                    if (_isBerezinaConnected == true)
                    {
                        if (_BerezinaConnectionType != arg.PCProperties.TypeOfConection)
                        {
                            DeInitIExGRZ_BRZ();
                            InitIExGRZ_BRZ(arg, isManual);
                        }
                        else
                        {
                            switch (_BerezinaConnectionType)
                            {
                                case TypeOfConection.Tainet:

                                    if (_ActiveBerezinaCom != arg.PCProperties.ComPortTainet)
                                    {
                                        DeInitIExGRZ_BRZ();
                                        InitIExGRZ_BRZ(arg, isManual);
                                        return;
                                    }

                                    break;

                                case TypeOfConection.PPC:

                                    if (_ActiveBerezinaRRSIPPort != EjectBRZIpPortString(arg))
                                    {
                                        DeInitIExGRZ_BRZ();
                                        InitIExGRZ_BRZ(arg, isManual);
                                        return;
                                    }

                                    break;

                                case TypeOfConection._3G:

                                    if (_ActiveBerezina34GIPPort != EjectBRZIpPortString(arg))
                                    {
                                        DeInitIExGRZ_BRZ();
                                        InitIExGRZ_BRZ(arg, isManual);
                                        return;
                                    }

                                    break;

                                case TypeOfConection.Router:

                                    if (_ActiveBerezinaRouterIPPort != EjectBRZIpPortString(arg))
                                    {
                                        DeInitIExGRZ_BRZ();
                                        InitIExGRZ_BRZ(arg, isManual);
                                        return;
                                    }

                                    break;
                            }

                            if (isManual)
                                DeInitIExGRZ_BRZ();
                        }
                    }
                    else
                    {
                        InitIExGRZ_BRZ(arg, isManual);
                    }

                }
                else
                {
                    if (_isBerezinaConnected == true)
                    {
                        DeInitIExGRZ_BRZ();
                    }
                    else
                    {

                    }
                }
            }
        }

        private void InitIExGRZ_BRZ(LocalProperties localProperties, bool isManual = false)
        {
            if (isManual)
            {
                //TO DO: 
                {
                    //Адрес свой
                    //ClientPu = new IExGRZ_BRZ(Convert.ToByte(variableCommon.OwnAddressPC));

                    int ind = lASP.FindIndex(x => x.ISOwn == true);
                    byte OwnStationAdress = (ind != -1 && lASP[ind].Id <= 255) ? (byte)(lASP[ind].Id) : (byte)1;

                    ClientPu = new IExGRZ_BRZ(OwnStationAdress);
                }

                ClientPu.OnConnect += new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Connect);
                ClientPu.OnDisconnect += new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Disconnect);
                ClientPu.OnReadByte += new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(ReadByte);
                ClientPu.OnWriteByte += new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(WriteByte);

                //TO DO: Подключение
                if (localProperties.PCProperties.IsBerezinaVisible == true
                    && localProperties.PCProperties.Type == TypePC.Berezina
                    && localProperties.PCProperties.State == true)
                {
                    switch (localProperties.PCProperties.TypeOfConection)
                    {
                        case TypeOfConection.Tainet:
                            try
                            {
                                ClientPu.Connect(localProperties.PCProperties.ComPortTainet, 9600,
                                    standartParam.Parity, standartParam.DataBits, standartParam.StopBits);
                                if (ClientPu == null) return;
                                _ActiveBerezinaCom = localProperties.PCProperties.ComPortTainet;
                            }
                            catch { }
                            break;
                        case TypeOfConection.PPC:
                            bool ok = ClientPu.Connect(localProperties.PCProperties.IpAddressPPC, localProperties.PCProperties.PortPPC);
                            if (!ok) return;
                            _ActiveBerezinaRRSIPPort = EjectBRZIpPortString(localProperties);
                            break;
                        case TypeOfConection._3G:
                            ok = ClientPu.Connect(localProperties.PCProperties.IpAddress3G4GRouter, localProperties.PCProperties.Port3G4GRouter);
                            if (!ok) return;
                            _ActiveBerezina34GIPPort = EjectBRZIpPortString(localProperties);
                            break;
                        case TypeOfConection.Router:
                            ok = ClientPu.Connect(localProperties.PCProperties.IpAddressRouter, localProperties.PCProperties.PortRouter);
                            if (!ok) return;
                            _ActiveBerezinaRouterIPPort = EjectBRZIpPortString(localProperties);
                            break;
                    }

                }

                _isBerezinaConnected = true;

                ClientPu.OnRequestSynchTime += new IExGRZ_BRZ.RequestSynchTimeEventHandler(Receive_RequestSynchTime); //Синхронизация времени

                ClientPu.OnRequestRegime += new IExGRZ_BRZ.RequestRegimeEventHandler(Receive_RequestRegime); //Устаноква Режима работы

                ClientPu.OnRequestRangeSpec += new IExGRZ_BRZ.RequestRangeSpecEventHandler(Receive_RequestRangeSpec); //Специальные частоты

                ClientPu.OnRequestSupprFWS += new IExGRZ_BRZ.RequestSupprFWSEventHandler(Receive_RequestSupprFWS); //Назначение ФРЧ на РП

                ClientPu.OnRequestState += new IExGRZ_BRZ.RequestStateEventHandler(Receive_RequestState); //Запрос состояния

                ClientPu.OnRequestRangeSector += new IExGRZ_BRZ.RequestRangeSectorEventHandler(Receive_RequestRangeSector); //Сектора и диапазоны

                ClientPu.OnReceiveTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.CmdTextEventHandler(Receive_TextCmd);

                ClientPu.OnReceiveConfirmTextCmd += new GrozaBerezinaDLL.IExGRZ_BRZ.ConfirmTextEventHandler(Receive_ConfirmTextCmd);

                ClientPu.OnRequestReconFWS += ClientPu_OnRequestReconFWS; //Запрос ИРИ ФРЧ
                ClientPu.OnRequestReconFHSS += ClientPu_OnRequestReconFHSS; //Запрос ИРИ ППРЧ 
                ClientPu.OnRequestSupprFHSS += ClientPu_OnRequestSupprFHSS; ////Назначение ППРЧ на РП 
                ClientPu.OnRequestCoord += ClientPu_OnRequestCoord; // Запрос координат
                ClientPu.OnRequestExecBear += ClientPu_OnRequestExecBear; // Запрос на исполнительное пеленгование
                ClientPu.OnRequestSimulBear += ClientPu_OnRequestSimulBear; // Запрос на квазиодновременное пеленгование

                ClientPu.OnRequestStateSupprFHSS += ClientPu_OnRequestStateSupprFHSS;//Квитанция ири ППРЧ РП
                ClientPu.OnRequestStateSupprFWS += ClientPu_OnRequestStateSupprFWS;//Квитанция ири ФРЧ РП

                

                Events.OnSendStationsMessage += Events_OnSendStationsMessage1;

                _BerezinaConnectionType = localProperties.PCProperties.TypeOfConection;
            }
        }

       
        private void DeInitIExGRZ_BRZ()
        {
            try
            {
                ClientPu.OnConnect -= new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Connect);
                //ClientPu.OnDisconnect -= new GrozaBerezinaDLL.IExGRZ_BRZ.ConnectEventHandler(Disconnect);
                ClientPu.OnReadByte -= new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(ReadByte);
                ClientPu.OnWriteByte -= new GrozaBerezinaDLL.IExGRZ_BRZ.ByteEventHandler(WriteByte);

                ClientPu.OnRequestSynchTime -= new IExGRZ_BRZ.RequestSynchTimeEventHandler(Receive_RequestSynchTime); //Синхронизация времени

                ClientPu.OnRequestRegime -= new IExGRZ_BRZ.RequestRegimeEventHandler(Receive_RequestRegime); //Устаноква Режима работы

                ClientPu.OnRequestRangeSpec -= new IExGRZ_BRZ.RequestRangeSpecEventHandler(Receive_RequestRangeSpec); //Специальные частоты

                ClientPu.OnRequestSupprFWS -= new IExGRZ_BRZ.RequestSupprFWSEventHandler(Receive_RequestSupprFWS); //Назначение ФРЧ на РП

                ClientPu.OnRequestState -= new IExGRZ_BRZ.RequestStateEventHandler(Receive_RequestState); //Запрос состояния

                ClientPu.OnRequestRangeSector -= new IExGRZ_BRZ.RequestRangeSectorEventHandler(Receive_RequestRangeSector); //Сектора и диапазоны

                ClientPu.OnReceiveTextCmd -= new GrozaBerezinaDLL.IExGRZ_BRZ.CmdTextEventHandler(Receive_TextCmd);

                ClientPu.OnReceiveConfirmTextCmd -= new GrozaBerezinaDLL.IExGRZ_BRZ.ConfirmTextEventHandler(Receive_ConfirmTextCmd);

                ClientPu.OnRequestReconFWS -= ClientPu_OnRequestReconFWS; //Запрос ИРИ ФРЧ
                ClientPu.OnRequestReconFHSS -= ClientPu_OnRequestReconFHSS; //Запрос ИРИ ППРЧ 
                ClientPu.OnRequestSupprFHSS -= ClientPu_OnRequestSupprFHSS; ////Назначение ППРЧ на РП 
                ClientPu.OnRequestCoord -= ClientPu_OnRequestCoord; // Запрос координат
                ClientPu.OnRequestExecBear -= ClientPu_OnRequestExecBear; // Запрос на исполнительное пеленгование
                ClientPu.OnRequestSimulBear -= ClientPu_OnRequestSimulBear; // Запрос на квазиодновременное пеленгование

                ClientPu.OnRequestStateSupprFHSS -= ClientPu_OnRequestStateSupprFHSS;//Квитанция ири ППРЧ РП
                ClientPu.OnRequestStateSupprFWS -= ClientPu_OnRequestStateSupprFWS;//Квитанция ири ФРЧ РП

                _isBerezinaConnected = false;

                Events.OnSendStationsMessage -= Events_OnSendStationsMessage1;

                //ClientPu = null;
                ClientPu.Disconnect();

                _BerezinaConnectionType = TypeOfConection.APD;

                _ActiveBerezinaCom = "";
                _ActiveBerezinaRRSIPPort = "";
                _ActiveBerezina34GIPPort = "";
                _ActiveBerezinaRouterIPPort = "";
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + e.StackTrace);
            }
        }

        private string EjectBRZIpPortString(LocalProperties localProperties)
        {
            switch (localProperties.PCProperties.TypeOfConection)
            {
                case TypeOfConection.PPC:
                    return localProperties.PCProperties.IpAddressPPC + ":" + localProperties.PCProperties.PortPPC.ToString();
                case TypeOfConection._3G:
                    return localProperties.PCProperties.IpAddress3G4GRouter + ":" + localProperties.PCProperties.Port3G4GRouter.ToString();
                case TypeOfConection.Router:
                    return localProperties.PCProperties.IpAddressRouter + ":" + localProperties.PCProperties.PortRouter.ToString();
                default:
                    return "";
            }
        }



        StructTypeGR_BRZ.TResSupprFWS[] StateSuppFWS; // Cостояние ФРЧ на РП
        void UpdateRadioJamStateStructForBRZ(List<TempSuppressFWS> listTempSuppressFWS)
        {
            try
            {
                StateSuppFWS = new StructTypeGR_BRZ.TResSupprFWS[listTempSuppressFWS.Count];
                for (int i = 0; i < listTempSuppressFWS.Count; i++)
                {
                    StateSuppFWS[i].iID = listTempSuppressFWS[i].Id;
                    StateSuppFWS[i].iFreq = (int)listTempSuppressFWS[i].FreqKHz * 10;
                    StateSuppFWS[i].bResCtrl = (byte)listTempSuppressFWS[i].Control;
                    StateSuppFWS[i].bResSuppr = (byte)listTempSuppressFWS[i].Suppress;
                }
            }
            catch{ }
        }

        StructTypeGR_BRZ.TResSupprFHSS[] StateSuppFHSS; // Cостояние ППРЧ на РП
        void UpdateFhssRadioJamStructForBRZ(Protocols.FhssRadioJamUpdateEvent answer)
        {
            try
            {
                if (answer == null || answer.Frequencies.Length == 0)
                    return;
                if (answer.Header.ErrorCode != 0)
                    return;
                StateSuppFHSS = new StructTypeGR_BRZ.TResSupprFHSS[answer.JammingStates.Length];
                for (int i = 0; i < answer.JammingStates.Length; i++)
                {
                    StateSuppFHSS[i].iID = answer.JammingStates[i].Id;
                    StateSuppFHSS[i].iFreqMin = answer.Frequencies.Min();
                    StateSuppFHSS[i].iFreqMax = answer.Frequencies.Max();
                    StateSuppFHSS[i].bResCtrl = 1;
                    StateSuppFHSS[i].bResSuppr = Convert.ToByte(answer.JammingStates[i].IsJammed);
                }
            }
            catch { }
        }

        //Квитанция ири ФРЧ РП
        void ClientPu_OnRequestStateSupprFWS(object sender)
        {
            try
            {
                if (StateSuppFWS == null)
                    ClientPu.SendStateSupprFWS(1, null);
                else
                    ClientPu.SendStateSupprFWS(0, StateSuppFWS);
            }
            catch
            { ClientPu.SendStateSupprFWS(1, null); }
        }

        //Квитанция ири ППРЧ РП
        void ClientPu_OnRequestStateSupprFHSS(object sender)
        {
            try
            {
                if (StateSuppFHSS == null)
                    ClientPu.SendStateSupprFHSS(1, null);
                else
                    ClientPu.SendStateSupprFHSS(0, StateSuppFHSS);
            }
            catch
            { ClientPu.SendStateSupprFHSS(1, null); }
        }

        //Запрос координат
        void ClientPu_OnRequestCoord(object sender)
        {
            byte ErrorCode = 0;
            StructTypeGR_BRZ.TCoord tCoord = new StructTypeGR_BRZ.TCoord();
            try
            {
                ConvertCoord convertCoord = new ConvertCoord();
                CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                //Добыть координаты из листа ASP
                //Брать только координаты своей станции
                var OwnStation = lASP.Where(x => x.ISOwn == true).FirstOrDefault();

                //coordDegMinSec = convertCoord.DegToDegMinSec(Math.Abs(variableWork.CoordsGNSS[0].Lat), Math.Abs(variableWork.CoordsGNSS[0].Lon));
                coordDegMinSec = convertCoord.DegToDegMinSec(
                    Math.Abs(OwnStation.Coordinates.Latitude),
                    Math.Abs(OwnStation.Coordinates.Longitude));

                tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;

                //tCoord.bSignLatitude = variableWork.CoordsGNSS[0].signLat;
                tCoord.bSignLatitude = (byte)((OwnStation.Coordinates.Latitude > 0) ? 0 : 1);

                //tCoord.bSignLongitude = variableWork.CoordsGNSS[0].signLon;
                tCoord.bSignLongitude = (byte)((OwnStation.Coordinates.Longitude > 0) ? 0 : 1);
            }
            catch { ErrorCode = 1; }
            ClientPu.SendCoord(ErrorCode, tCoord);
        }

        //Поиск попадания в Литеру
        byte LetterFind(int iFreq)
        {
            byte bLetter = 10;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_1 & iFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_2 & iFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_3 & iFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_4 & iFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_5 & iFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_6 & iFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_7 & iFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_8 & iFreq < RangesLetters.FREQ_START_LETTER_9)
                bLetter = 8;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_9 & iFreq <= RangesLetters.FREQ_STOP_LETTER_9)
                bLetter = 9;
            return bLetter;
        }

        //ППРЧ от Березина в ППРЧ РП
        void ClientPu_OnRequestSupprFHSS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFHSS[] tSupprFHSS)
        {
            byte ErrorCode = 0;
            try
            {
                //TO DO: iDuration to Global Properties
                for (int i = 0; i < tSupprFHSS.Length; i++)
                {
                    if (tSupprFHSS[i].iFreqMin < FreqMin_RP_BRZ || tSupprFHSS[i].iFreqMax > FreqMax_RP_BRZ)
                    {
                        ErrorCode = 1;
                    }
                    else
                    {
                        TableSuppressFHSS tableSuppressFHSS = new TableSuppressFHSS
                        {
                            Id = 0,
                            NumberASP = TableEvents.PropNumberASP.SelectedNumASP,

                            FreqMinKHz = tSupprFHSS[i].iFreqMin / 10,
                            FreqMaxKHz = tSupprFHSS[i].iFreqMax / 10,
                            StepKHz = 50,
                            Letters = DefinitionParams.DefineLetters(tSupprFHSS[i].iFreqMin / 10, tSupprFHSS[i].iFreqMax / 10),

                            Threshold = ConstValues.constThreshold,

                            EPO = DefinitionParams.DefineEPO(tSupprFHSS[i].iFreqMin / 10, tSupprFHSS[i].iFreqMax / 10),
                            InterferenceParam = new InterferenceParam
                            {
                                Manipulation = tSupprFHSS[i].bManipulation,
                                Modulation = tSupprFHSS[i].bModulation,
                                Deviation = tSupprFHSS[i].bDeviation,
                                Duration = 1
                            },
                        };

                        UcReconFHSS_OnAddFHSS_RS_Recon(this, tableSuppressFHSS);

                        //TO DO: bCodeFFT to Global Properties
                        //temp.bCodeFFT = tSupprFHSS[i].bCodeFFT;
                    }
                }
            }
            catch (SystemException)
            { ErrorCode = 2; }

            if (ClientPu != null)
                ClientPu.SendSupprFHSS(ErrorCode);
        }

        public bool isBerezinaComPort()
        {
            if (basicProperties.Local.PCProperties.IsBerezinaVisible == true
                && basicProperties.Local.PCProperties.State == true
                && basicProperties.Local.PCProperties.TypeOfConection == TypeOfConection.Tainet)
                return true;
            else return false;
        }

        //Разведанные ППРЧ из Таблицы ППРЧ на Березину
        async void ClientPu_OnRequestReconFHSS(object sender)
        {
            //Кол-во элементов в листе ИРИ ППРЧ TableReconFHSS
            //Думаю даже сделать lock на этот лист
            try
            {
                StructTypeGR_BRZ.TReconFHSS[] tReconFHSS = new StructTypeGR_BRZ.TReconFHSS[lReconFHSS.Count];
                for (int i = 0; i < lReconFHSS.Count; i++)
                {

                    tReconFHSS[i].bBandWidth = (byte)lReconFHSS[i].Deviation;
                    tReconFHSS[i].bLevelLinked = 0; // Еще спросить у Лины 
                    tReconFHSS[i].bLevelOwn = 0; // Еще спросить у Лины  
                    tReconFHSS[i].bModulation = lReconFHSS[i].Modulation; // Сросить у Иры
                    tReconFHSS[i].bSignAudio = 0;
                    tReconFHSS[i].iFreqMin = (int)(lReconFHSS[i].FreqMinKHz * 10);
                    tReconFHSS[i].iFreqMax = (int)(lReconFHSS[i].FreqMaxKHz * 10);
                    tReconFHSS[i].iID = lReconFHSS[i].Id;

                    var FHSSSource = lSourceFHSS.Where(x => x.IdFHSS == lReconFHSS[i].Id).FirstOrDefault();

                    if (FHSSSource != null)
                    {
                        tReconFHSS[i].sBearingOwn = (short)FHSSSource.ListJamDirect[0].JamDirect.Bearing;
                        tReconFHSS[i].sBearingLinked = (short)((FHSSSource.ListJamDirect.Count > 1) ? FHSSSource.ListJamDirect[1].JamDirect.Bearing : -1);

                        tReconFHSS[i].bLevelOwn = (byte)(FHSSSource.ListJamDirect[0].JamDirect.Level * -1);
                        tReconFHSS[i].bLevelLinked = (byte)((FHSSSource.ListJamDirect.Count > 1) ? FHSSSource.ListJamDirect[1].JamDirect.Level * -1 : 0);
                    }

                    ConvertCoord convertCoord = new ConvertCoord();

                    CoordDegMinSec coordDegMinSec = new CoordDegMinSec();
                    coordDegMinSec = convertCoord.DegToDegMinSec(
                        Math.Abs(FHSSSource.Coordinates.Latitude),
                        Math.Abs(FHSSSource.Coordinates.Longitude));
                    tReconFHSS[i].tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                    tReconFHSS[i].tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                    tReconFHSS[i].tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                    tReconFHSS[i].tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                    tReconFHSS[i].tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                    tReconFHSS[i].tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;

                    tReconFHSS[i].tCoord.bSignLatitude = (byte)((FHSSSource.Coordinates.Latitude > 0) ? 0 : -1);
                    tReconFHSS[i].tCoord.bSignLongitude = (byte)((FHSSSource.Coordinates.Longitude > 0) ? 0 : -1);

                    DateTime dTime = DateTime.Now;
                    tReconFHSS[i].bMinute = (byte)dTime.Minute;
                    tReconFHSS[i].bSecond = (byte)dTime.Second;
                    tReconFHSS[i].sStep = (short)lReconFHSS[i].StepKHz;
                    tReconFHSS[i].iDuration = lReconFHSS[i].ImpulseDuration;
                }
                //VariablePU varPU = new VariableStatic.VariablePU();

                //if (variableConnection.PostControlType == 2)
                //TO DO: BerezinaComPort
                bool BerezinaComPort = isBerezinaComPort();
                if (BerezinaComPort)
                {
                    if (tReconFHSS.Length > varPU.NumPacket)
                    {
                        do
                        {
                            StructTypeGR_BRZ.TReconFHSS[] Buff = new StructTypeGR_BRZ.TReconFHSS[varPU.NumPacket];
                            Array.Copy(tReconFHSS, Buff, varPU.NumPacket);
                            Array.Reverse(tReconFHSS);
                            Array.Resize(ref tReconFHSS, tReconFHSS.Length - varPU.NumPacket);
                            Array.Reverse(tReconFHSS);
                            ClientPu.SendReconFHSS(0, Buff);
                            await Task.Run(async () =>
                            {
                                await Task.Delay(varPU.TimePeriod);
                            });
                        }
                        while (tReconFHSS.Length > varPU.NumPacket);
                    }
                    if (tReconFHSS.Length > 0)
                        ClientPu.SendReconFHSS(0, tReconFHSS);
                }
                else
                {
                    ClientPu.SendReconFHSS(0, tReconFHSS);
                }
            }
            catch { }
        }

        //Запрос ИРИ ФРЧ
        async void ClientPu_OnRequestReconFWS(object sender)
        {
            //Забрать данные из таблици ИРИ ФРЧ на ЦР        
            // Передать их ПУ, отчистить табл ИРИ ФРЧ на ЦР
            try
            {
                StructTypeGR_BRZ.TReconFWS[] tReconFWSAuto = new StructTypeGR_BRZ.TReconFWS[lReconFWS.Count];
                for (int i = 0; i < lReconFWS.Count; i++)
                {
                    tReconFWSAuto[i].bBandWidth = (byte)Math.Round(lReconFWS[i].Deviation);
                    tReconFWSAuto[i].bLevelLinked = (lReconFWS[i]?.ListJamDirect.Count() > 1) ? (byte)(lReconFWS[i]?.ListJamDirect[1]?.JamDirect?.Level * -1) : (byte)0;
                    tReconFWSAuto[i].bLevelOwn = (byte)(lReconFWS[i]?.ListJamDirect[0]?.JamDirect?.Level * -1);
                    tReconFWSAuto[i].bModulation = lReconFWS[i].Type;
                    tReconFWSAuto[i].bSignAudio = 0;
                    tReconFWSAuto[i].iFreq = (int)lReconFWS[i].FreqKHz;
                    tReconFWSAuto[i].iID = lReconFWS[i].Id;
                    tReconFWSAuto[i].sBearingLinked = (lReconFWS[i]?.ListJamDirect.Count() > 1) ? (short)lReconFWS[i]?.ListJamDirect[1]?.JamDirect?.Bearing : (short)-1;
                    tReconFWSAuto[i].sBearingOwn = (short)lReconFWS[i]?.ListJamDirect[0]?.JamDirect?.Bearing;
                    ConvertCoord convertCoord = new ConvertCoord();

                    CoordDegMinSec coordDegMinSec = new CoordDegMinSec();

                    coordDegMinSec = convertCoord.DegToDegMinSec(
                        Math.Abs(lReconFWS[i].Coordinates.Latitude),
                         Math.Abs(lReconFWS[i].Coordinates.Longitude));
                    tReconFWSAuto[i].tCoord.bDegreeLatitude = (byte)coordDegMinSec.Lat[0].LatDeg;
                    tReconFWSAuto[i].tCoord.bDegreeLongitude = (byte)coordDegMinSec.Lon[0].LonDeg;
                    tReconFWSAuto[i].tCoord.bMinuteLatitude = (byte)coordDegMinSec.Lat[0].LatMin;
                    tReconFWSAuto[i].tCoord.bMinuteLongitude = (byte)coordDegMinSec.Lon[0].LonMin;
                    tReconFWSAuto[i].tCoord.bSecondLatitude = (byte)coordDegMinSec.Lat[0].LatSec;
                    tReconFWSAuto[i].tCoord.bSecondLongitude = (byte)coordDegMinSec.Lon[0].LonSec;

                    tReconFWSAuto[i].tCoord.bSignLongitude = (byte)((lReconFWS[i].Coordinates.Longitude >= 0) ? 0 : -1);
                    tReconFWSAuto[i].tCoord.bSignLatitude = (byte)((lReconFWS[i].Coordinates.Latitude >= 0) ? 0 : -1);

                    DateTime dTime = DateTime.Now;
                    tReconFWSAuto[i].tTime.bHour = (byte)dTime.Hour;
                    tReconFWSAuto[i].tTime.bMinute = (byte)dTime.Minute;
                    tReconFWSAuto[i].tTime.bSecond = (byte)dTime.Second;
                }
                bool BerezinaComPort = isBerezinaComPort();
                if (BerezinaComPort)
                {
                    if (tReconFWSAuto.Length > varPU.NumPacket)
                    {
                        do
                        {
                            StructTypeGR_BRZ.TReconFWS[] Buff = new StructTypeGR_BRZ.TReconFWS[varPU.NumPacket];
                            Array.Copy(tReconFWSAuto, Buff, varPU.NumPacket);
                            Array.Reverse(tReconFWSAuto);
                            Array.Resize(ref tReconFWSAuto, tReconFWSAuto.Length - varPU.NumPacket);
                            Array.Reverse(tReconFWSAuto);
                            ClientPu.SendReconFWS(0, Buff);
                            await Task.Run(async () =>
                            {
                                await Task.Delay(varPU.TimePeriod);
                            });
                        }
                        while (tReconFWSAuto.Length > varPU.NumPacket);
                    }
                    if (tReconFWSAuto.Length > 0)
                        ClientPu.SendReconFWS(0, tReconFWSAuto);
                }
                else
                {
                    ClientPu.SendReconFWS(0, tReconFWSAuto);
                }
            }
            catch { }
        }

        //Текстовое сообщение получено
        private void Receive_TextCmd(object sender, string strText)
        {
            try
            {
                List<UserControl_Chat.Message> messages = new List<UserControl_Chat.Message>();
                UserControl_Chat.Message message = new UserControl_Chat.Message()
                {
                    Id = 0,
                    MessageFiled = strText,
                    IsSendByMe = UserControl_Chat.Roles.Received,
                    IsTransmited = true,
                };
                messages.Add(message);
                DispatchIfNecessary(() =>
                {
                    newWindow.curChat.DrawMessageToChat(messages);
                });

                if (ClientPu != null)
                    ClientPu.SendConfirmText(0);
            }
            catch { }
        }

        //Отправка текстового сообщения на ПУ
        private void Events_OnSendStationsMessage1(List<Message> stationsMessages)
        {
            Message message = stationsMessages.Where(x => x.Id == 0).FirstOrDefault();

            ClientPu?.SendText(message.MessageFiled);
        }

        //Подтверждение получения текстового сообщения от ПУ 
        private void Receive_ConfirmTextCmd(object sender, byte bCodeError)
        {
            /*try
            {
                string strName = CMD_ConfirmText + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";"";                   
                string strDetail = "Код ошибки  " + bCodeError.ToString();

                if (rtbHistoryPC.InvokeRequired)
                {
                    rtbHistoryPC.Invoke((MethodInvoker)(delegate()
                    {
                        rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                        showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", "\n");
                    }));

                }
                else
                {
                    rtbHistoryPC.SelectionAlignment = HorizontalAlignment.Right;
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    showLogJammer.ShowMessage(rtbHistoryPC, 0, "", "", "\n");
                }
            }
            catch (SystemException)
            { }*/
        }

        //Синхронизация времени
        private void Receive_RequestSynchTime(object sender, StructTypeGR_BRZ.TTimeMy tTime)
        {
            try
            {
                SYSTEMTIME time = new SYSTEMTIME();
                DateTime DateUtc = DateTime.UtcNow;
                short utcHour = (short)(DateTime.Now.Hour - DateTime.UtcNow.Hour);
                short utcMin = (short)(DateTime.Now.Minute - DateTime.UtcNow.Minute);
                //получаем текущее время
                GetSystemTime(ref time);

                time.wHour = (short)(tTime.bHour - utcHour);
                time.wMinute = (short)(tTime.bMinute - utcMin);
                if (time.wMinute < 0)
                {
                    time.wMinute = (short)(60 + time.wMinute);
                    time.wHour--;
                }
                if (time.wMinute >= 60)
                {
                    time.wMinute = (short)(time.wMinute - 60);
                    time.wHour++;
                }
                if (time.wHour < 0) { time.wHour = (short)(24 + time.wHour); }
                if (time.wHour > 23) { time.wHour = (short)(time.wHour - 24); }
                time.wSecond = tTime.bSecond;

                //DateAndTime.TimeOfDay = DateTime.Now.AddSeconds(3000).AddMilliseconds(80);
                //устанавливаем новые значения
                if (!SetSystemTime(ref time))
                {
                    //получаем текущее время
                    GetSystemTime(ref time);
                    tTime.bHour = (byte)(time.wHour + utcHour);
                    tTime.bMinute = (byte)(time.wMinute + utcMin);
                    tTime.bSecond = (byte)utcHour;// time.wSecond;

                    if (ClientPu != null)
                        ClientPu.SendSynchTime(1, tTime);
                }
                else
                {
                    if (ClientPu != null)
                        ClientPu.SendSynchTime(0, tTime);
                }

            }
            catch (SystemException) { }
        }

        byte RegimeToPU(byte bRegime)
        {
            switch (bRegime)
            {
                case 0:
                    break;
                case 1:
                case 2:
                    bRegime = 1;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    bRegime = 2;
                    break;
            }
            return bRegime;
        }
        byte RegimeToARM(byte bRegime)
        {
            switch (bRegime)
            {
                case 0:
                    break;
                case 1:
                    bRegime = 2;
                    break;
                case 2:
                    bRegime = (byte)(3 + basicProperties.Global.TypeRadioSuppr);
                    break;
            }
            return bRegime;
        }

        MainPanel.MPanel.Buttons RegimeToButtons(byte bRegime)
        {
            switch (bRegime)
            {
                case 0: return MainPanel.MPanel.Buttons.Preparation;
                case 1: return MainPanel.MPanel.Buttons.RadioIntelligence;
                case 3: return MainPanel.MPanel.Buttons.RadioSuppression;
                default: return MainPanel.MPanel.Buttons.Preparation;
            }
        }

        /////Изменение режима
        private void Receive_RequestRegime(object sender, byte bRegime)
        {
            byte ErrorCode = 0;
            //int Regim = RegimeToARM(bRegime);

            //Protocols.ModeMessage modeMessage = new Protocols.ModeMessage(new Protocols.MessageHeader(), (DspDataModel.Tasks.DspServerMode)Regim);
            //Dsp_ModeMessageUpdate(modeMessage);

            DispatchIfNecessary(() =>
            {
                MPanel_OnEnumButtonClick(this, RegimeToButtons(bRegime));
            });

            if (ClientPu != null)
            {
                ClientPu.SendRegime(ErrorCode);
            }
        }

        private void Receive_RequestRangeSpec0(object sender, byte bType, StructTypeGR_BRZ.TRangeSpec[] tRangeSpec)
        {
            byte ErrorCode = 0;
            try
            {
                switch (bType)
                {
                    case 0: // Запрещенные частоты
                        for (int i = 0; i < tRangeSpec.Length; i++)
                        {
                            if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                            {
                                ErrorCode = 1;
                            }
                            else
                            {
                                TableFreqSpec tableFreqSpec = new TableFreqSpec();
                                tableFreqSpec.NumberASP = PropNumberASP.SelectedNumASP;
                                tableFreqSpec.FreqMinKHz = tRangeSpec[i].iFregMin / 10;
                                tableFreqSpec.FreqMaxKHz = tRangeSpec[i].iFregMax / 10;
                                tableFreqSpec.Note = "Post Control";
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqForbidden()));
                            }
                        }
                        break;

                    case 1: //Известные частоты
                        for (int i = 0; i < tRangeSpec.Length; i++)
                        {
                            if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                            {
                                ErrorCode = 1;
                            }
                            else
                            {
                                TableFreqSpec tableFreqSpec = new TableFreqSpec();
                                tableFreqSpec.NumberASP = PropNumberASP.SelectedNumASP;
                                tableFreqSpec.FreqMinKHz = tRangeSpec[i].iFregMin;
                                tableFreqSpec.FreqMaxKHz = tRangeSpec[i].iFregMax;
                                tableFreqSpec.Note = "Post Control";
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqKnown()));
                            }
                        }
                        break;

                    case 2: //Важные частоты 
                        for (int i = 0; i < tRangeSpec.Length; i++)
                        {
                            if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                            {
                                ErrorCode = 1;
                            }
                            else
                            {
                                TableFreqSpec tableFreqSpec = new TableFreqSpec();
                                tableFreqSpec.NumberASP = PropNumberASP.SelectedNumASP;
                                tableFreqSpec.FreqMinKHz = tRangeSpec[i].iFregMin;
                                tableFreqSpec.FreqMaxKHz = tRangeSpec[i].iFregMax;
                                tableFreqSpec.Note = "Post Control";
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqImportant()));
                            }
                        }
                        break;
                }

            }
            catch (Exception e)
            {
                ErrorCode = 2;
            }

            if (ClientPu != null)
                ClientPu.SendRangeSpec(ErrorCode, bType);

        }

        //Специальные частоты от Березины в АРМ
        private void Receive_RequestRangeSpec(object sender, byte bType, StructTypeGR_BRZ.TRangeSpec[] tRangeSpec)
        {
            byte ErrorCode = 0;
            try
            {
                for (int i = 0; i < tRangeSpec.Length; i++)
                {
                    if (tRangeSpec[i].iFregMin < FreqMin_RP_BRZ || tRangeSpec[i].iFregMax > FreqMax_RP_BRZ) // Проверка попадания в допустимый диапазон РП
                    {
                        ErrorCode = 1;
                    }
                    else
                    {
                        TableFreqSpec tableFreqSpec = new TableFreqSpec();
                        tableFreqSpec.NumberASP = PropNumberASP.SelectedNumASP;
                        tableFreqSpec.FreqMinKHz = tRangeSpec[i].iFregMin / 10;
                        tableFreqSpec.FreqMaxKHz = tRangeSpec[i].iFregMax / 10;
                        tableFreqSpec.Note = "Post Control";

                        switch (bType)
                        {
                            case 0: // Запрещенные частоты
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqForbidden()));
                                break;
                            case 1: //Известные частоты
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqKnown()));
                                break;
                            case 2: //Важные частоты 
                                OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqImportant()));
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ErrorCode = 2;
            }

            if (ClientPu != null)
                ClientPu.SendRangeSpec(ErrorCode, bType);
        }

        //Назначение ФРЧ на РП
        private void Receive_RequestSupprFWS(object sender, int iDuration, StructTypeGR_BRZ.TSupprFWS[] tSupprFWS)
        {
            byte ErrorCode = 0;
            try
            {
                for (int i = 0; i < tSupprFWS.Length; i++)
                {
                    if (tSupprFWS[i].iFreq < FreqMin_RP_BRZ || tSupprFWS[i].iFreq > FreqMax_RP_BRZ)
                    {
                        ErrorCode = 1;
                    }
                    else
                    {
                       int Threshold = (tSupprFWS[i].bThreshold * -1);
                       TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                        {
                            Id = 0,
                            Sender = SignSender.PC,
                            NumberASP = 0,
                            FreqKHz = tSupprFWS[i].iFreq / 10d,
                            Bearing = tSupprFWS[i].sBearing,
                            Letter = TableOperations.DefinitionParams.DefineLetter(tSupprFWS[i].iFreq),
                            Threshold = (short)Threshold,
                            Priority = tSupprFWS[i].bPrioritet,
                            Coordinates = new Coord
                            {
                                Latitude = -1,
                                Longitude = -1,
                                Altitude = -1
                            },
                            InterferenceParam = new InterferenceParam
                            {
                                Manipulation = tSupprFWS[i].bManipulation,
                                Modulation = tSupprFWS[i].bModulation,
                                Deviation = tSupprFWS[i].bDeviation,
                                Duration = tSupprFWS[i].bDuration //4
                            },
                        };

                        UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    }
                }
            }
            catch (SystemException)
            { ErrorCode = 2; }

            if (ClientPu != null)
                ClientPu.SendSupprFWS(ErrorCode);
        }

        //Запрос состояния
        private void Receive_RequestState(object sender)
        {
            try
            {
                byte bCodeError = 0;

                byte bRegime = 0;

                short sNum = 0;

                byte bType = 0;

                byte bRole = 0;

                int ind = lASP.FindIndex(x => x.ISOwn == true);
                if (ind != -1)
                {
                    bRegime = TableModeToRegimePU((byte)lASP[ind].Mode);

                    sNum = (short)lASP[ind].Id;

                    bRole = (byte)lASP[ind].Role;
                }

                byte[] bLetter = new byte[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };

                if (ClientPu != null)
                    ClientPu.SendState(bCodeError, bRegime, sNum, bType, bRole, bLetter);
            }
            catch
            {

            }
        }

        byte TableModeToRegimePU(byte bRegime)
        {
            switch (bRegime)
            {
                case 0:
                    bRegime =0;
                    break;
                case 1:
                    bRegime = 1;
                    break;
                case 2:
                    bRegime = 3;
                    break;
            }
            return bRegime;
        }

        //Сектора и диапазоны
        private void Receive_RequestRangeSector(object sender, byte bType, StructTypeGR_BRZ.TRangeSector[] tRangeSector)
        {
            byte ErrorCode = 0;
            try
            {
                List<TableSectorsRanges> tableSectorsRanges = new List<TableSectorsRanges>();

                for (int i = 0; i < tRangeSector.Length; i++)
                {
                    if (tRangeSector[i].iFregMin < FreqMin_RR_BRZ || tRangeSector[i].iFregMax > FreqMax_RR_BRZ)
                    {
                        ErrorCode = 1;
                    }
                    else
                    {
                        TableSectorsRanges tableSectorsRange = new TableSectorsRanges()
                        {
                            FreqMinKHz = tRangeSector[i].iFregMin / 10,
                            FreqMaxKHz = tRangeSector[i].iFregMax / 10,
                            AngleMin = tRangeSector[i].sAngleMin,
                            AngleMax = tRangeSector[i].sAngleMax
                        };

                        tableSectorsRanges.Add(tableSectorsRange);
                    }
                }

                object Ranges = new object();
                for (int i = 0; i < tableSectorsRanges.Count; i++)
                {
                    tableSectorsRanges[i].IsCheck = true;
                    tableSectorsRanges[i].NumberASP = PropNumberASP.SelectedNumASP;
                }

                switch (bType)
                {
                    case 0:  // Радиоразведка                        

                        Ranges = (from t in tableSectorsRanges let c = t.ToRangesRecon() select c).ToList();
                        clientDB.Tables[NameTable.TableSectorsRangesRecon].AddRange(Ranges);

                        break;

                    case 1: // Радиоподавление

                        Ranges = (from t in tableSectorsRanges let c = t.ToRangesSuppr() select c).ToList();
                        clientDB.Tables[NameTable.TableSectorsRangesSuppr].AddRange(Ranges);

                        break;
                }
            }
            catch (SystemException) { ErrorCode = 2; }

            if (ClientPu != null)
                ClientPu.SendRangeSector(ErrorCode, bType);
        }

        //Запрос на исполнительное пеленгование
        async void ClientPu_OnRequestExecBear(object sender, int iID, int iFreq)
        {
            byte ErrorCode = 0;
            iFreq = iFreq / 10;
            int iFmin = 0;
            int iFmax = 0;
            Protocols.ExecutiveDFResponse answer;
            try
            {
                iFmin = (iFreq - 250);
                iFmax = (iFreq + 250);
                answer = await dsp.ExecutiveDfRequest(iFmin, iFmax, 3);
                ClientPu.SendExecBear(ErrorCode, iID, answer.Frequency, (short)(answer.Direction == -1 ? (361) : Math.Round(answer.Direction / 10.0d)));
            }
            catch
            {
                ErrorCode = 1;
                ClientPu.SendExecBear(ErrorCode, 0, 0, 0);
            }
        }

        //Запрос на квазиодновременное пеленгование
        async void ClientPu_OnRequestSimulBear(object sender, int iID, int iFreq)
        {
            byte ErrorCode = 0;
            int iFmin = 0;
            int iFmax = 0;
            Protocols.QuasiSimultaneouslyDFResponse answer;
            try
            {
                iFmin = iFreq - 250;
                iFmax = iFreq + 250;
                answer = await dsp.QuasiSimultaneouslyDFX10(iFmin, iFmax, 3, 3);

                short Direction2 = -1;

                if (answer?.Header.ErrorCode == 0)
                {
                    if (answer.Source.Direction != -1)
                    {
                        if (answer.LinkedStationResults.Count() > 0)
                        {
                            Direction2 = (short)(answer.LinkedStationResults[0].Direction * 10);
                        }
                    }
                }

                ClientPu.SendSimulBear(ErrorCode, iID, answer.Source.Frequency, (short)(answer.Source.Direction == -1 ? (361) : Math.Round(answer.Source.Direction / 10.0d)),
                    (short)(Direction2 == -1 ? (361) : Math.Round(Direction2 / 10.0d)));
            }
            catch
            {
                ErrorCode = 1;
                ClientPu.SendSimulBear(ErrorCode, 0, 0, 0, 0);
            }

        }

        #endregion
    }
}
