﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        //FILETYPE FOR CRRX_1
        private string CurrentFileType
        {
            get => basicProperties.Local.Common.FileType.ToString();
            set
            {
                ReceiverTableFirst.FormatFile = value;
                ReceiverTable.FormatFile = value;
                ReceiverTableSec.FormatFile = value;
                ReceiverTableArOneSec.FormatFile = value;
            }
        }

        private void ReceiverTableFirst_OnUpdateFileType()
        {
            ReceiverTableFirst.FormatFile = CurrentFileType;
        }

        private void OnUpdateFileType()
        {
            ReceiverTable.FormatFile = CurrentFileType;
        }

        //FILETYPE FOR CRRX_2
        private void ReceiverTableSec_OnUpdateFileType()
        {
            ReceiverTableSec.FormatFile = CurrentFileType;
        }

        private void ReceiverTableArOneSec_OnUpdateFileType()
        {
            ReceiverTableArOneSec.FormatFile = CurrentFileType;
        }
    }
}
