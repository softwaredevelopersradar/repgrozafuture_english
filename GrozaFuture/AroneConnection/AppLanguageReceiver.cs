﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void ChangeLangugeReceiverControls()
        {
            var localLanguage = basicProperties.Local.Common.Language.ToString();
            AroneConnetion.Language(localLanguage);
            ReceiverTable.Language(localLanguage);

            ControlAR6000First.Language(localLanguage);
            ReceiverTableFirst.Language(localLanguage);

            ControlArOneSec.Language(localLanguage);
            ReceiverTableArOneSec.Language(localLanguage);

            ControlAR6000Sec.Language(localLanguage);
            ReceiverTableSec.Language(localLanguage);
        }
    }
}
