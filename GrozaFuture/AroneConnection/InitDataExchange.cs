﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void InitReceiversDataExchange() 
        {
            //AroneConnetion = new WpfControlLibraryBut.UserControl1();
            //ControlArOneSec = new WpfControlLibraryBut.UserControl1();
            AroneConnetion.FrequencyChanged += AroneConnetion_FrequencyChanged;
            ControlArOneSec.FrequencyChanged += ControlArOneSec_FrequencyChanged;

            ControlAR6000First.FrequencyChanged += ControlAR6000First_FrequencyChanged;
            ControlAR6000Sec.FrequencyChanged += ControlAR6000Sec_FrequencyChanged;
        }

        private void ControlAR6000Sec_FrequencyChanged(object sender, long ChangedFreq, double ChangedBw)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.CursorCRRChange(ChangedFreq / 1000000d, ChangedBw / 1000d, PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver2);
            });
        }

        private void ControlAR6000First_FrequencyChanged(object sender, long ChangedFreq, double ChangedBw)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.CursorCRRChange(ChangedFreq / 1000000d, ChangedBw / 1000d, PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver);
            });
        }

        private void ControlArOneSec_FrequencyChanged(object sender, long ChangedFreq, double ChangedBw)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.CursorCRRChange(ChangedFreq / 1000000d, ChangedBw / 1000d, PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver2);
            });
        }

        private void AroneConnetion_FrequencyChanged(object sender, long ChangedFreq, double ChangedBw)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.CursorCRRChange(ChangedFreq / 1000000d, ChangedBw / 1000d, PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver);
            });
        }
    }
}
