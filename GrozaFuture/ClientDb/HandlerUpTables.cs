﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using ClientDataBase;
using ModelsTablesDBLib;
using InheritorsEventArgs;
using TableEvents;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using UserControl_Chat;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void HandlerError_ClientDb(object sender, OperationTableEventArgs e)
        {
            MessageBox.Show(e.GetMessage);
        }

        private void HandlerUpdate_TableMission(object sender, TableEventArs<TableMission> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                MessageBox.Show($"Пришла таблица TableMission кол-во строк: {e.Table.Count}");
            });
        }

        private void HandlerUpdate_TableASP(object sender, TableEventArs<TableASP> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableASP кол-во строк: {e.Table.Count}");
                lASP = e.Table;
                ucASP.UpdateASPs(lASP);
                UpdateSideMenu(lASP);
                UpdateTableASP4MainPanel(lASP);

                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));

                /////////////////////////////////////////////
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
                /////////////////////////////////////////////
            });
        }

        private void HandlerUpdate_TableSectorRangesRecon(object sender, TableEventArs<TableSectorsRangesRecon> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableSectorsRangesRecon кол-во строк: {e.Table.Count}");
                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList();
                lSRangeRecon = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();

                ucSRangesRecon.UpdateSRanges(lSRangeRecon);

                PanoramaPreImportRIFreqs(e);
            });
        }

        private void HandlerUpdate_TableSectorRangesSuppr(object sender, TableEventArs<TableSectorsRangesSuppr> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableSectorsRangesSuppr кол-во строк: {e.Table.Count}");

                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList();
                //List<TableSectorsRanges> listSRanges = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                //ucSRangesSuppr.UpdateSRanges(listSRanges);
                //PanoramaImportRSFreqs(listSRanges);

                lSRangeSuppr = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSRangesSuppr.UpdateSRanges(lSRangeSuppr);

                PanoramaPreImportRSFreqs(e);
            });
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, TableEventArs<TableFreqForbidden> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableFreqForbidden кол-во строк: {e.Table.Count}");

                //ucSpecFreqForbidden.UpdateSpecFreqs((from t in e.Table let a = t as TableFreqSpec select a).ToList());
                lSpecFreqForbidden = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqForbidden.UpdateSpecFreqs(lSpecFreqForbidden);

                PanoramaPreImportSpecFreqs(e);
            });
        }

        private void HandlerUpdate_TableFreqImportant(object sender, TableEventArs<TableFreqImportant> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableFreqImportant кол-во строк: {e.Table.Count}");

                //ucSpecFreqImportant.UpdateSpecFreqs((from t in e.Table let a = t as TableFreqSpec select a).ToList());
                lSpecFreqImportant = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqImportant.UpdateSpecFreqs(lSpecFreqImportant);

                PanoramaPreImportSpecFreqs(e);
            });
        }

        private void HandlerUpdate_TableFreqKnown(object sender, TableEventArs<TableFreqKnown> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //MessageBox.Show($"Пришла таблица TableFreqKnown кол-во строк: {e.Table.Count}");

                //ucSpecFreqKnown.UpdateSpecFreqs((from t in e.Table let a = t as TableFreqSpec select a).ToList());
                lSpecFreqKnown = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSpecFreqKnown.UpdateSpecFreqs(lSpecFreqKnown);

                PanoramaPreImportSpecFreqs(e);
            });
        }

        private void HandlerUpdate_TempWFS(object sender, TableEventArs<TempFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //ucTemsFWS.UpdateTempFWS(e.Table);
                ucTemsFWS.UpdateTempFWS(e.Table, lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ucTemsFWS.AddToStatusBarCountFI(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList().Count);
            });
        }

        private void HandlerChangeFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ucTemsFWS.ChangeTempFWS(e.Id, e);
                ucTemsFWS.AddToStatusBarCountFI(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList().Count);
            });
        }

        private void HandlerAddFWS(object sender, TempFWS e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ucTemsFWS.AddTempFWSs(new List<TempFWS>() { e }, lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ucTemsFWS.AddToStatusBarCountFI(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList().Count);
            });
        }
        
        private void HandlerAddRangeFWS(object sender, TableEventArs<TempFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (e.Table.Count > 0)
                {
                    ucTemsFWS.AddTempFWSs(ExcludeKnownForbiddenFreqs(e.Table), lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                    //ucTemsFWS.AddTempFWSs(ExcludeKnownForbiddenFreqs(e.Table));
                    //ucTemsFWS.AddTempFWSs(ExcludeKnownFreq(e.Table));
                    ucTemsFWS.ColorFreqImportantForbidden(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList(), lSpecFreqForbidden.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                    ucTemsFWS.AddToStatusBarCountFI(lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList().Count);
                }
            });
        }

        private void HandlerUpdate_GlobalProperties(object sender, TableEventArs<GlobalProperties> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                var globalProperties = e.Table.FirstOrDefault();
                basicProperties.Global = globalProperties;
                UpdatePanoramaManager(globalProperties);
                UpdateGlobalProperties4MainPanel(globalProperties);
                UpdateGlobalProperties4LeftRIButtons(globalProperties);
                UpdateSpoofCoord(globalProperties);
            });
        }

        private void HandlerUpdate_TableSuppressFWS(object sender, TableEventArs<TableSuppressFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSuppressFWS = e.Table;
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ListFormer(lSuppressFWS, null);

                //ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS);
            });
        }

        private void HandlerUpdate_TempSuppressFWS(object sender, TableEventArs<TempSuppressFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                List<TempSuppressFWS> listTempSuppressFWS = e.Table;
                ucSuppressFWS.UpdateRadioJamState(listTempSuppressFWS);
                PanoramaUpdateTempSuppressFWS(listTempSuppressFWS);
                UpdateRadioJamStateStructForBRZ(listTempSuppressFWS);
                //this.ListStateSuppFWS = e.Table;
            });
        }

        private void HandlerUpdate_TableReconFWS(object sender, TableEventArs<TableReconFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFWS = e.Table;
                ucReconFWS.UpdateReconFWS(lReconFWS);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
            });
        }

        private void HandlerAddRangeReconFWS(object sender, TableEventArs<TableReconFWS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                ///////////////////////////////////////////
                //lReconFWS = e.Table;
                //lReconFWS.AddRange(e.Table);
                //ucReconFWS.AddReconFWSs(lReconFWS);
                ///////////////////////////////////////////
                for (int i = 0; i < e.Table.Count; i++)
                {
                    int ind = lReconFWS.FindIndex(x => x.Id == e.Table[i].Id);
                    if (ind != -1)
                    {
                        lReconFWS[ind] = e.Table[i];
                    }
                }
                //ucReconFWS.AddReconFWSs(e.Table);
                ucReconFWS.UpdateReconFWS(lReconFWS);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);
                ///////////////////////////////////////////
                //lReconFWS = e.Table;
                //ucReconFWS.UpdateReconFWS(lReconFWS);
                //ucReconFWS.UpdateASPRP(lASP, lReconFWS);

            });
        }

        private void HandlerUpdate_TableSuppressFHSS(object sender, TableEventArs<TableSuppressFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSuppressFHSS = (from t in e.Table let a = t as TableSuppressFHSS select a).ToList().Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);

                PanoramaUpdateSuppressFHSS();

                ListFormer(lSuppressFWS, null);
            });
        }

        private void HandlerUpdate_TableFHSSExcludedFreq(object sender, TableEventArs<TableFHSSExcludedFreq> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFHSSExcludedFreq = e.Table;
                ucSuppressFHSS.UpdateFHSSExcludedFreq(lFHSSExcludedFreq);

                PanoramaUpdateSuppressFHSS();
            });
        }

        private void HandlerUpdate_TempSuppressFHSS(object sender, TableEventArs<TempSuppressFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                List<TempSuppressFHSS> listTempSuppressFHSS = e.Table;
                ucSuppressFHSS.UpdateRadioJamState(listTempSuppressFHSS);
            });
        }

        private void HandlerUpdate_TableReconFHSS(object sender, TableEventArs<TableReconFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFHSS = e.Table;
                ucReconFHSS.UpdateReconFHSS(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
            });
        }

        private void HandlerAddRangeReconFHSS(object sender, TableEventArs<TableReconFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lReconFHSS = e.Table;
                ucReconFHSS.AddReconFHSSs(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
            });
        }

        private void HandlerUpdate_TableSourceFHSS(object sender, TableEventArs<TableSourceFHSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lSourceFHSS = e.Table;
                ucReconFHSS.UpdateSourceFHSS(e.Table);
            });
        }

        private void HandlerUpdate_TableFHSSReconExcluded(object sender, TableEventArs<TableFHSSReconExcluded> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lFHSSReconExcluded = e.Table;
                ucReconFHSS.UpdateTableFHSSReconExcluded(lFHSSReconExcluded);
            });
        }

        private void HandlerUpdate_TempGNSS(object sender, TableEventArs<TempGNSS> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                var tempGNSS = e.Table.FirstOrDefault();

                if (tempGNSS != null)
                {
                    tempGNSS.Location.Latitude = Math.Abs(Math.Round(tempGNSS.Location.Latitude, 6));
                    tempGNSS.Location.Longitude = Math.Abs(Math.Round(tempGNSS.Location.Longitude, 6));
                    tempGNSS.Location.Altitude = Math.Round(tempGNSS.Location.Altitude, 0);

                    basicProperties.Local.CoordinatesProperty.CoordGPS = tempGNSS.Location;
                    basicProperties.Local.CoordinatesProperty.CompassPA = tempGNSS.CmpPA;

                    if (basicProperties.Local.CoordinatesProperty.CompassRR!= tempGNSS.CmpRR)
                    {
                        basicProperties.Local.CoordinatesProperty.CompassRR = tempGNSS.CmpRR;
                    }
                    ChangeCourseAngle((int)Math.Round(tempGNSS.CmpRR, MidpointRounding.ToEven));
                    
                    try
                    {
                        TableASP temp = ucASP.UpdateRowASPsGNSS(lASP, tempGNSS.Location, basicProperties.Global.GnssInaccuracy);
                        if (temp != null)
                        {
                            clientDB?.Tables[NameTable.TableASP].Change(temp);
                        }
                        //ucASP.UpdateASPsGNSS(lASP, tempGNSS.Location, basicProperties.Global.GnssInaccuracy);
                    }
                    catch { }
                }
            });
        }


        private void HandlerUpdate_TableChat(object sender, TableEventArs<TableChatMessage> e)
        {
            try
            {
                lChatMessages = new List<TableChatMessage>(e.Table);
                //var messages = new List<UserControl_Chat.Message>();
                
                //messages = e.Table.OrderBy(t => t.Time).Select(s => new UserControl_Chat.Message()
                //{
                //    Id = s.ReceiverAddress == ownAddress ? s.SenderAddress : s.ReceiverAddress,
                //    MessageFiled = s.Text,
                //    IsSendByMe = s.ReceiverAddress == ownAddress ? UserControl_Chat.Roles.Received : Roles.SentByMe,
                //    IsTransmited = s.Status == ChatMessageStatus.Delivered,
                //}).ToList();


                //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                //{
                //    newWindow.curChat.DrawMessageToChat(messages);
                //});

                ////if (ClientPu != null)
                ////    ClientPu.SendConfirmText(0);
            }
            catch { }
        }

        private void Load_TableChat(List<TableChatMessage> e)
        {
            try
            {
                //lChatMessages = new List<TableChatMessage>(e.Table);
                var messages = new List<UserControl_Chat.Message>();
                var chatIds = GetSideMenu();
                messages = e.OrderBy(t => t.Time).Where(t => chatIds.Contains(t.ReceiverAddress) || chatIds.Contains(t.SenderAddress))
                    .Select(s => new UserControl_Chat.Message()
                                                                        {
                                                                            Id = s.ReceiverAddress == ownAddress ? s.SenderAddress : s.ReceiverAddress,
                                                                            MessageFiled = s.Text,
                                                                            IsSendByMe = s.ReceiverAddress == ownAddress ? UserControl_Chat.Roles.Received : Roles.SentByMe,
                                                                            IsTransmited = s.Status == ChatMessageStatus.Delivered,
                                                                        }).ToList();


                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        newWindow.curChat.ClearChatHistory(GetSideMenu());
                        newWindow.curChat.DrawMessageToChat(messages);
                    });

                //if (ClientPu != null)
                //    ClientPu.SendConfirmText(0);
            }
            catch { }
        }



        private async void LoadTables()
        {
            try
            {
                lASP = await clientDB.Tables[NameTable.TableASP].LoadAsync<TableASP>();
                ucASP.UpdateASPs(lASP);
                UpdateSideMenu(lASP);
                UpdateTableASP4MainPanel(lASP);

                lSRangeRecon = await clientDB.Tables[NameTable.TableSectorsRangesRecon].LoadAsync<TableSectorsRanges>();
                lSRangeSuppr =  await clientDB.Tables[NameTable.TableSectorsRangesSuppr].LoadAsync<TableSectorsRanges>();

                ucSRangesRecon.UpdateSRanges(lSRangeRecon.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ucSRangesSuppr.UpdateSRanges(lSRangeSuppr.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqForbidden = await clientDB.Tables[NameTable.TableFreqForbidden].LoadAsync<TableFreqSpec>();
                ucSpecFreqForbidden.UpdateSpecFreqs((lSpecFreqForbidden).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqImportant = await clientDB.Tables[NameTable.TableFreqImportant].LoadAsync<TableFreqSpec>();
                ucSpecFreqImportant.UpdateSpecFreqs((lSpecFreqImportant).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSpecFreqKnown = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<TableFreqSpec>();
                ucSpecFreqKnown.UpdateSpecFreqs((lSpecFreqKnown).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());

                lSuppressFWS = await clientDB.Tables[NameTable.TableSuppressFWS].LoadAsync<TableSuppressFWS>();
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                
                lReconFWS = await clientDB.Tables[NameTable.TableReconFWS].LoadAsync<TableReconFWS>();
                ucReconFWS.UpdateReconFWS(lReconFWS);
                ucReconFWS.UpdateASPRP(lASP, lReconFWS);

                lReconFHSS = await clientDB.Tables[NameTable.TableReconFHSS].LoadAsync<TableReconFHSS>();
                ucReconFHSS.UpdateReconFHSS(lReconFHSS);
                ucReconFHSS.UpdateASPRP(UpdateASPRPRecon(lASP));
                lSourceFHSS = await clientDB.Tables[NameTable.TableSourceFHSS].LoadAsync<TableSourceFHSS>();
                ucReconFHSS.UpdateSourceFHSS(lSourceFHSS);
                lFHSSReconExcluded = await clientDB.Tables[NameTable.TableFHSSReconExcluded].LoadAsync<TableFHSSReconExcluded>();
                ucReconFHSS.UpdateTableFHSSReconExcluded(lFHSSReconExcluded);

                ucTemsFWS.UpdateTempFWS(await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>(), lSpecFreqImportant.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                //ucTemsFWS.UpdateTempFWS(await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>());

                var arg = (await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>()).FirstOrDefault();
                basicProperties.Global = arg;

                UpdatePanoramaManager(arg);

                PanoramaPreImportRIFreqs(lSRangeRecon);
                PanoramaPreImportRSFreqs(lSRangeSuppr);

                PanoramaPreImportForbiddenFreqs(lSpecFreqForbidden);
                PanoramaPreImportImportantFreqs(lSpecFreqImportant);
                PanoramaPreImportKnownFreqs(lSpecFreqKnown);

                UpdateGlobalProperties4MainPanel(arg);
                UpdateGlobalProperties4LeftRIButtons(arg);
                UpdateRanges(arg);
               

                UpdateSpoofCoord(arg);
                
                lSuppressFHSS = (await clientDB.Tables[NameTable.TableSuppressFHSS].LoadAsync<TableSuppressFHSS>()).Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList();
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);
                lFHSSExcludedFreq = await clientDB.Tables[NameTable.TableFHSSExcludedFreq].LoadAsync<TableFHSSExcludedFreq>();
                ucSuppressFHSS.UpdateFHSSExcludedFreq(lFHSSExcludedFreq);
                PanoramaUpdateSuppressFHSS();

                try
                {
                    double anglePA = AnglePA;
                    double angleRR = AngleRR;


                    var tempGNSS = (await clientDB?.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();

                    if (anglePA != -1 && angleRR != -1)
                    {
                        basicProperties.Local.CoordinatesProperty.CoordGPS = tempGNSS.Location;
                        basicProperties.Local.CoordinatesProperty.CompassPA = tempGNSS.CmpPA;
                        basicProperties.Local.CoordinatesProperty.CompassRR = tempGNSS.CmpRR;
                        return;
                    }
                    if (tempGNSS != null)
                    {
                        tempGNSS.CmpPA = anglePA == -1 ? tempGNSS.CmpPA : anglePA;
                        tempGNSS.CmpRR = angleRR == -1 ? tempGNSS.CmpRR : angleRR;
                        clientDB.Tables[NameTable.TempGNSS].Change(tempGNSS);
                    }
                    else
                    {
                        TempGNSS addTempGNSS = new TempGNSS();
                        addTempGNSS.CmpPA = anglePA == -1 ? addTempGNSS.CmpPA : anglePA;
                        addTempGNSS.CmpRR = angleRR == -1 ? addTempGNSS.CmpRR : angleRR;

                        clientDB?.Tables[NameTable.TempGNSS].Add(addTempGNSS);
                    }

                }
                catch (Exception)
                { }

                lChatMessages = await clientDB.Tables[NameTable.TableChat].LoadAsync<TableChatMessage>();
                Load_TableChat(this.lChatMessages);
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ClientDataBase.Exceptions.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }

    
}
