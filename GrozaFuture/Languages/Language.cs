﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using ValuesCorrectLib;

namespace GrozaFuture
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void SetResourcelanguage(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/StringResource.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
        public void HandlerLanguageChanged(object sender, Languages language)
        {
            SetResourcelanguage(language);

            SetLanguageTables(language);
            TranslatorTables.LoadDictionary(language);
            ChangeFreqHeaderMHz_kHz();

            DispatchIfNecessary(() =>
            {
                ucSuppressFWS.UpdateSuppressFWS(lSuppressFWS.Where(x => x.NumberASP == PropNumberASP.SelectedNumASP).ToList());
                ucSuppressFHSS.UpdateSuppressFHSS(lSuppressFHSS);
                ucReconFWS.UpdateReconFWS(lReconFWS);
            });

            DispatchIfNecessary(() =>
            {
                SetControlsLanguage();
            });

            //YamlSave(basicProperties.Local);

            DispatchIfNecessary(() =>
            {
                ChangeLangugeReceiverControls();
            });
            ChangeLangVD();
            AudioPlayer.AudioPlayerControl.Language(language.ToString());
            SetLanguageTab(language);
            this.att.SetLanguage(language.ToString());

            SetTitle(language);

            newWindow?.SetLanguage(language);
        }

        private void SetTitle(Languages language)
        {
            switch (language)
            {
                case Languages.Rus:
                    this.Title = "Р-934УМ2";
                    break;
                default:
                    this.Title = "Groza";
                    break;
            }
        }
    }
}
