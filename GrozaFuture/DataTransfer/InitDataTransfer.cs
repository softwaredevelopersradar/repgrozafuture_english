﻿using DataTransferClientLibrary;
using DataTransferModel.DataTransfer;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserControl_Chat;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public DataTransferClient dTClient;

        private void InitDataTransfer()
        {
            //var localProperties = YamlLoad<LocalProperties>("LocalProperties.yaml");
            //dTClient = new DataTransferClient(localProperties.EdServer.IpAddress, localProperties.EdServer.Port);
            dTClient = new DataTransferClient(basicProperties.Local.EdServer.IpAddress, basicProperties.Local.EdServer.Port);

            dTClient.OnMessageReceived += (sender, message) => { DrawMessageToChat(message); };
            dTClient.OnDisconnect += (sender, EventArgs) => { HandlerDisconnect_DTClient(sender); };
            dTClient.OnConnect += (sender, EventArgs) => { HandlerConnect_DTClient(sender); };
            //Events.OnSendStationsMessage += SendMessagesToStations;
        }

        private void DrawMessageToChat(DataTransferModel.DataTransfer.Message curMessage)
        {
            //List<UserControl_Chat.Message> curMessages = new List<UserControl_Chat.Message>();
            //curMessages.Add(new UserControl_Chat.Message
            //{
            //    MessageFiled = curMessage.Text,
            //    Id = curMessage.SenderId,
            //    IsTransmited = true,
            //    IsSendByMe = Roles.Received

            //});

            //DispatchIfNecessary(() =>
            //{
            //    chatBuble.SetMessage(curMessages[0].MessageFiled);
            //    newWindow.curChat.DrawMessageToChat(curMessages);
            //});
            try
            {
                chatBuble?.SetMessage(curMessage.Text);
            }
            catch
            {
            }
            newWindow.DrawReceivedMessage(curMessage.SenderId, curMessage.Text);

            var message = new TableChatMessage() { SenderAddress = curMessage.SenderId, ReceiverAddress = curMessage.ReceiverId, Time = DateTime.Now, Status = ChatMessageStatus.Delivered, Text = curMessage.Text };
            clientDB?.Tables[NameTable.TableChat]?.Add(message);
        }

        //private void SendMessagesToStations(List<UserControl_Chat.Message> stationsMessages)
        //{
        //    foreach (UserControl_Chat.Message curMessage in stationsMessages)
        //        dTClient.SendMessage(curMessage.Id, curMessage.MessageFiled);
        //}
    }
}
