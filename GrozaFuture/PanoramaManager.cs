﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using InheritorsEventArgs;
using ModelsTablesDBLib;
using Protocols;
using TableEvents;
using TableOperations;
using ValuesCorrectLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private int GetSpectrumMode = 0;

        public int ApdComNumber = 1;

        public void InitPanoramaManager()
        {
            pLibrary.ViewMode = 1;
            var panoramaSettings = LoadPanoramaSettings();
            if (panoramaSettings.spectrumSettings.Version == 0)
                pLibrary.ChangeConfigSSR("SSR100.json");
            if (panoramaSettings.spectrumSettings.Version == 1)
                pLibrary.ChangeConfigSSR("SSR200.json");
            pLibrary.RequestSpectrumTimer = panoramaSettings.spectrumSettings.RequestSpectrumTimer;
            pLibrary.SpectrumPointCount = panoramaSettings.spectrumSettings.SpectrumPointCount;
            pLibrary.Decay = panoramaSettings.spectrumSettings.Decay;
            pLibrary.FlagScanSpeed = panoramaSettings.spectrumSettings.FlagScanSpeed;
            pLibrary.FHSSTest = panoramaSettings.spectrumSettings.FHSSTest;
            dsp.AwaiterTime = panoramaSettings.spectrumSettings.AwaiterTime;
            pLibrary.CancellationTokenDelay = panoramaSettings.spectrumSettings.CancellationTokenDelay;
            GetSpectrumMode = panoramaSettings.spectrumSettings.GetSpectrumMode;
            ApdComNumber = panoramaSettings.spectrumSettings.ApdComNumber;
            pLibrary.SavePlotStorageTime = panoramaSettings.spectrumSettings.SavePlotStorageTime;

            pLibrary.NeedSpectrumRequest += PLibrary_NeedSpectrumRequest;

            pLibrary.NeedBearingRequest += PLibrary_NeedBearingRequest;

            pLibrary.NeedIntensityRequest += PLibrary_NeedIntensityRequest;

            //pLibrary.NeedAdaptiveThresholdRequest += PLibrary_NeedAdaptiveThresholdRequest;

            pLibrary.NeedAdaptiveThresholdRequest2 += PLibrary_NeedAdaptiveThresholdRequest2;

            pLibrary.NeedScanSpeedRequest += PLibrary_NeedScanSpeedRequest;

            pLibrary.ThresholdChange += PLibrary_ThresholdChange;

            pLibrary.NeedExBearingRequest += PLibrary_NeedExBearingRequest;
            pLibrary.NeedQBearingRequest += PLibrary_NeedQBearingRequest;

            pLibrary.FreqOnTypeDivece += PLibrary_FreqOnTypeDivece;

            pLibrary.BandOnRS += PLibrary_BandOnRS;

            pLibrary.OnFreqArea += PLibrary_OnFreqArea;

            pLibrary.OnIsNeedChangeView += PLibrary_OnIsNeedChangeView;

            pLibrary.NbarIndex += PLibrary_NbarIndex;

            //InitReceiversDataExchange();
        }

        public void UpdatePanoramaManager(GlobalProperties arg)
        {
            //pLibrary.GlobalNumberOfBands = (arg.RangeRadioRecon == 0) ? 100 : 200;
            PLibraryRangeRadioReconSwither(arg.RangeRadioRecon);
            pLibrary.LettersCount = (int)arg.RangeJamming;
        }

        private void PLibraryRangeRadioReconSwither(EnumRangeRadioRecon enumRangeRadioRecon)
        {
            switch ((int)enumRangeRadioRecon)
            {
                case 0:
                    pLibrary.ChangeConfigSSR("SSR100.json");
                    break;
                case 1:
                    pLibrary.ChangeConfigSSR("SSR200.json");
                    break;
                case 2:
                    pLibrary.ChangeConfigSSR("SSR.json");
                    break;
                default:
                    break;
            }
            PanoramaPreImportRIFreqs(lSRangeRecon);
            PanoramaPreImportRSFreqs(lSRangeSuppr);
        }

        public void UpdatePanoramaManager(LocalProperties arg)
        {
            pLibrary.CRRXVisibleChange(PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver, arg.ARONE1.State);
            pLibrary.CRRXVisibleChange(PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver2, arg.ARONE2.State);
        }

        private void PLibrary_OnIsNeedChangeView(object sender, int ChangeView)
        {
            DispatchIfNecessary(() =>
            {
                PTabControl.SelectedIndex = PanoramaReverseViewConverter(ChangeView);
            });
        }

        private void PTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            pLibrary.ViewMode = PanoramaViewConverter(PTabControl.SelectedIndex);
        }

        private int PanoramaViewConverter(int SelectedIndex)
        {
            switch (SelectedIndex)
            {
                case 0:
                    return 1;
                case 1:
                    return 2;
                case 2:
                    return 4;
                case 3:
                    return 5;
                default:
                    return 1;
            }
        }

        private int PanoramaReverseViewConverter(int ChangeView)
        {
            switch (ChangeView)
            {
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 3:
                case 4:
                    return 2;
                case 5:
                    return 3;
                default:
                    return 0;
            }
        }



        private async void PLibrary_NeedSpectrumRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount)
        {
            try
            {
                var answer = new GetSpectrumResponse();
                if (GetSpectrumMode == 0)
                {
                    answer = await dsp.GetSpectrumOld(MinVisibleX, MaxVisibleX, PointCount);
                }
                if (GetSpectrumMode == 1)
                {
                    answer = await dsp.GetSpectrum(MinVisibleX, MaxVisibleX, PointCount);
                }

                if (answer?.Header.ErrorCode == 0)
                {
                    if (answer.Spectrum != null)
                    {
                        DispatchIfNecessary(() =>
                        {
                            pLibrary.IQSpectrumPainted(MinVisibleX, MaxVisibleX, answer.Spectrum);
                        });
                    }
                }
            }
            catch { }
        }

        private async void PLibrary_NeedBearingRequest(object sender, double MinVisibleX, double MaxVisibleX)
        {
            var answer = await dsp.GetBearingPanoramaSignals(MinVisibleX, MaxVisibleX);

            if (answer?.Header.ErrorCode == 0)
            {
                BearingFromDSPPainted(answer);
            }
        }
        //Отрисовка пеленгов
        private void BearingFromDSPPainted(GetBearingPanoramaSignalsResponse data)
        {
            int[] xData = new int[data.FixedSignalsCount];
            int[] yData = new int[data.FixedSignalsCount];

            for (int i = 0; i < data.FixedSignalsCount; i++)
            {
                xData[i] = data.FixedSignals[i].Frequency;
                yData[i] = data.FixedSignals[i].Direction;
            }

            pLibrary.IQBearingPainted(xData, yData);

            xData = new int[data.ImpulseSignalsCount];
            yData = new int[data.ImpulseSignalsCount];

            for (int i = 0; i < data.ImpulseSignalsCount; i++)
            {
                xData[i] = data.ImpulseSignals[i].Frequency;
                yData[i] = data.ImpulseSignals[i].Direction;
            }

            pLibrary.IQBearingPainted(xData, yData);
        }

        private async void PLibrary_NeedIntensityRequest(object sender, double MinVisibleX, double MaxVisibleX, int PointCount, byte TimeLenght)
        {
            var answer = await dsp.GetAmplitudeTimeSpectrum(MinVisibleX, MaxVisibleX, PointCount, TimeLenght);

            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.IQIntensityPaint(answer.Spectrum, MinVisibleX, MaxVisibleX, PointCount, TimeLenght);
            }
        }

        private async void PLibrary_NeedAdaptiveThresholdRequest(object sender)
        {
            var answer = await dsp.GetAdaptiveThreshold(0);
            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.AdaptiveThreshold = answer.ThresholdLevel;
            }
        }

        private async void PLibrary_NeedAdaptiveThresholdRequest2(object sender, byte[] Bands)
        {
            var answer = await dsp.GetBandsAdaptiveThreshold(Bands);
            if (answer?.Header.ErrorCode == 0)
            {
                pLibrary.AdaptiveBandsPaint(Bands, answer.Thresholds);
            }
        }

        private async void PLibrary_NeedScanSpeedRequest(object sender)
        {
            var answer = await dsp.GetScanSpeed();
            pLibrary.ScanSpeed = answer.FpgaScanSpeed;
            pLibrary.ScanSpeed2 = answer.UsrpScanSpeed;
        }

        private async void PLibrary_ThresholdChange(object sender, int value)
        {
            var answer = await dsp.SetFilters(value, 0, 0, 0);
        }

        private async void PLibrary_NeedExBearingRequest(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount)
        {
            PhAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? PhAvCount : basicProperties.Global.NumberAveragingPhase;
            PlAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? PlAvCount : basicProperties.Global.NumberAveragingBearing;

            var answer = await dsp.ExecutiveDF(MinBandX, MaxBandX, PhAvCount, PlAvCount);
            pLibrary.IQExBearingPaited(answer.Direction, answer.CorrelationHistogram, answer.Frequency, answer.StandardDeviation, answer.DiscardedDirectionPercent);
        }

        private async void PLibrary_NeedQBearingRequest(object sender, double MinBandX, double MaxBandX, int PhAvCount, int PlAvCount)
        {
            PhAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? PhAvCount : basicProperties.Global.NumberAveragingPhase;
            PlAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? PlAvCount : basicProperties.Global.NumberAveragingBearing;

            pLibrary.qBearingButtonEnabled(false);

            var answer = await dsp.QuasiSimultaneouslyDFX10((int)(MinBandX * 10000), (int)(MaxBandX * 10000), (byte)PhAvCount, (byte)PlAvCount);
            if (answer?.Header.ErrorCode == 0)
            {
                if (answer.Source.Direction != -1)
                {
                    short Direction2 = -1;
                    if (answer.LinkedStationResults.Count() > 0)
                    {
                        Direction2 = (short)(answer.LinkedStationResults[0].Direction * 10);
                    }
                    pLibrary.QBearingPaited(answer.Source.Frequency, answer.Source.Direction, Direction2, answer.Source.StandardDeviation, 0);
                }
            }

            pLibrary.qBearingButtonEnabled(true);
        }

        private void PLibrary_FreqOnTypeDivece(object sender, PanoramaLibrary.PLibrary.RDiviceType rDiviceType, double FreqMHz, short threshold)
        {
            switch (rDiviceType)
            {
                case PanoramaLibrary.PLibrary.RDiviceType.Targetting:
                    {
                        TableReconFWS tableReconFWS = new TableReconFWS
                        {
                            Id = 0,
                            FreqKHz = FreqMHz * 1000,
                            Time = DateTime.Now.ToLocalTime(),
                            Deviation = -1,
                            Coordinates = new Coord
                            {
                                Latitude = -1,
                                Longitude = -1,
                                Altitude = -1
                            },
                            ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                            {
                                new TableJamDirect
                                {
                                    JamDirect = new JamDirect
                                    {
                                        NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                        Bearing = -1,
                                        Level = threshold,
                                        Std = -1,
                                        DistanceKM = -1,
                                        IsOwn = true
                                    }
                                }
                            }
                        };

                        UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                    }
                    break;
                case PanoramaLibrary.PLibrary.RDiviceType.RadioSuppression:
                    {
                        TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                        {
                            Id = 0,
                            Sender = SignSender.Panorama,
                            NumberASP = 0,
                            FreqKHz = FreqMHz * 1000,
                            Bearing = -1,
                            Letter = TableOperations.DefinitionParams.DefineLetter(FreqMHz * 1000),
                            Threshold = (short)(threshold - 10),
                            Priority = 2,
                            Coordinates = new Coord
                            {
                                Latitude = -1,
                                Longitude = -1,
                                Altitude = -1
                            },
                            InterferenceParam = new InterferenceParam
                            {
                                Manipulation = 0,
                                Modulation = 1,
                                Deviation = 8,
                                Duration = 4
                            },
                        };

                        UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    }
                    break;
                case PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver:
                    {
                        //CRRX1
                        var type = basicProperties.Local.ARONE1.Type.ToString();
                        
                        switch (type)
                        {
                            case "AR_6000":
                                ControlAR6000First.FrequencyFromPanorama(FreqMHz);
                                break;
                            case "AR_ONE":
                                AroneConnetion.FrequencyFromPanorama(FreqMHz);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case PanoramaLibrary.PLibrary.RDiviceType.ControlRadioReceiver2:
                    {
                        //CRRX2
                        var type = basicProperties.Local.ARONE2.Type.ToString();

                        switch (type)
                        {
                            case "AR_6000":
                                ControlAR6000Sec.FrequencyFromPanorama(FreqMHz);
                                break;
                            case "AR_ONE":
                                ControlArOneSec.FrequencyFromPanorama(FreqMHz);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
            }
        }

        private void PLibrary_BandOnRS(object sender, double freqLeftMHz, double freqRightMHz)
        {
            TableSuppressFHSS tableSuppressFHSS = new TableSuppressFHSS
            {
                Id = 0,
                NumberASP = TableEvents.PropNumberASP.SelectedNumASP,

                FreqMinKHz = freqLeftMHz * 1000,
                FreqMaxKHz = freqRightMHz * 1000,
                StepKHz = 50,
                Letters = DefinitionParams.DefineLetters(freqLeftMHz * 1000, freqRightMHz * 1000),

                Threshold = ConstValues.constThreshold,

                EPO = DefinitionParams.DefineEPO(freqLeftMHz * 1000, freqRightMHz * 1000),
                InterferenceParam = new InterferenceParam
                {
                    Manipulation = 0,
                    Modulation = 1,
                    Deviation = 8,
                    Duration = 1
                },
            };
            UcReconFHSS_OnAddFHSS_RS_Recon(this, tableSuppressFHSS);
        }

        private void PLibrary_OnFreqArea(object sender, PanoramaLibrary.PLibrary.FrequencyType frequencyType, double startFreq, double endFreq)
        {
            TableFreqSpec tableFreqSpec = new TableFreqSpec();
            tableFreqSpec.NumberASP = PropNumberASP.SelectedNumASP;
            tableFreqSpec.FreqMinKHz = startFreq * 1000;
            tableFreqSpec.FreqMaxKHz = endFreq * 1000;
            tableFreqSpec.Note = "Панорама";
            switch (frequencyType)
            {
                case PanoramaLibrary.PLibrary.FrequencyType.Forbidden:
                    OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqForbidden()));
                    break;
                case PanoramaLibrary.PLibrary.FrequencyType.Important:
                    OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqImportant()));
                    break;
                case PanoramaLibrary.PLibrary.FrequencyType.Known:
                    OnAddRecord(this, new TableEvent(tableFreqSpec.ToFreqKnown()));
                    break;
            }
        }





        public void PanoramaPreImportRIFreqs(TableEventArs<TableSectorsRangesRecon> e)
        {
            if (lASP.Count > 0 && lASP.Any(x=> x.ISOwn == true))
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSRangeRecon4Panorama = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == currentNumberASP && x.IsCheck == true).ToList();
                PanoramaImportRIFreqs(lSRangeRecon4Panorama);
            }
        }

        public void PanoramaPreImportRIFreqs(List<TableSectorsRanges> e)
        {
            if (lASP.Count > 0 && lASP.Any(x => x.ISOwn == true))
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSRangeRecon4Panorama = (from t in e let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == currentNumberASP && x.IsCheck == true).ToList();
                PanoramaImportRIFreqs(lSRangeRecon4Panorama);
            }
        }

        public void PanoramaPreImportRSFreqs(TableEventArs<TableSectorsRangesSuppr> e)
        {
            if (lASP.Count > 0 && lASP.Any(x => x.ISOwn == true))
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSRangeSuppr4Panorama = (from t in e.Table let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == currentNumberASP && x.IsCheck == true).ToList();
                PanoramaImportRSFreqs(lSRangeSuppr4Panorama);
            }
        }

        public void PanoramaPreImportRSFreqs(List<TableSectorsRanges> e)
        {
            if (lASP.Count > 0 && lASP.Any(x => x.ISOwn == true))
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSRangeRecon4Panorama = (from t in e let a = t as TableSectorsRanges select a).ToList().Where(x => x.NumberASP == currentNumberASP && x.IsCheck == true).ToList();
                PanoramaImportRSFreqs(lSRangeRecon4Panorama);
            }
        }

        public void PanoramaImportRIFreqs(List<TableSectorsRanges> listSRanges)
        {
            List<double> lMinFreqs = new List<double>();
            List<double> lMaxFreqs = new List<double>();

            for (int i = 0; i < listSRanges.Count; i++)
            {
                if (listSRanges[i].IsCheck)
                {
                    lMinFreqs.Add(listSRanges[i].FreqMinKHz / 1000d);
                    lMaxFreqs.Add(listSRanges[i].FreqMaxKHz / 1000d);
                }
            }

            double[] MinFreqs = lMinFreqs.ToArray();
            double[] MaxFreqs = lMaxFreqs.ToArray();

            DispatchIfNecessary(() =>
            {
                pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRI, MinFreqs, MaxFreqs);
            });
        }

        public void PanoramaImportRSFreqs(List<TableSectorsRanges> listSRanges)
        {
            List<double> lMinFreqs = new List<double>();
            List<double> lMaxFreqs = new List<double>();

            for (int i = 0; i < listSRanges.Count; i++)
            {
                if (listSRanges[i].IsCheck)
                {
                    lMinFreqs.Add(listSRanges[i].FreqMinKHz / 1000d);
                    lMaxFreqs.Add(listSRanges[i].FreqMaxKHz / 1000d);
                }
            }

            double[] MinFreqs = lMinFreqs.ToArray();
            double[] MaxFreqs = lMaxFreqs.ToArray();

            DispatchIfNecessary(() =>
            {
                pLibrary.ImportFreqs(PanoramaLibrary.PLibrary.FreqsType.FreqsRS, MinFreqs, MaxFreqs);
            });
        }

        public void PanoramaUpdateTempSuppressFWS(List<TempSuppressFWS> listTempSuppressFWS)
        {
            if (pLibrary.Mode == 3 || pLibrary.Mode == 4 || pLibrary.Mode == 5)
            {
                if (listTempSuppressFWS.Count() > 0)
                {
                    int count = 0;
                    for (int w = 0; w < listTempSuppressFWS.Count(); w++)
                    {
                        if (listTempSuppressFWS[w].Control == Led.Empty &&
                            listTempSuppressFWS[w].Radiation == Led.Empty &&
                           listTempSuppressFWS[w].Suppress == Led.Empty)
                        {
                            count++;
                        }
                    }

                    if (count == listTempSuppressFWS.Count())
                    {
                        pLibrary.ClearArrowsAndLines();
                    }
                    else
                    {
                        List<int> Frequencies = new List<int>();
                        List<int> Amplitudes = new List<int>();
                        List<Color> Color = new List<Color>();

                        for (int i = 0; i < listTempSuppressFWS.Count(); i++)
                        {
                            if (listTempSuppressFWS[i].Suppress == Led.Green || listTempSuppressFWS[i].Suppress == Led.Red)
                            {
                                Frequencies.Add((int)listTempSuppressFWS[i].FreqKHz * 10);
                                Amplitudes.Add((listTempSuppressFWS[i].Threshold <= 0) ? listTempSuppressFWS[i].Threshold : (-1) * listTempSuppressFWS[i].Threshold);

                                switch (listTempSuppressFWS[i].Radiation)
                                {
                                    case Led.Red:
                                        Color.Add(Colors.Red);
                                        break;
                                    case Led.Blue:
                                        Color.Add(Colors.Blue);
                                        break;
                                    case Led.White:
                                        Color.Add(Colors.White);
                                        break;
                                    default:
                                        Color.Add(Colors.White);
                                        break;
                                }
                            }
                        }

                        if (Frequencies.Count > 0 && Amplitudes.Count > 0)
                        {
                            DispatchIfNecessary(() =>
                            {
                                pLibrary.IQDrawRSArrows(false, Color.ToArray(), Frequencies.ToArray(), Amplitudes.ToArray());
                            });
                        }
                    }
                }
                else
                {
                    pLibrary.ClearArrowsAndLines();
                }
            }
        }

        public void ExternalExBearing(double freqMHz, double freqWidthMHz)
        {
            DispatchIfNecessary(() =>
            {
                pLibrary.ExternalExBearing(freqMHz, freqWidthMHz);
            });
        }

        public async Task<float> ExternalExBearing2(double freqMHz, double freqWidthMHz)
        {
            if (freqWidthMHz <= 0) freqWidthMHz = 0.0036;

            double MinBandXMHz = freqMHz - freqWidthMHz;
            double MaxBandXMHz = freqMHz + freqWidthMHz;

            var PhAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? 3 : basicProperties.Global.NumberAveragingPhase;
            var PlAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? 3 : basicProperties.Global.NumberAveragingBearing;

            var answer = await dsp.ExecutiveDF(MinBandXMHz, MaxBandXMHz, PhAvCount, PlAvCount);

            float bearing = -1;
            if (answer?.Header.ErrorCode == 0)
            {
                PTabControl.SelectedIndex = 2;
                pLibrary.IQExBearingPaited(answer.Direction, answer.CorrelationHistogram, answer.Frequency, answer.StandardDeviation, answer.DiscardedDirectionPercent);
                bearing = answer.Direction / 10f;
            }
            return bearing;
        }


        public void PanoramaPreImportSpecFreqs(TableEventArs<TableFreqForbidden> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqForbidden4Panorama = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(0, lSpecFreqForbidden4Panorama);
            }
        }

        public void PanoramaPreImportForbiddenFreqs(List<TableFreqSpec> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqForbidden4Panorama = (from t in e let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(0, lSpecFreqForbidden4Panorama);
            }
        }

        public void PanoramaPreImportSpecFreqs(TableEventArs<TableFreqImportant> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqImportant4Panorama = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(2, lSpecFreqImportant4Panorama);
            }
        }

        public void PanoramaPreImportImportantFreqs(List<TableFreqSpec> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqImportant4Panorama = (from t in e let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(2, lSpecFreqImportant4Panorama);
            }
        }

        public void PanoramaPreImportSpecFreqs(TableEventArs<TableFreqKnown> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqKnown4Panorama = (from t in e.Table let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(1, lSpecFreqKnown4Panorama);
            }
        }

        public void PanoramaPreImportKnownFreqs(List<TableFreqSpec> e)
        {
            if (lASP.Count > 0)
            {
                var currentNumberASP = lASP.Where(x => x.ISOwn == true).Select(x => x.Id).ToList()[0];
                var lSpecFreqKnown4Panorama = (from t in e let a = t as TableFreqSpec select a).ToList().Where(x => x.NumberASP == currentNumberASP).ToList();
                PanoramaImportSpecFreqs(1, lSpecFreqKnown4Panorama);
            }
        }

        public void PanoramaImportSpecFreqs(byte typeSpecFreq, List<TableFreqSpec> SpecFreqs)
        {
            double[] FreqsStartMHz = new double[SpecFreqs.Count()];
            double[] FreqsEndMHz = new double[SpecFreqs.Count()];
            for (int i = 0; i < SpecFreqs.Count(); i++)
            {
                FreqsStartMHz[i] = SpecFreqs[i].FreqMinKHz / 1000d;
                FreqsEndMHz[i] = SpecFreqs[i].FreqMaxKHz / 1000d;
            }

            switch (typeSpecFreq)
            {
                case 0:
                    pLibrary.PaintSpecBands(PanoramaLibrary.PLibrary.FrequencyType.Forbidden, FreqsStartMHz, FreqsEndMHz);
                    break;
                case 1:
                    pLibrary.PaintSpecBands(PanoramaLibrary.PLibrary.FrequencyType.Known, FreqsStartMHz, FreqsEndMHz);
                    break;
                case 2:
                    pLibrary.PaintSpecBands(PanoramaLibrary.PLibrary.FrequencyType.Important, FreqsStartMHz, FreqsEndMHz);
                    break;
                default: return;
            }
        }


        public class FHSSandExcluded
        {
            public int Id { get; set; }
            public double FreqMinKHz { get; set; }
            public double FreqMaxKHz { get; set; }
            public double Threshold { get; set; }

            public List<Excluded> Excludeds { get; set; }
        }

        public class Excluded
        {
            public double FreqKHz { get; set; }
            public float Deviation { get; set; }
        }

        List<FHSSandExcluded> lFHSSandExcluded = new List<FHSSandExcluded>();
        public void PanoramaUpdateSuppressFHSS()
        {
            lFHSSandExcluded.Clear();
            for (int i = 0; i < lSuppressFHSS.Count(); i++)
            {
                var temp = new FHSSandExcluded();
                temp.Id = lSuppressFHSS[i].Id;
                temp.FreqMinKHz = lSuppressFHSS[i].FreqMinKHz;
                temp.FreqMaxKHz = lSuppressFHSS[i].FreqMaxKHz;
                temp.Threshold = lSuppressFHSS[i].Threshold;
                temp.Excludeds = new List<Excluded>();

                for (int j = 0; j < lFHSSExcludedFreq.Count(); j++)
                {
                    if (lFHSSExcludedFreq[j].IdFHSS == temp.Id)
                    {
                        var temp2 = new Excluded();
                        temp2.FreqKHz = lFHSSExcludedFreq[j].FreqKHz;
                        temp2.Deviation = lFHSSExcludedFreq[j].Deviation;
                        temp.Excludeds.Add(temp2);
                    }
                }
                lFHSSandExcluded.Add(temp);
            }

            PanoramaPaintUpdateSuppressFHSS();
        }

        private void PanoramaPaintUpdateSuppressFHSS()
        {
            List<double> lFreqStartMHz = new List<double>();
            List<double> lFreqEndMHz = new List<double>();
            List<double[]> lCutOffFreqMHz = new List<double[]>();
            List<double[]> lCutOffWidthMHz = new List<double[]>();
            List<double> lNegativeThreshold = new List<double>();

            for (int i = 0; i < lFHSSandExcluded.Count(); i++)
            {
                lFreqStartMHz.Add(lFHSSandExcluded[i].FreqMinKHz / 1000);
                lFreqEndMHz.Add(lFHSSandExcluded[i].FreqMaxKHz / 1000);

                double[] templCutOffFreq = new double[lFHSSandExcluded[i].Excludeds.Count()];
                double[] templCutOffWidth = new double[lFHSSandExcluded[i].Excludeds.Count()];

                for (int j = 0; j < lFHSSandExcluded[i].Excludeds.Count(); j++)
                {
                    templCutOffFreq[j] = lFHSSandExcluded[i].Excludeds[j].FreqKHz / 1000;
                    templCutOffWidth[j] = lFHSSandExcluded[i].Excludeds[j].Deviation / 1000;
                }

                lCutOffFreqMHz.Add(templCutOffFreq);
                lCutOffWidthMHz.Add(templCutOffWidth);

                var negativeThreshold = (lFHSSandExcluded[i].Threshold < 0) ? lFHSSandExcluded[i].Threshold : (-1) * lFHSSandExcluded[i].Threshold;
                lNegativeThreshold.Add(negativeThreshold);
            }

            pLibrary.FHSSonRS(lFreqStartMHz, lFreqEndMHz, lCutOffFreqMHz, lCutOffWidthMHz, lNegativeThreshold);
        }


        private PanoramaSettings LoadPanoramaSettings()
        {
            var panoramaSettings = YamlLoad<PanoramaSettings>("PanoramaSettings.yaml");
            if (panoramaSettings == null)
            {
                panoramaSettings = new PanoramaSettings();
                YamlSave<PanoramaSettings>(panoramaSettings, "PanoramaSettings.yaml");
            }
            return panoramaSettings;
        }

        public class PanoramaSettings
        {
            public PanoramaSettings()
            {
                spectrumSettings = new SpectrumSettings();
            }

            public SpectrumSettings spectrumSettings { get; set; }
        }

        public class SpectrumSettings
        {
            public SpectrumSettings()
            {
                SpectrumPointCount = 3000;
                RequestSpectrumTimer = 100;
                Decay = 0.85;
                FlagScanSpeed = true;
                FHSSTest = true;
                AwaiterTime = 400;
                CancellationTokenDelay = 1000;
                GetSpectrumMode = 0;
                ApdComNumber = 1;
                SavePlotStorageTime = 60;
                Version = 0;
            }

            public int SpectrumPointCount { get; set; }
            public int RequestSpectrumTimer { get; set; }
            public double Decay { get; set; }
            public bool FlagScanSpeed { get; set; }
            public bool FHSSTest { get; set; }
            public int AwaiterTime { get; set; }
            public int CancellationTokenDelay { get; set; }
            public int GetSpectrumMode { get; set; }
            public int ApdComNumber { get; set; }
            public uint SavePlotStorageTime { get; set; }
            public int Version { get; set; }
        }

    }
}
