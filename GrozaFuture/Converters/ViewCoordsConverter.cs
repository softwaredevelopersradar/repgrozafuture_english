﻿using ModelsTablesDBLib;
using System;
using System.Globalization;
using System.Windows.Data;

namespace GrozaFuture
{
    public class ViewCoordsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if (value == null || !(value is View))
            //    return Binding.DoNothing;

            switch (value)
            {
                case "DD.dddddd":
                    return 1;

                case "DD MM.mmmm":
                    return 2;

                case "DD MM SS.ss":
                    return 3;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
