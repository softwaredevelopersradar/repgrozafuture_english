﻿using AwsToConsoleTransferLib;
using BPSS_DLL;
using ModelsTablesDBLib;
using SHS_DLL;
using SpoofingControl;
using SpoofingControl.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;


namespace GrozaFuture
{
    public partial class MainWindow
    {
        private readonly Spoof spoof = new Spoof();
        private double TempLatitude { get; set; }
        private double TempLongitude { get; set; }

        private void InitSpoof()
        {
            spoof.NeedHide += Spoof_NeedHide;

            spoof.OnStateAnten += Spoof_OnStateAnten;
            spoof.OnStateNavigation += Spoof_OnStateNavigation;
            spoof.OnStateSpoof += Spoof_OnStateSpoof;
            spoof.OnSelectionWorkingMode += Spoof_OnSelectionWorkingMode;
            spoof.OnGetStatus += Spoof_OnGetStatus;
            spoof.OnGetCoordinates += Spoof_OnGetCoordinates;
        }

        private void Spoof_OnGetStatus(object sender, EventArgs e)
        {
            SendGetStatusAmp();
        }

        private void Spoof_OnSelectionWorkingMode(object sender, WorkingModeModel e)
        {
            switch (e.Mode)
            {
                case EMode.Spoofing:
                    isSpoofButton = e.IsChecked;
                    if (e.IsChecked)
                    {
                        var getParams = spoof.GetSpoofingParameters();
                        var getCoordinates = spoof.GetStateCoordinates();

                        var spoofParams = new ControlParameters((short)getParams.Altitude,
                                                    (int)getParams.Speed,
                                                    (short)getParams.Direction,
                                                    CodeRange(getParams.Range));

                        var spoofCoord = new CoordinatesParameters(getCoordinates.Latitude, getCoordinates.Longitude);

                        SetCoordinates(spoofCoord);
                        SetControlParams(spoofParams);

                        SpoofingOn();

                        //Запустить Task опроса состояния спуфинга
                        Task.Run(() => SpoofPoolingAsync());

                        //Включить спуфинг у Шаброва
                        //bpss.SendSetSPOOFB(e.PowerLevel);
                        _waitAnswerAmp = comAmp.SendSetSPOOF();

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });
                    }
                    else
                    {
                        //Выключить спуфинг у Жибуля
                        SpoofingOff();

                        //Выключить спуфинг у Шаброва
                        //bpss.SendRadiatOff(5);
                        _waitAnswerAmp = comAmp.SendRadiatOff(0);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });

                        //Отмена опроса по спуфингу
                        isBPSSSpoofOn = false;

                        //Разовый запрос состояния по "0" - всем литерам
                        //bpss.SendStatus(0);

                        //Запрос состояния через 5секунд
                        Task.Run(() => BppsPoolingAsync5s());
                    }
                    break;
                case EMode.NavigationJam:
                    isNaviJamButton = e.IsChecked;
                    //Включить подавление навигации
                    if (e.IsChecked)
                    {
                        //Запрос на включение подавления навигации
                        //bpss.SendSetGNSSB(
                        //    new TGnss()
                        //    {
                        //        gpsL1 = NaviBooles[0],
                        //        gpsL2 = NaviBooles[1],
                        //        glnssL1 = NaviBooles[2],
                        //        glnssL2 = NaviBooles[3]
                        //    },
                        //    e.PowerLevel);

                        TBeidouGalileo beidouGalileo = new TBeidouGalileo();
                        beidouGalileo.beidouL1 = NaviBooles[4];
                        beidouGalileo.beidouL2 = NaviBooles[5];
                        beidouGalileo.galileoL1 = NaviBooles[4];
                        beidouGalileo.galileoL2 = NaviBooles[5];


                        TGpsGlonass gnss = new TGpsGlonass();
                        gnss.gpsL1 = NaviBooles[0];
                        gnss.gpsL2 = NaviBooles[1];

                        gnss.glnssL1 = NaviBooles[2];
                        gnss.glnssL2 = NaviBooles[3];

                        if(NaviBooles[0] || NaviBooles[1] || NaviBooles[2] || NaviBooles[3] || NaviBooles[4] || NaviBooles[5])
                        {
                            _waitAnswerAmp = comAmp.SendSetGNSS(gnss, beidouGalileo, e.PowerLevel);
                        }
                        else
                        {
                            SendRadiatOffAmp(0);
                        }

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });
                    }
                    //Выключить подавление навигации
                    else
                    {
                        //Выключить подавление навигации, 0 - все литеры
                        //bpss.SendRadiatOff(4);
                        SendRadiatOffAmp(0);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });

                        //Отмена опроса по навигации
                        isBPSSNaviOn = false;

                        //Разовый запрос состояния по "0" - всем литерам
                        //bpss.SendStatus(0);

                        //Запрос состояния через 5секунд
                        Task.Run(() => BppsPoolingAsync5s());
                    }
                    break;
                default:
                    break;
            }
        }

        private async void Spoof_OnGetCoordinates(object sender, SpoofingControl.Models.StateCoordinates e)
        {
            switch (e.Coordinate)
            {
                case ECoordinates.LATITUDE:
                    TempLatitude = e.Latitude;
                    break;
                case ECoordinates.LONGITUDE:
                    TempLongitude = e.Longitude;
                    break;
                default:
                    break;
            }

            var spoofCoord = new CoordinatesParameters(TempLatitude, TempLongitude);

            if (clientDB != null)
            {
                var arg = (await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>()).FirstOrDefault();
                if (TempLatitude > 0)
                    arg.Latitude = TempLatitude;
                if (TempLongitude > 0)
                    arg.Longitude = TempLongitude;
                clientDB.Tables[NameTable.GlobalProperties].Add(arg);
            }

            if (IsSpoofConnected && isSpoofOn)
            {
                SetCoordinates(spoofCoord);
            }                
        }

        private async void Spoof_OnStateSpoof(object sender, StateSpoof e)
        {
            var spoofParams = new ControlParameters((short)e.Altitude,
                                                    (int)e.Speed,
                                                    (short)e.Direction,
                                                    CodeRange(e.Range));

            //Если спуфинг включен
            if (IsSpoofConnected && isSpoofOn)
            {
                //То установить параметры спуфинга у Жибуля                
                SetControlParams(spoofParams);
            }
        }

        private byte CodeRange(string valueComboBox)
        {
            byte code = 0;

            switch (valueComboBox)
            {
                case " < 1 km":
                    code = 1;
                    break;
                case " 1..2 km":
                    code = 2;
                    break;
                case " 2..5 km":
                    code = 3;
                    break;
                case " 5..10 km":
                    code = 4;
                    break;
                case " 10..20 km":
                    code = 5;
                    break;
                case " > 20 km":
                    code = 6;
                    break;
                    default:
                    code = 1;
                        break;
            }

            return code;
        }

        private bool[] NaviBooles = new bool[6];
        private bool[] checkedLiters = new bool[2];

        private void Spoof_OnStateNavigation(object sender, StateNavi e)
        {
            NaviBooles[0] = e.IsCheckedGPS_L1;
            NaviBooles[1] = e.IsCheckedGPS_L2;
            NaviBooles[2] = e.IsCheckedGLONASS_L1;
            NaviBooles[3] = e.IsCheckedGLONASS_L2;
            NaviBooles[4] = e.IsCheckedGPS_BeidouL1;
            NaviBooles[5] = e.IsCheckedGPS_BeidouL2;

            if(NaviBooles[0] || NaviBooles[2] || NaviBooles[4])
            {
                checkedLiters[0] = true;
            }
            else { checkedLiters[0] = false; }
            if(NaviBooles[1] || NaviBooles[3] || NaviBooles[5])
            {
                checkedLiters[1] = true;
            }
            else { checkedLiters[1] = false; }
            //checkedLiters[0] = e.IsCheckedGPS_L1;
            //checkedLiters[1] = e.IsCheckedGPS_L2;
        }

        private void Spoof_OnStateAnten(object sender, StateAnten e)
        {
            if (e.IsCheckedLog) 
            {
                Console.WriteLine("LOG" + e.IsCheckedLog);
                byte antLog= 0;
                _waitAnswerAmp = comAmp.SendRelaySwitching(antLog); 
            }
            if (e.IsCheckedOmni) 
            {
                Console.WriteLine("OMNI" + e.IsCheckedOmni);
                byte antOmni = 1;
                _waitAnswerAmp = comAmp.SendRelaySwitching(antOmni);
            }
        }

        private void UpdateSpoofCoord(GlobalProperties globalProperties)
        {
            if (globalProperties.Latitude > 0 && globalProperties.Longitude > 0)
            {
                if(TempLatitude != globalProperties.Latitude || TempLongitude != globalProperties.Longitude)
                {
                    spoof?.UpdateCoodrdinates(globalProperties.Latitude, globalProperties.Longitude);
                }                
            }
        }

        private void Spoof_NeedHide(object sender)
        {
            SpoofToggleButton.IsChecked = false;
        }

        private void SpoofControl_ChangedAntennaDirect(object sender, int e)
        {
            Console.WriteLine(e);
        }

        /*
        private void SpoofControl_ChangedAntennaType(object sender, SpoofingControl.AntennaMode e)
        {
            Console.WriteLine(e);
            if (e == AntennaMode.Log)
                bpss.SendSetAntenna(AntennaCondition.LOG);
            if (e == AntennaMode.OMNI)
                bpss.SendSetAntenna(AntennaCondition.OMNI);
        }

        
        private async void SpoofControl_ChangedCoordinates(object sender, SpoofingControl.CoordinatesModel e)
        {
            var spoofParams = new CoordinatesParameters(e.Latitude, e.Longitude);

            var parameters = new ControlParameters(spoof.SpoofControl.SpoofParameters.Altitude,
                            (int)spoof.SpoofControl.SpoofParameters.Speed,
                            spoof.SpoofControl.SpoofParameters.Heading,
                            CodeRange(spoof.SpoofControl.SpoofParameters.RangeGate));

            if (clientDB != null)
            {
                var arg = (await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>()).FirstOrDefault();
                if (e.Latitude > 0)
                    arg.Latitude = e.Latitude;
                if (e.Longitude > 0)
                    arg.Longitude = e.Longitude;
                clientDB.Tables[NameTable.GlobalProperties].Add(arg);
            }
            //Если спуфинг включен
            if (IsSpoofConnected && isSpoofOn)
            {
                //То установить параметры спуфинга у Жибуля
                SetCoordinates(spoofParams);
                SetControlParams(parameters);
            }

        }
        */

        //Параметры спуфинга

        //private CoordinatesReal SpoofParams = new CoordinatesReal();

        /*
        //Параметры подавления навигации
        private bool[] NaviBooles = new bool[4];

        private void SpoofControl_ChangedNavigation(object sender, NavigationModel e)
        {
            //Установить параметры подавления навигации
            switch (e.Navi)
            {
                case Navi.GPS:

                    switch (e.L)
                    {
                        case L.L1:
                            NaviBooles[0] = e.IsCheked;
                            break;
                        case L.L2:
                            NaviBooles[1] = e.IsCheked;
                            break;
                    }

                    break;
                case Navi.GLONASS:

                    switch (e.L)
                    {
                        case L.L1:
                            NaviBooles[2] = e.IsCheked;
                            break;
                        case L.L2:
                            NaviBooles[3] = e.IsCheked;
                            break;
                    }

                    break;
            }
        }
        */

        private bool isNaviJamButton = false;
        private bool isSpoofButton = false;
       
        /*
        private void SpoofControl_ChangedMainToggleButtons(object sender, MainToggleButtonsModel e)
        {
            switch (e.SpoofAndNavi)
            {
                case SpoofOrNavi.Spoofing:
                    isSpoofButton = e.IsCheked;
                    if (e.IsCheked)
                    {
                        //Включить спуфинг у Жибуля

                        var parameters = new ControlParameters(spoof.SpoofControl.SpoofParameters.Altitude,
                            (int)spoof.SpoofControl.SpoofParameters.Speed,
                            spoof.SpoofControl.SpoofParameters.Heading,
                            CodeRange(spoof.SpoofControl.SpoofParameters.RangeGate));

                        var coordinates = new CoordinatesParameters(spoof.SpoofControl.SpoofParameters.Latitude,
                                                                    spoof.SpoofControl.SpoofParameters.Longitude);

                        SetCoordinates(coordinates);
                        SetControlParams(parameters);

                        SpoofingOn();

                        //Запустить Task опроса состояния спуфинга
                        Task.Run(() => SpoofPoolingAsync());

                        //Включить спуфинг у Шаброва
                        bpss.SendSetSPOOFB(e.PowerLevel);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });
                    }
                    else
                    {
                        //Выключить спуфинг у Жибуля
                        SpoofingOff();

                        //Выключить спуфинг у Шаброва
                        bpss.SendRadiatOff(5);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });

                        //Отмена опроса по спуфингу
                        isBPSSSpoofOn = false;

                        //Разовый запрос состояния по "0" - всем литерам
                        bpss.SendStatus(0);

                        //Запрос состояния через 5секунд
                        Task.Run(() => BppsPoolingAsync5s());
                    }

                    break;
                case SpoofOrNavi.NaviJamming:
                    isNaviJamButton = e.IsCheked;

                    //Включить подавление навигации
                    if (e.IsCheked)
                    {
                        //Запрос на включение подавления навигации
                        bpss.SendSetGNSSB(
                            new TGnss()
                            {
                                gpsL1 = NaviBooles[0],
                                gpsL2 = NaviBooles[1],
                                glnssL1 = NaviBooles[2],
                                glnssL2 = NaviBooles[3]
                            },
                            e.PowerLevel);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });
                    }
                    //Выключить подавление навигации
                    else
                    {
                        //Выключить подавление навигации, 0 - все литеры
                        bpss.SendRadiatOff(4);

                        DispatchIfNecessary(() =>
                        {
                            TumanConnection.ShowWrite();
                        });

                        //Отмена опроса по навигации
                        isBPSSNaviOn = false;

                        //Разовый запрос состояния по "0" - всем литерам
                        bpss.SendStatus(0);

                        //Запрос состояния через 5секунд
                        Task.Run(() => BppsPoolingAsync5s());
                    }

                    break;
            }
        }
        */

        private byte CodeRange(byte range)
        {

            byte code = 0;

            if (range <= 1)
                code = 1;

            if (range > 1 && range <= 2)
                code = 2;

            if (range > 2 && range <= 5)
                code = 3;

            if (range > 5 && range <= 10)
                code = 4;

            if (range > 10 && range <= 20)
                code = 5;

            if (range > 20 && range <= 30)
                code = 6;

            if (range > 30)
                code = 7;


            return code;
        }

        private List<StateNAVControl.StateModel> GenerateDefaultBPSSParams()
        {
            return new List<StateNAVControl.StateModel>()
                {
                    new StateNAVControl.StateModel()
                    {
                        Temperature = -102,
                        Amperage = (float)-2f,
                        Snt = (StateNAVControl.Led)0,
                        Rad = (StateNAVControl.Led)0,
                        Pow = (StateNAVControl.Led)0
                    },
                    new StateNAVControl.StateModel()
                    {
                        Temperature = -102,
                        Amperage = (float)-2f,
                        Snt = (StateNAVControl.Led)0,
                        Rad = (StateNAVControl.Led)0,
                        Pow = (StateNAVControl.Led)0
                    }
                };
        }
    }
}
