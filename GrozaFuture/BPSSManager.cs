﻿using BPSS_DLL;
using SpoofingControl;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        BPSS bpss = new BPSS(0x04, 0x05);

        private bool isBPSSConnected = false;
        private bool isBPSSOn = false;

        private bool isBPSSNaviOn = false;
        private bool isBPSSSpoofOn = false;

        private void TumanConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (comAmp != null)
                DisconnectAmp();
            else
                ConnectAmp();
            /*
            if (isBPSSConnected == false)
            {
                //Значит делаем Connect
                bool isOpen = bpss.OpenPort(basicProperties.Local.Tuman.ComPort, basicProperties.Local.Tuman.PortSpeed, Parity.None, 8, StopBits.One);
                if (isOpen == true)
                {
                    bpss.OnReadByte += Bpps_OnReadByte;
                    bpss.OnWriteByte += Bpps_OnWriteByte;
                    bpss.OnConfirmSet += Bpps_OnConfirmSet;
                    bpss.OnConfirmStatus += Bpps_OnConfirmStatus;
                    bpss.OnConfirmAntennaStatus += Bpps_OnConfirmAntennaStatus;

                    isBPSSConnected = true;
                    TumanConnection.ShowConnect();

                    bpss.SendStatusAntenna();
                }
                else
                {
                    TumanConnection.ShowDisconnect();
                }
            }
            else
            {
                //Значит делаем Disconnect
                bool isClose = bpss.ClosePort();
                if (isClose == true)
                {
                    bpss.OnReadByte -= Bpps_OnReadByte;
                    bpss.OnWriteByte -= Bpps_OnWriteByte;
                    bpss.OnConfirmSet -= Bpps_OnConfirmSet;
                    bpss.OnConfirmStatus -= Bpps_OnConfirmStatus;
                    bpss.OnConfirmAntennaStatus -= Bpps_OnConfirmAntennaStatus;

                    isBPSSConnected = false;
                    TumanConnection.ShowDisconnect();
                }

            }
            */

        }

        private void Bpps_OnConfirmAntennaStatus(object sender, ConfirmAntennaStatusEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                TumanConnection.ShowRead();

                //spoof.SpoofControl.SetAntennaMode(AntennaModeConverter(e.Antenna));
            });

            //AntennaMode AntennaModeConverter(AntennaCondition Antenna)
            //{
            //    switch (Antenna)
            //    {
            //        case AntennaCondition.LOG: return AntennaMode.Log;
            //        case AntennaCondition.OMNI: return AntennaMode.OMNI;
            //        default: return 0;
            //    }
            //}
        }

        private void Bpps_OnConfirmStatus(object sender, ConfirmStatusEventArgs e)
        {
            if (e.CodeError[0] == 0 || e.CodeError[0] == 9)
            {
                List<StateNAVControl.StateModel> Params = new List<StateNAVControl.StateModel>()
                {
                    new StateNAVControl.StateModel()
                    {
                        Temperature = e.ParamAmp[0].Temp,
                        Amperage = (float)e.ParamAmp[0].Current / 10f,
                        Snt = (StateNAVControl.Led)e.ParamAmp[0].Snt,
                        Rad = (StateNAVControl.Led)e.ParamAmp[0].Rad,
                        Pow = (StateNAVControl.Led)e.ParamAmp[0].Power
                    },
                    new StateNAVControl.StateModel()
                    {   
                        Temperature = e.ParamAmp[1].Temp,
                        Amperage = (float)e.ParamAmp[1].Current / 10f,
                        Snt = (StateNAVControl.Led)e.ParamAmp[1].Snt,
                        Rad = (StateNAVControl.Led)e.ParamAmp[1].Rad,
                        Pow = (StateNAVControl.Led)e.ParamAmp[1].Power
                    }
                };


                DispatchIfNecessary(() =>
                {
                    TumanConnection.ShowRead();

                    //spoof.SetBPSSParams(Params);
                });

            }

            try { semaphoreSlim.Release(); } catch { }
        }

        private void Bpps_OnConfirmSet(object sender, ConfirmSetEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                TumanConnection.ShowRead();
            });

            switch (e.AmpCode)
            {
                //Если шифр 17 по подавлению навигации
                case AmpCodes.GNSS_APP:
                    {
                        //Если успешно
                        if (e.CodeError == 0)
                        {
                            //Тогда запускаем поток опроса состояния, если он не запущен
                            if ((isBPSSNaviOn && isBPSSSpoofOn) == false)
                            {
                                isBPSSNaviOn = true;

                                //Запустить Task опроса состояния БПСС
                                Task.Run(() => BppsPoolingAsync());
                            }
                            else
                            {
                                isBPSSNaviOn = true;
                            }
                        }
                        else
                        {
                            isNaviJamButton = false;
                            //spoof.SpoofControl.NavigationToggleButtonOff();
                        }
                    }
                    break;
                //Если шифр 19 по Спуфингу
                case AmpCodes.SPOOF_APP:
                    {
                        //Если успешно
                        if (e.CodeError == 0)
                        {
                            //Тогда запускаем поток опроса состояния, если он не запущен
                            if ((isBPSSNaviOn && isBPSSSpoofOn) == false)
                            {
                                isBPSSSpoofOn = true;

                                //Запустить Task опроса состояния БПСС
                                Task.Run(() => BppsPoolingAsync());
                            }
                            else
                            {
                                isBPSSSpoofOn = true;
                            }
                        }
                        else
                        {
                            isSpoofButton = false;
                            //spoof.SpoofControl.SpoofingToggleButtonOff();
                        }
                    }
                    break;

                default:
                    break;

            }

            try
            {
                //if (Display.ShowAllByte == false)
                //{
                //    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                //    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                //}
            }
            catch { }
        }

        private void Bpps_OnWriteByte(object sender, byte[] e)
        {
            try
            {
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                //Display.AddTextToLog(a, Brushes.Red);

                DispatchIfNecessary(() =>
                {
                    TumanConnection.ShowWrite();
                });

            }
            catch { }
        }

        private void Bpps_OnReadByte(object sender, byte[] e)
        {
            try
            {
                string a = "";
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                //Dispatcher.Invoke(() => { Display.AddTextToLog(a, Brushes.Green); });

                DispatchIfNecessary(() =>
                {
                    TumanConnection.ShowRead();
                });

            }
            catch
            {

            }
        }


        public int BppsPoolingTimer { get; set; } = 5000;

        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(0, 1);

        private async Task BppsPoolingAsync()
        {
            while (isBPSSConnected && (isBPSSNaviOn || isBPSSSpoofOn))
            {
                //Запрос статуса по "0" - всем литерам
                bpss.SendStatus(0);

                DispatchIfNecessary(() =>
                {
                    TumanConnection.ShowWrite();
                });

                await semaphoreSlim.WaitAsync(BppsPoolingTimer);
                await Task.Delay(BppsPoolingTimer);
            }
        }

        private async Task BppsPoolingAsync5s()
        {
            await Task.Delay(BppsPoolingTimer);
            {
                //Запрос статуса по "0" - всем литерам
                //bpss.SendStatus(0);

                DispatchIfNecessary(() =>
                {
                    TumanConnection.ShowWrite();
                });
            }
        }

    }
}
