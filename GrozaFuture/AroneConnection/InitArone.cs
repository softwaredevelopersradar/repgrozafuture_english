﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void InitArone()
        {
            ARONEconnection.ShowDisconnect();
            ARONE2connection.ShowDisconnect();

            //CRRX_1


            ReceiverTableFirst.OnUpdateBank += ReceiverTableFirst_OnUpdateBank;
            ReceiverTableFirst.OnUpdateFileType += ReceiverTableFirst_OnUpdateFileType;

            ReceiverTable.OnUpdate += new WpfControlLibraryBut.ReceiverTableControl.MyControlEventHandler(OnUpdateBank);
            ReceiverTable.OnUpdateFileType += new WpfControlLibraryBut.ReceiverTableControl.MyControlEventHandler(OnUpdateFileType);

            //CRRX_2
            ReceiverTableSec.OnUpdateBank += ReceiverTableSec_OnUpdateBank;
            ReceiverTableSec.OnUpdateFileType += ReceiverTableSec_OnUpdateFileType;

            ReceiverTableArOneSec.OnUpdate += ReceiverTableArOneSec_OnUpdate;
            ReceiverTableArOneSec.OnUpdateFileType += ReceiverTableArOneSec_OnUpdateFileType;

            InitReceiversDataExchange();
        }
    }
}
