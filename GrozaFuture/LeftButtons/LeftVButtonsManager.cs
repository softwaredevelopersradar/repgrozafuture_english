﻿using ModelsTablesDBLib;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var CGLS = YamlLoad<CustomGridLengthSettings>("CustomMarkup.yaml");
            if (CGLS == null)
            {
                CGLS = GenerateDefaultMarkup();
                YamlSave<CustomGridLengthSettings>(CGLS, "DefaultMarkup.yaml");
            }
            ApplyMarkup(CGLS);

        }

        private void ApplyMarkup(CustomGridLengthSettings CGLS)
        {
            try
            {
                BaseField.ColumnDefinitions[0].Width = new GridLength(CGLS.n1.Value, CGLS.n1.gridUnitTypeValue);
                BaseField.ColumnDefinitions[1].Width = new GridLength(CGLS.n2.Value, CGLS.n2.gridUnitTypeValue);
                BaseField.ColumnDefinitions[2].Width = new GridLength(CGLS.n3.Value, CGLS.n3.gridUnitTypeValue);
                BaseField.ColumnDefinitions[3].Width = new GridLength(CGLS.n4.Value, CGLS.n4.gridUnitTypeValue);
                //BaseField.ColumnDefinitions[4].Width = new GridLength(CGLS.n5.Value, CGLS.n5.gridUnitTypeValue);
                SavedGridLength = new GridLength(CGLS.n1.Value, CGLS.n1.gridUnitTypeValue);
                SavedGridLengthTwo = new GridLength(CGLS.n3.Value, CGLS.n3.gridUnitTypeValue);

                RDGrid.RowDefinitions[0].Height = new GridLength(CGLS.n6.Value, CGLS.n6.gridUnitTypeValue);
                RDGrid.RowDefinitions[1].Height = new GridLength(CGLS.n7.Value, CGLS.n7.gridUnitTypeValue);
                RDGrid.RowDefinitions[2].Height = new GridLength(CGLS.n8.Value, CGLS.n8.gridUnitTypeValue);
                RDGrid.RowDefinitions[3].Height = new GridLength(CGLS.n9.Value, CGLS.n9.gridUnitTypeValue);
                RDGrid.RowDefinitions[4].Height = new GridLength(CGLS.n10.Value, CGLS.n10.gridUnitTypeValue);

                //RGrid.RowDefinitions[0].Height = new GridLength(CGLS.n11.Value, CGLS.n11.gridUnitTypeValue);
                //RGrid.RowDefinitions[1].Height = new GridLength(CGLS.n12.Value, CGLS.n12.gridUnitTypeValue);

                //RGrid.RowDefinitions[3].Height = new GridLength(CGLS.n13.Value, CGLS.n13.gridUnitTypeValue);
                //RGrid.RowDefinitions[3].MinHeight = CGLS.n14.Value;

                PanoramaToggleButton.IsChecked = CGLS.P;
                TableToggleButton.IsChecked = CGLS.T;
                sTableToggleButton.IsChecked = CGLS.sT;
                RCToggleButton.IsChecked = CGLS.R;
            }
            catch
            {
                var tryCGLS = GenerateDefaultMarkup();
                YamlSave<CustomGridLengthSettings>(tryCGLS, "CustomMarkup.yaml");
                YamlSave<CustomGridLengthSettings>(tryCGLS, "DefaultMarkup.yaml");
                ApplyMarkup(tryCGLS);
            }
        }

        private CustomGridLengthSettings GenerateDefaultMarkup()
        {
            var CGLS = new CustomGridLengthSettings();
            {
                CGLS.n1 = new CustomGridLength(1, GridUnitType.Star);
                CGLS.n2 = new CustomGridLength(1, GridUnitType.Auto);
                CGLS.n3 = new CustomGridLength(1.4, GridUnitType.Star);
                CGLS.n4 = new CustomGridLength(1, GridUnitType.Auto);
                CGLS.n5 = new CustomGridLength(359, GridUnitType.Pixel);

                CGLS.n6 = new CustomGridLength(5, GridUnitType.Star);
                CGLS.n7 = new CustomGridLength(1, GridUnitType.Auto);
                CGLS.n8 = new CustomGridLength(3, GridUnitType.Star);
                CGLS.n9 = new CustomGridLength(1, GridUnitType.Auto);
                CGLS.n10 = new CustomGridLength(2, GridUnitType.Star);

                //CGLS.n11 = new CustomGridLength(1, GridUnitType.Auto);
                //CGLS.n12 = new CustomGridLength(1, GridUnitType.Star);

                //CGLS.n13 = new CustomGridLength(0.8, GridUnitType.Star);
                //CGLS.n14 = new CustomGridLength(50, GridUnitType.Pixel);

                CGLS.P = false;
                CGLS.T = false;
                CGLS.sT = false;
                CGLS.R = false;
            }
            return CGLS;

        }

        private CustomGridLengthSettings ExportCurrentMarkup()
        {
            var CGLS = new CustomGridLengthSettings();
            {
                CGLS.n1 = new CustomGridLength(BaseField.ColumnDefinitions[0].Width);
                CGLS.n2 = new CustomGridLength(BaseField.ColumnDefinitions[1].Width);
                CGLS.n3 = new CustomGridLength(BaseField.ColumnDefinitions[2].Width);
                CGLS.n4 = new CustomGridLength(BaseField.ColumnDefinitions[3].Width);
                CGLS.n5 = new CustomGridLength(BaseField.ColumnDefinitions[4].Width);

                CGLS.n6 = new CustomGridLength(RDGrid.RowDefinitions[0].Height);
                CGLS.n7 = new CustomGridLength(RDGrid.RowDefinitions[1].Height);
                CGLS.n8 = new CustomGridLength(RDGrid.RowDefinitions[2].Height);
                CGLS.n9 = new CustomGridLength(RDGrid.RowDefinitions[3].Height);
                CGLS.n10 = new CustomGridLength(RDGrid.RowDefinitions[4].Height);

                //CGLS.n11 = new CustomGridLength(RGrid.RowDefinitions[0].Height);
                //CGLS.n12 = new CustomGridLength(RGrid.RowDefinitions[1].Height);

                //CGLS.n13 = new CustomGridLength(RGrid.RowDefinitions[3].Height);
                //CGLS.n14 = new CustomGridLength(RGrid.RowDefinitions[3].MinHeight, GridUnitType.Pixel);

                CGLS.P = PanoramaToggleButton.IsChecked.Value;
                CGLS.T = TableToggleButton.IsChecked.Value;
                CGLS.sT = sTableToggleButton.IsChecked.Value;
                CGLS.R = RCToggleButton.IsChecked.Value;
            }
            return CGLS;

        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            var CGLS = ExportCurrentMarkup();
            YamlSave<CustomGridLengthSettings>(CGLS, "CustomMarkup.yaml");
            CloseAPD();

        }

        public class CustomGridLengthSettings
        {
            public CustomGridLength n1 { get; set; }
            public CustomGridLength n2 { get; set; }
            public CustomGridLength n3 { get; set; }
            public CustomGridLength n4 { get; set; }
            public CustomGridLength n5 { get; set; }
            public CustomGridLength n6 { get; set; }
            public CustomGridLength n7 { get; set; }
            public CustomGridLength n8 { get; set; }
            public CustomGridLength n9 { get; set; }
            public CustomGridLength n10 { get; set; }
            //public CustomGridLength n11 { get; set; }
            //public CustomGridLength n12 { get; set; }
            //public CustomGridLength n13 { get; set; }
            //public CustomGridLength n14 { get; set; }
            public bool P { get; set; }
            public bool T { get; set; }
            public bool sT { get; set; }
            public bool R { get; set; }
        }

        public class CustomGridLength
        {
            public CustomGridLength()
            {
                Value = 1;
                gridUnitTypeValue = GridUnitType.Auto;
            }

            public CustomGridLength(double value, GridUnitType gridUnitType)
            {
                Value = value;
                gridUnitTypeValue = gridUnitType;
            }

            public CustomGridLength(GridLength gridLength)
            {
                Value = gridLength.Value;
                gridUnitTypeValue = gridLength.GridUnitType;
            }

            public double Value { get; set; }
            public GridUnitType gridUnitTypeValue { get; set; }
        }

        private void DButton_Click(object sender, RoutedEventArgs e)
        {
            var CGLS = YamlLoad<CustomGridLengthSettings>("DefaultMarkup.yaml");
            if (CGLS == null)
            {
                CGLS = GenerateDefaultMarkup();
                YamlSave<CustomGridLengthSettings>(CGLS, "DefaultMarkup.yaml");
            }
            ApplyMarkup(CGLS);
            VisibleAll();
            UnChekedAll();
            DefaultSplitters();
        }

        private void VisibleAll()
        {
            ucTemsFWS.Visibility = Visibility.Visible;

            ucASP.Visibility = Visibility.Visible;
            SRSF.Visibility = Visibility.Visible;

            sTables.Visibility = Visibility.Visible;

            if(basicProperties.Local.ARONE1.State == true) 
            {
                AroneConnetion.Visibility = Visibility.Visible;
                ReceiverTable.Visibility = Visibility.Visible;
            }

            if(basicProperties.Local.ARONE2.State == true) 
            {
                ControlAR6000Sec.Visibility = Visibility.Visible;
                ReceiverTableSec.Visibility = Visibility.Visible;
            }
        }
        private void UnChekedAll()
        {
            PanoramaToggleButton.IsChecked = false;
            TableToggleButton.IsChecked = false;
            sTableToggleButton.IsChecked = false;
            //RToggleButton.IsChecked = false;
            //CToggleButton.IsChecked = false;
            RCToggleButton.IsChecked = false;
        }
        private void DefaultSplitters()
        {
            PnT_Splitter(true);
            TnsT_Splitter(true);
            //RnC_Splitter(true);
            Rp_Splitter(true);
            Lp_Splitter(true);
        }

        //Изменение видимости панорам
        GridLength SavedGridPanoramaHeight = new GridLength(5, GridUnitType.Star);
        private void PanoramaToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (TableToggleButton.IsChecked.Value == false)
            {
                if (PanoramaToggleButton.IsChecked.Value == true)
                {
                    PanoramaCollapsed(true);
                }
                else
                {
                    PanoramaCollapsed(false);
                }
            }
            else
            {
                TableToggleButton.IsChecked = false;

                TablesCollapsed(false);
                PanoramaCollapsed(true);

                PanoramaToggleButton.IsChecked = true;
            }
        }

        private void PanoramaCollapsed(bool flag)
        {
            if (flag == true)
            {
                SavedGridPanoramaHeight = RDGrid.RowDefinitions[0].Height;
                RDGrid.RowDefinitions[0].Height = new GridLength(0);

                PnT_Splitter(false);
            }
            else
            {
                RDGrid.RowDefinitions[0].Height = SavedGridPanoramaHeight;

                PnT_Splitter(true);
            }
        }

        private void PnT_Splitter(bool value)
        {
            if (value)
            {
                gridSplitterH1.IsEnabled = true;
                //RDGrid.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Auto); // P/T Splitter
            }
            else
            {
                gridSplitterH1.IsEnabled = false;
                //RDGrid.RowDefinitions[1].Height = new GridLength(0); // P/T Splitter
            }
        }

        //Изменение видимости таблиц
        GridLength SavedGridTableHeight = new GridLength(3, GridUnitType.Star);
        private void TableToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (PanoramaToggleButton.IsChecked.Value == false)
            {
                if (TableToggleButton.IsChecked.Value == true)
                {
                    TablesCollapsed(true);
                }
                else
                {
                    TablesCollapsed(false);
                }
            }
            else
            {
                PanoramaToggleButton.IsChecked = false;

                PanoramaCollapsed(false);
                TablesCollapsed(true);

                TableToggleButton.IsChecked = true;
            }
        }

        private void TablesCollapsed(bool flag)
        {
            if (flag == true)
            {
                ucTemsFWS.Visibility = Visibility.Collapsed;

                PnT_Splitter(false);
                TnsT_Splitter(false);

                SavedGridTableHeight = RDGrid.RowDefinitions[2].Height;
                RDGrid.RowDefinitions[2].Height = new GridLength(0);
            }
            else
            {
                ucTemsFWS.Visibility = Visibility.Visible;

                PnT_Splitter(true);
                TnsT_Splitter(true);

                RDGrid.RowDefinitions[2].Height = SavedGridTableHeight;
            }
        }

        private void TnsT_Splitter(bool value)
        {
            if (value)
            {
                gridSplitterH2.IsEnabled = true;
                //RDGrid.RowDefinitions[3].Height = new GridLength(1, GridUnitType.Auto); // sT Splitter
            }
            else
            {
                gridSplitterH2.IsEnabled = false;
                //RDGrid.RowDefinitions[3].Height = new GridLength(0); // sT Splitter
            }
        }

        //Изменение видимости двух подтаблиц
        GridLength SavedGridsTableHeight = new GridLength(2, GridUnitType.Star);
        private void sTableToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (sTableToggleButton.IsChecked.Value == true)
            {
                ucASP.Visibility = Visibility.Collapsed;
                SRSF.Visibility = Visibility.Collapsed;

                sTables.Visibility = Visibility.Collapsed;

                TnsT_Splitter(false);

                SavedGridsTableHeight = RDGrid.RowDefinitions[4].Height;
                RDGrid.RowDefinitions[4].Height = new GridLength(0);
            }
            else
            {
                ucASP.Visibility = Visibility.Visible;
                SRSF.Visibility = Visibility.Visible;

                sTables.Visibility = Visibility.Visible;

                TnsT_Splitter(true);

                RDGrid.RowDefinitions[4].Height = SavedGridsTableHeight;
            }
        }

        //Переменная для сохранения размера правой панели (Rp)
        GridLength SavedGridRpWidth = new GridLength(359, GridUnitType.Pixel);

        //Изменение видимости приёмника + логика правой панели (Rp)
        GridLength SavedGridR1Height = new GridLength(1, GridUnitType.Auto);
        GridLength SavedGridR2Height = new GridLength(1, GridUnitType.Star);
        //private void RToggleButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (RToggleButton.IsChecked.Value == true)
        //    {
        //        AroneConnetion.Visibility = Visibility.Collapsed;
        //        AroneGraph.Visibility = Visibility.Collapsed;

        //        SavedGridR1Height = RGrid.RowDefinitions[0].Height;
        //        RGrid.RowDefinitions[0].Height = new GridLength(0);

        //        SavedGridR2Height = RGrid.RowDefinitions[1].Height;
        //        RGrid.RowDefinitions[1].Height = new GridLength(0);

        //        RnC_Splitter(false);

        //        if (CToggleButton.IsChecked.Value == true)
        //        {
        //            SavedGridRpWidth = BaseField.ColumnDefinitions[4].Width;
        //            BaseField.ColumnDefinitions[4].Width = new GridLength(0);
        //            Rp_Splitter(false);
        //        }
        //    }
        //    else
        //    {
        //        AroneConnetion.Visibility = Visibility.Visible;
        //        AroneGraph.Visibility = Visibility.Visible;

        //        RGrid.RowDefinitions[0].Height = SavedGridR1Height;
        //        RGrid.RowDefinitions[1].Height = SavedGridR2Height;

        //        RnC_Splitter(true);

        //        BaseField.ColumnDefinitions[4].Width = SavedGridRpWidth;
        //        Rp_Splitter(true);
        //    }
        //}

        private void RnC_Splitter(bool value)
        {
            if (value)
            {
                //gridSplitterH3.IsEnabled = true; // R/C Splitter
                //RGridItem1.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Auto); // R/C Splitter
            }
            else
            {
                //gridSplitterH3.IsEnabled = false; // R/C Splitter
                //RGridItem1.RowDefinitions[2].Height = new GridLength(0); // R/C Splitter
            }
        }

        private void Rp_Splitter(bool value)
        {
            if (value)
            {
                gridGeneralSplitterV2.IsEnabled = true; // Rp Splitter
            }
            else
            {
                gridGeneralSplitterV2.IsEnabled = false; // Rp Splitter
            }
        }

        //Изменение видимости чата и прочего + логика правой панели (Rp)
        GridLength SavedGridCHeight = new GridLength(0.8, GridUnitType.Star);
        //private void CToggleButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (CToggleButton.IsChecked.Value == true)
        //    {
        //        ReceiverTable.Visibility = Visibility.Collapsed;

        //        RnC_Splitter(false);

        //        SavedGridCHeight = RGrid.RowDefinitions[3].Height;
        //        RGrid.RowDefinitions[3].Height = new GridLength(0);
        //        //RGrid.RowDefinitions[3].MinHeight = 0;

        //        if (RToggleButton.IsChecked.Value == true)
        //        {
        //            SavedGridRpWidth = BaseField.ColumnDefinitions[4].Width;
        //            BaseField.ColumnDefinitions[4].Width = new GridLength(0);
        //            Rp_Splitter(false);
        //        }
        //    }
        //    else
        //    {
        //        ReceiverTable.Visibility = Visibility.Visible;

        //        RnC_Splitter(true);

        //        RGrid.RowDefinitions[3].Height = SavedGridCHeight;
        //        //RGrid.RowDefinitions[3].MinHeight = 10;

        //        BaseField.ColumnDefinitions[4].Width = SavedGridRpWidth;
        //        Rp_Splitter(true);
        //    }
        //}


        private void RCToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (RCToggleButton.IsChecked.Value == true)
            {
                AroneConnetion.Visibility = Visibility.Collapsed;
                //AroneGraph.Visibility = Visibility.Collapsed;

                SavedGridR1Height = RGrid.RowDefinitions[0].Height;
                RGrid.RowDefinitions[0].Height = new GridLength(0);

                //SavedGridR2Height = RGrid.RowDefinitions[1].Height;
                //RGrid.RowDefinitions[1].Height = new GridLength(0);

                //RnC_Splitter(false);

                SavedGridRpWidth = BaseField.ColumnDefinitions[4].Width;
                BaseField.ColumnDefinitions[4].Width = new GridLength(0);
                Rp_Splitter(false);

                ReceiverTable.Visibility = Visibility.Collapsed;

                //SavedGridCHeight = RGrid.RowDefinitions[3].Height;
                //RGrid.RowDefinitions[3].Height = new GridLength(0);
            }
            else
            {
                AroneConnetion.Visibility = Visibility.Visible;
                //AroneGraph.Visibility = Visibility.Visible;

                RGrid.RowDefinitions[0].Height = SavedGridR1Height;
                //RGrid.RowDefinitions[1].Height = SavedGridR2Height;

                //RnC_Splitter(true);

                BaseField.ColumnDefinitions[4].Width = SavedGridRpWidth;
                Rp_Splitter(true);

                ReceiverTable.Visibility = Visibility.Visible;

                //RGrid.RowDefinitions[3].Height = SavedGridCHeight;
            }
        }
    }
}
