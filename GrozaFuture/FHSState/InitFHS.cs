﻿using System.Windows;
using FPSFullStateControl;
using FpsStateControl;
using System;
using System.Collections.Generic;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private FHSState.FHSState FHSStateWindow;
        private void InitFHS()
        {
            FHSStateWindow = new FHSState.FHSState();
            if (FHSStateWindow.quickFPS.numOfLetters == 9 || FHSStateWindow.fullFPS.numOfLetters == 9)
            {
                FHSStateWindow.Height = 550;
                FHSStateWindow.MaxHeight = 550;
            }
            else
            {
                FHSStateWindow.Height = 590;
                FHSStateWindow.MaxHeight = 590;
            }
            FHSStateWindow.NeedHide += FHS_NeedHide;
            FHSStateWindow.quickFPS.OnSetPower += QuickFPS_OnSetPower; 
            FHSStateWindow.quickFPS.OnSetLoad += QuickFPS_OnSetLoad;
            FHSStateWindow.quickFPS.OnUpdateAll += QuickFPS_OnUpdateAll;
        }

        private async void QuickFPS_OnUpdateAll(object sender, EventArgs e)
        {
            var asnwer = await dsp.FHSGetState();
        }

        private async void QuickFPS_OnSetLoad(object sender, SetLoad e)
        {
            var asnwer = await dsp.SetTypeLoad(e.Letter, e.Load);
        }

        private async void QuickFPS_OnSetPower(object sender, SetHalfPower e)
        {
            var asnwer = await dsp.SetPowerPercentage(e.Letter, e.Power);
        }

        private void FHS_NeedHide(object sender)
        {
            FButton.IsChecked = false;
        }

        private void FButton_Click(object sender, RoutedEventArgs e)
        {
            if (FHSStateWindow.IsVisible)
                FHSStateWindow.Hide();
            else
                FHSStateWindow.Show();
        }

    }
}
