﻿using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private bool _OmniButton = false;
        public bool OmniButton
        {
            get { return _OmniButton; }
            set
            {
                _OmniButton = value;
                OnPropertyChanged(nameof(OmniButton));
            }
        }

        private void OmniButtonInteraction()
        {
            OmniButton = !OmniButton;
        }

        private void OmniToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (OmniToggleButton.IsChecked.Value)
                omni.Show();
            else
                omni.Hide();
        }

        private Omni omni = new Omni();

        private void InitOmni()
        {
            omni.NeedHide += Omni_NeedHide;
            omni.SendHexValues += Omni_SendHexValues;
        }

        private async void Omni_SendHexValues(object sender, int[] hexValues)
        {
            var answer = await dsp.SendPoyntingCommutatorCommand((DspDataModel.Hardware.CommutatorCommands)(hexValues[3]));
        }

        private void Omni_NeedHide(object sender)
        {
            OmniToggleButton.IsChecked = false;
        }

        private void ConnectionToOmni(bool flag)
        {
            if (flag)
            {
                omni.SetDefaut();
            }
        }

    }
}
