﻿using PoytingControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GrozaFuture
{
    /// <summary>
    /// Interaction logic for Omni.xaml
    /// </summary>
    public partial class Omni : Window
    {
        public Omni()
        {
            InitializeComponent();
            PoyControl.Gen4HexValues += PoyControl_Gen4HexValues;

            PoyControl.ChangeView += PoyControl_ChangeView;

            PoyControl.CurrentView = View.Lite;
        }

        private void PoyControl_ChangeView(PoytingControl.View view)
        {
            switch (view)
            {
                case PoytingControl.View.Lite:
                    this.Width = 180;
                    break;
                case PoytingControl.View.Pro:
                    this.Width = 360;
                    break;
            }
        }

        public void SetDefaut()
        {
            PoyControl.SetDefaut();
        }

        public delegate void ReSendHexValues(object sender, int[] hexValues);
        public event ReSendHexValues SendHexValues;

        private void PoyControl_Gen4HexValues(int[] hexValues)
        {
            SendHexValues?.Invoke(this, hexValues);
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        public delegate void Event(object sender);
        public event Event NeedHide;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

        public void SetLanguage(string param)
        {
            bClose1.ToolTip = (param.ToLower().Contains("ru")) ? "Закрыть" : "Close";

            pTranslations.Languages language = pTranslations.Languages.EN;
            if (param.ToLower().Contains("ru")) language = pTranslations.Languages.RU;
            if (param.ToLower().Contains("en")) language = pTranslations.Languages.EN;
            if (param.ToLower().Contains("az")) language = pTranslations.Languages.AZ;
            PoyControl.CurrentLanguage = language;
        }

    

    }
}
