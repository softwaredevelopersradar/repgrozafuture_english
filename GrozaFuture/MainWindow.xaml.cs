﻿using ClientDataBase;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;
using TableEvents;
using ValuesCorrectLib;


namespace GrozaFuture
{
    using AttSettings;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();

            TranslatorTables.LoadDictionary(basicProperties.Local.Common.Language);
            InitDSP();
            InitPanoramaManager();
            InitMainPanelManager();
            InitAtt();
            InitTables();
            InitFHS();
            InitLocalProperties();
            SetResourcelanguage(basicProperties.Local.Common.Language);
            SetLanguageTables(basicProperties.Local.Common.Language);
            SetLanguageTab(basicProperties.Local.Common.Language);
            SetTitle(basicProperties.Local.Common.Language);

            this.basicProperties.Global.PropertyChanged += Global_PropertyChanged;
            InitDefaultRIButtons();
            InitArone();
            InitReceiverManager();
            InitVoiceDisturb();
            InitChat();
            InitRadant(basicProperties.Local.Common.Language);

            PropViewCoords.ViewCoords = ViewCoordToByte(basicProperties.Local.CoordinatesProperty.View);

            InitCompass();
            InitDataTransfer();
            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;

            DataContext = this;

            //ReInitAPD(basicProperties.Local);
            //ReInitPostControl(basicProperties.Local);

            InitAPlayer();

            InitSpoof();
            InitOmni();
            newWindow?.SetLanguage(basicProperties.Local.Common.Language);
        }

        private void Global_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(this.basicProperties.Global.RangeRadioRecon))
            {
                if (this.basicProperties.Global.RangeRadioRecon == EnumRangeRadioRecon.Range_0_6000)
                {

                    this.att.bClose1.Visibility = Visibility.Visible;
                    this.att.GainSett.Visibility = Visibility.Visible;
                    this.att.attSet.Visibility = Visibility.Hidden;
                    this.att.bCloseForAtt.Visibility = Visibility.Hidden;

                }
                else 
                {
                    this.att.attSet.Visibility = Visibility.Visible;
                    this.att.bCloseForAtt.Visibility= Visibility.Visible;
                    this.att.bClose1.Visibility = Visibility.Hidden;
                    this.att.GainSett.Visibility = Visibility.Hidden;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }



        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        private void ScrollViewer_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            try
            {
                System.Windows.Controls.ScrollViewer scroll = sender as System.Windows.Controls.ScrollViewer;
                if (scroll != null)
                {
                    if (e.Delta > 0)
                    {
                        scroll.LineRight();
                    }
                    else
                        scroll.LineLeft();
                }
                e.Handled = true;
            }
            catch(Exception)
            { }
        }

        private void Cmp_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.Invoke(() => 
            {
                SendToDbAngleRR();
                SendToDbAnglePA();                
            });
        }

        // функция смены языка
        public void SetLanguage(string lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("XMLTranslation.xml")) xDoc.Load("XMLTranslation.xml");
            else
            {
                switch (lang)
                {

                    case "rus":
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "eng":
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "az":
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case "azlat":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "azkir":
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }

            SortedList<string, string> Dictionary = new SortedList<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;

            string SectionName = "Main_Translation";
            foreach (XmlNode x1Node in xRoot)
            {
                if (x1Node.Name == SectionName)
                {
                    foreach (XmlNode x2Node in x1Node.ChildNodes)
                    {
                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {

                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - lang
                                    if (childnode.Name == lang)
                                    {
                                        if (!Dictionary.ContainsKey(attr.Value))
                                            Dictionary.Add(attr.Value, childnode.InnerText);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            renameControls(Dictionary);
        }

        //переименование всех контролов при смене языка
        private void renameControls(SortedList<string, string> Dictionary)
        {
            foreach (var control in leftButtonsPanel.Children)
            {
                if (control is Label)
                {
                    var l = (Label)control;

                    if (Dictionary.ContainsKey(l.Name))
                        l.Content = Dictionary[l.Name];

                    if (Dictionary.ContainsKey(l.Name + ".ToolTip"))
                        l.ToolTip = Dictionary[l.Name + ".ToolTip"];
                }
                if (control is Button)
                {
                    var b = (Button)control;

                    if (Dictionary.ContainsKey(b.Name))
                        b.Content = Dictionary[b.Name];

                    if (Dictionary.ContainsKey(b.Name + ".ToolTip"))
                        b.ToolTip = Dictionary[b.Name + ".ToolTip"];
                }
                if (control is ToggleButton)
                {
                    var t = (ToggleButton)control;

                    if (Dictionary.ContainsKey(t.Name))
                        t.Content = Dictionary[t.Name];

                    if (Dictionary.ContainsKey(t.Name + ".ToolTip"))
                        t.ToolTip = Dictionary[t.Name + ".ToolTip"];
                }
            }

            foreach (var item in PTabControl.Items)
            {
                var i = (TabItem)item;

                if (Dictionary.ContainsKey(i.Name))
                    i.Header = Dictionary[i.Name];

                if (Dictionary.ContainsKey(i.Name + ".ToolTip"))
                    i.ToolTip = Dictionary[i.Name + ".ToolTip"];
            }
        }

        public void SetControlsLanguage()
        {
            mPanel.SetLanguage(basicProperties.Local.Common.Language.ToString().ToLower());

            pLibrary.SetLanguage(basicProperties.Local.Common.Language.ToString().ToLower());

            att.SetLanguage(basicProperties.Local.Common.Language.ToString().ToLower());

            SetLanguage(basicProperties.Local.Common.Language.ToString().ToLower());

            FHSStateWindow.SetFHSLanguage(basicProperties.Local.Common.Language);

            spoof.SetLanguage(basicProperties.Local.Common.Language.ToString());

            omni.SetLanguage(basicProperties.Local.Common.Language.ToString().ToLower());

            SetLanguageRadant(basicProperties.Local.Common.Language);
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.O)
            {
                OmniButtonInteraction();
            }
        }

        //private void TEST_Click(object sender, RoutedEventArgs e)
        //{
        //    Coord coord = new Coord() { Altitude = 234.4, Latitude = -34.4646456457, Longitude = 23.6868989789 };
        //    ucASP.SetCoordsASPToPG(coord);
        //}
    }
}
