﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfControlLibraryBut;
using WpfControlDataGridAR6000;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void InitReceiverManager()
        {
            AroneConnetion.OnLvlSignal += AroneConnetion_OnLvlSignal;
            ReceiverTable.OnFreqSupress += new EventHandler<WpfControlLibraryBut.ToSupressEventArgs>(DataToTable);            

            ControlArOneSec.OnLvlSignal += ControlArOneSec_OnLvlSignal;
            ReceiverTableArOneSec.OnFreqSupress += new EventHandler<WpfControlLibraryBut.ToSupressEventArgs>(DataToTable);

            ControlAR6000Sec.OnDataToSave += ControlAR6000_OnDataToSave;
            ControlAR6000Sec.OnLvlSignal += ControlAR6000Sec_OnLvlSignal;

            ControlAR6000First.OnDataToSave += ControlAR6000First_OnDataToSave;
            ControlAR6000First.OnLvlSignal += ControlAR6000First_OnLvlSignal;

            ControlAR6000First.OnScanInformation += ControlAR6000First_OnScanInformation;
            ControlAR6000Sec.OnScanInformation += ControlAR6000Sec_OnScanInformation;

            EventsOfTableControlsAR6000();
            InitScanEvents();
        }

        private void EventsOfTableControlsAR6000() 
        {
            ReceiverTableFirst.OnSendScanDelayTime += ReceiverTableFirst_OnSendScanDelayTime;
            ReceiverTableFirst.OnSendScanPauseTime += ReceiverTableFirst_OnSendScanPauseTime;

            ReceiverTableSec.OnSendScanDelayTime += ReceiverTableSec_OnSendScanDelayTime;
            ReceiverTableSec.OnSendScanPauseTime += ReceiverTableSec_OnSendScanPauseTime;

            ReceiverTableFirst.OnFreqSupress += ReceiverTableSec_OnFreqSupress;
            ReceiverTableSec.OnFreqSupress += ReceiverTableFirst_OnFreqSupress;
        }

        private void InitScanEvents() 
        {
            ReceiverTableFirst.OnRequestReceiverStatus += ReceiverTableFirst_OnRequestReceiverStatus;
            ControlAR6000First.OnSendScanModeStatus += ControlAR6000First_OnSendScanModeStatus;

            ReceiverTableSec.OnRequestReceiverStatus += ReceiverTableSec_OnRequestReceiverStatus;
            ControlAR6000Sec.OnSendScanModeStatus += ControlAR6000Sec_OnSendScanModeStatus;
        }

        private void ControlAR6000Sec_OnSendScanModeStatus(object sender, string data, Int64 frequency)
        {
            ReceiverTableSec.DecodedScanMode(data);
            ReceiverTableSec.ScanFrequency = frequency;
        }

        private void ReceiverTableSec_OnRequestReceiverStatus(bool value)
        {
            ControlAR6000Sec.GetScanStatus();
        }

        private void ControlAR6000First_OnSendScanModeStatus(object sender, string data, Int64 frequency)
        {
            ReceiverTableFirst.DecodedScanMode(data);
            ReceiverTableFirst.ScanFrequency = frequency;
        }

        private void ReceiverTableFirst_OnRequestReceiverStatus(bool value)
        {
            ControlAR6000First.GetScanStatus();
        }

        #region TOP CONTROLS
        private void ControlAR6000First_OnLvlSignal(object sender, WpfControlLibraryAR6000.LevelOfSignalsEventArgs e)
        {
            ReceiverTableFirst.SignalLvl = e.SignalLvl;
            ReceiverTableFirst.SqlLvl = e.SqlLvl;
        }

        private void ControlAR6000First_OnDataToSave(object sender, WpfControlLibraryAR6000.DataToSaveEventArgs e)
        {
            ReceiverTableFirst.Frequency = e.Frequency;
            ReceiverTableFirst.Modulation = e.Modulation;
            ReceiverTableFirst.Attenuator = e.Attenuator;
            ReceiverTableFirst.Bandwidth = e.Bandwidth;
        }

        private void AroneConnetion_OnLvlSignal(object sender, LevelOfSignalsEventArgs e)
        {
            ReceiverTable.SignalLvl = e.SignalLvl;
            ReceiverTable.SqlLvl = e.SqlLvl;
            ReceiverTable.Frequency = e.Frequency;
        }

        private void ReceiverTableFirst_OnSendScanPauseTime(object sender, ScanInformation e)
        {
            ControlAR6000First.ScanPauseTimeSet(e.Parameter);
        }

        private void ReceiverTableFirst_OnSendScanDelayTime(object sender, ScanInformation e)
        {
            ControlAR6000First.ScanDelayTimeSet(e.Parameter);
        }

        private void ControlAR6000First_OnScanInformation(object sender, WpfControlLibraryAR6000.ScanInformationEventArgs e)
        {
            ReceiverTableFirst.ScanDelayTime = e.Delay;
            ReceiverTableFirst.ScanPauseTime = e.Pause;
        }
        #endregion

        #region BOTTOM CONTROLS
        private void ControlArOneSec_OnLvlSignal(object sender, LevelOfSignalsEventArgs e)
        {
            ReceiverTableArOneSec.SignalLvl = e.SignalLvl;
            ReceiverTableArOneSec.SqlLvl = e.SqlLvl;
            ReceiverTableArOneSec.Frequency = e.Frequency;
        }

        private void ControlAR6000Sec_OnLvlSignal(object sender, WpfControlLibraryAR6000.LevelOfSignalsEventArgs e)
        {
            ReceiverTableSec.SignalLvl = e.SignalLvl;
            ReceiverTableSec.SqlLvl = e.SqlLvl;
        }

        private void ControlAR6000_OnDataToSave(object sender, WpfControlLibraryAR6000.DataToSaveEventArgs e)
        {
            ReceiverTableSec.Frequency = e.Frequency;
            ReceiverTableSec.Modulation = e.Modulation;
            ReceiverTableSec.Attenuator = e.Attenuator;
            ReceiverTableSec.Bandwidth = e.Bandwidth;
        }

        private void ReceiverTableSec_OnSendScanPauseTime(object sender, ScanInformation e)
        {
            ControlAR6000Sec.ScanPauseTimeSet(e.Parameter);
        }

        private void ReceiverTableSec_OnSendScanDelayTime(object sender, ScanInformation e)
        {
            ControlAR6000Sec.ScanDelayTimeSet(e.Parameter);
        }

        private void ControlAR6000Sec_OnScanInformation(object sender, WpfControlLibraryAR6000.ScanInformationEventArgs e)
        {
            ReceiverTableSec.ScanDelayTime = e.Delay;
            ReceiverTableSec.ScanPauseTime = e.Pause;
        }
        #endregion

        public void DataToTable(object sender, WpfControlLibraryBut.ToSupressEventArgs e)
        {
            var Id = e.ID;
            switch (Id)
            {
                case 0:
                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.RadioRecevier,
                        NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                        FreqKHz = e.FrqSupress / 1000,
                        Bearing = -1,
                        Letter = TableOperations.DefinitionParams.DefineLetter(e.FrqSupress / 1000),
                        Threshold = -1,
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 4
                        },
                    };

                    UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    break;                    
                case 1:
                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 0,
                        FreqKHz = e.FrqSupress / 1000,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = (float)(e.BwSupress * 1000.0),
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                    Bearing = -1,
                                    Level = -1,
                                    Std = -1,
                                    DistanceKM = -1,
                                    IsOwn = true
                                }
                            }
                        }
                    };

                    UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                    break;
                case 2:
                    double freqMHz = e.FrqSupress / 1000000.0;
                    double freqWidthMHz = e.BwSupress;                    
                    ExternalExBearing2(freqMHz, freqWidthMHz);
                    break;
            }
        }

        private void ReceiverTableSec_OnFreqSupress(object sender, WpfControlDataGridAR6000.ToSupressEventArgs e)
        {
            var Id = e.ID;
            switch (Id)
            {
                case 0:
                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.RadioRecevier,
                        NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                        FreqKHz = e.FrqSupress / 1000,
                        Bearing = -1,
                        Letter = TableOperations.DefinitionParams.DefineLetter(e.FrqSupress / 1000),
                        Threshold = -1,
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 4
                        },
                    };

                    UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    break;
                case 1:
                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 0,
                        FreqKHz = e.FrqSupress / 1000,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = (float)(e.BwSupress * 1000.0),
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                    Bearing = -1,
                                    Level = -1,
                                    Std = -1,
                                    DistanceKM = -1,
                                    IsOwn = true
                                }
                            }
                        }
                    };

                    UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                    break;
                case 2:
                    double freqMHz = e.FrqSupress / 1000000.0;
                    double freqWidthMHz = e.BwSupress;
                    ExternalExBearing2(freqMHz, freqWidthMHz);
                    break;
            }
        }

        private void ReceiverTableFirst_OnFreqSupress(object sender, WpfControlDataGridAR6000.ToSupressEventArgs e)
        {
            var Id = e.ID;
            switch (Id)
            {
                case 0:
                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.RadioRecevier,
                        NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                        FreqKHz = e.FrqSupress / 1000,
                        Bearing = -1,
                        Letter = TableOperations.DefinitionParams.DefineLetter(e.FrqSupress / 1000),
                        Threshold = -1,
                        Priority = 2,
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = 0,
                            Modulation = 1,
                            Deviation = 8,
                            Duration = 4
                        },
                    };

                    UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                    break;
                case 1:
                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 0,
                        FreqKHz = e.FrqSupress / 1000,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = (float)(e.BwSupress * 1000.0),
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                        {
                            new TableJamDirect
                            {
                                JamDirect = new JamDirect
                                {
                                    NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                    Bearing = -1,
                                    Level = -1,
                                    Std = -1,
                                    DistanceKM = -1,
                                    IsOwn = true
                                }
                            }
                        }
                    };

                    UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                    break;
                case 2:
                    double freqMHz = e.FrqSupress / 1000000.0;
                    double freqWidthMHz = e.BwSupress;
                    ExternalExBearing2(freqMHz, freqWidthMHz);
                    break;
            }
        }

    }
}
