﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GainControl;

namespace GrozaFuture
{
    /// <summary>
    /// Interaction logic for Att.xaml
    /// </summary>
    public partial class Att : Window
    {
        public Att()
        {
            InitializeComponent();
            this.GainSett.SliderValue += GainSett_SliderValue;
            this.GainSett.RequestFromServer += GainSett_RequestFromServer;
            attSet.AttValueChange += AttSet_AttValueChange;
            attSet.NeedGetRequest += AttSet_NeedGetRequest;
            
        }

        public delegate void GainRequest(int numbSSR);
        public event GainRequest OnGainRequest;

        private void GainSett_RequestFromServer(EPO epo)
        {
           OnGainRequest?.Invoke(epo.NumberOfSSR);
        }

        public delegate void GainValue(float curAmp, int numbSSR);
        public event GainValue OnGainValueChange;

        private void GainSett_SliderValue(EPO epo)
        {
            OnGainValueChange?.Invoke(epo.CurrentAmp, epo.NumberOfSSR);
        }

        private void AttSet_NeedGetRequest(object sender, int BandBumber)
        {
            NeedGetRequest?.Invoke(sender, BandBumber);
        }

        public delegate void AttValueChangeEvent(object sender, int BandBumber, double AttValue);
        public event AttValueChangeEvent AttValueChange;

        public delegate void NeedGetRequestEvent(object sender, int BandBumber);
        public event NeedGetRequestEvent NeedGetRequest;

        private void AttSet_AttValueChange(object sender, int BandBumber, double AttValue)
        {
            AttValueChange?.Invoke(sender, BandBumber, AttValue);
        }

        public void InitValues(int Band, double Att, double Amp)
        {
            attSet.Band = Band;
            attSet.Att = Att;
            attSet.Amp = Amp;
        }


        public void SetGainValue(float SliderValue, int numberSSR)
        {

            this.GainSett.SetGainValue(SliderValue, numberSSR);
           
        }


        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        public delegate void Event(object sender);
        public event Event NeedHide;

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

        public void SetLanguage(string param)
        {
            bClose1.ToolTip = (param.ToLower().Contains("ru")) ? "Закрыть" : "Close";
            //attSet.SetLanguage(param);
            this.GainSett.UpdateLanguage(param);

        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }
    }
}