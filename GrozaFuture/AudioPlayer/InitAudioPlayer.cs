﻿using AudioPlayerControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {        
        private AudioPlayer.APlayerWindow AudioPlayer = new AudioPlayer.APlayerWindow();

        public void InitAPlayer()
        {
            AudioPlayer = new AudioPlayer.APlayerWindow();
            AudioPlayer.HidePlayer += AudioPlayer_HidePlayer;
        }

        private void AudioPlayer_HidePlayer(object sender, EventArgs e)
        {
            bAudioPlayer.IsChecked = false;
        }

        private void bAudioPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (AudioPlayer.IsVisible)
            {
                AudioPlayer.Hide();                
            }
                
            else
            {                
                AudioPlayer.Show();                
            }
            
        }
    }
}
