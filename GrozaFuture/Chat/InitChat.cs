﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using UserControl_Chat;

namespace GrozaFuture
{
    using System.Threading;

    public partial class MainWindow
    {
        private Chat newWindow;
        private Buble chatBuble;

        private void InitChat()
        {
            newWindow = new Chat();
            chatBuble = new Buble();
	        newWindow.OnReturnApprovedMessages += NewWindow_OnReturnApprovedMessages;
            newWindow.SetStations();
            Events.OnClosingChat += DeactivatChatButton;
        }

	    private async void NewWindow_OnReturnApprovedMessages(object sender, List<Message> messages)
        {
            try
            {
                var lastMessage = messages.Last();
                if (lastMessage.Id == 255)
                {
                    
                    await Server_SendMessage(lastMessage.MessageFiled).ConfigureAwait(false); //TODO: check
                    //Dispatcher.Invoke(() =>
                    //{
                        
                    //});
                }
                else
                {
                    //TODO: переделать с учетом ПУ
                    foreach (UserControl_Chat.Message curMessage in messages)
                    {
                        if(!GetSideMenu().Contains(curMessage.Id))
                            continue; 
                        dTClient?.SendMessage(curMessage.Id, curMessage.MessageFiled);
                    }
                       
                }
            }
            catch { }
        }

        private void UpdateSideMenu(List<TableASP> ASPList)
        {
            newWindow.UpdateSideMenu(ASPList);
        }


        private void ChatButton_Click(object sender, RoutedEventArgs e)
        {
            if (newWindow.IsVisible)
                newWindow.Hide();
            else
                newWindow.Show();

        }

        private void DeactivatChatButton()
        {
            ChatButton.IsChecked = false;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            System.Windows.Threading.Dispatcher.ExitAllFrames();
            //newWindow.Close();
        }

        private List<int> GetSideMenu()
        {
            return newWindow.SideMenuList.Select(t => t.Id).ToList();
        }
    }
}
