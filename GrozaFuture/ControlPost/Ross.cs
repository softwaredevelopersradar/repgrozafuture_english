﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ClientDataBase;
using Database934;
using DataTransferModels;
using DataTransferModels.DB;
using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using GrozaBerezinaDLL;
using ModelsTablesDBLib;
using TransmissionLib.GrpcTransmission;
using TransmissionPackageGroza934;

namespace GrozaFuture
{
    public partial class MainWindow : IMediator
    {
        private bool _isRossConnected = false;
        private GrpcServer serverForCP;
        private int ownAddress = 0;
        private int rossAddress = 255;

        private void InitializeRoss(LocalProperties arg)
        {
            if (!arg.PCProperties.IsRossVisible) return;

            //if (_isRossConnected) ShutDownRoss();

            if (!arg.PCProperties.State) return;

            StartServerForRoss(arg);
        }

        private void StartServerForRoss(LocalProperties args)
        {
            try
            {
                //todo: check id 
                ownAddress = lASP.First(t => t.ISOwn).Id;
                
                switch (args.PCProperties.TypeOfConection)
                {
                    case TypeOfConection.RadioModem:
                        serverForCP = new GrpcServer(
                            args.PCProperties.IpAddressRadioModem,
                            args.PCProperties.PortRadioModem,
                            this,
                            255,
                            ownAddress);
                        break;

                    case TypeOfConection._3G:
                        serverForCP = new GrpcServer(
                            args.PCProperties.IpAddress3G4GRouter,
                            args.PCProperties.Port3G4GRouter,
                            this,
                            255,
                            ownAddress);
                        break;

                    default:
                        return;
                }
                serverForCP.Initialize();
                _isRossConnected = true;
                Dispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Normal,
                    (ThreadStart)delegate() { PCControlConnection.ShowConnect(); });
            }
            catch 
            {
            }
        }
        
        private void ShutDownRoss()
        {
            try
            {
                //serverForCP.OnTableFreqImportantReceived -= ServerForCP_OnTableFreqImportantReceived;
                //serverForCP.OnTableFreqForbiddenReceived -= ServerForCP_OnTableFreqForbiddenReceived;
                //serverForCP.OnTableFreqKnownReceived -= ServerForCP_OnTableFreqKnownReceived;
                
                serverForCP.ShutDown();
                serverForCP = null;
                _isRossConnected = false;
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    PCControlConnection.ShowDisconnect();
                });
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + e.StackTrace);
            }
        }

        public IDatabaseController DatabaseController { get; }
        public IDataStorage DataStorage { get; }
        public void DB_ReactOnAspUpdate(Station own, IList<Station> linked)
        {
            throw new NotImplementedException();
        }

        public void DB_ReactOnForbiddenFrequencies(IList<object> data)
        {
            throw new NotImplementedException();
        }

        private object locker = new object();
        private async Task UpdateTable(NameTable table, object updatedTable)
        {
            var ownId = lASP.FirstOrDefault(x => x.ISOwn == true).Id;

            if (this.clientDB == null) return;

            lock (locker)
            {
                clientDB.Tables[table].Clear();
                //await Task.Delay(500);
                clientDB.Tables[table].AddRange(updatedTable);
            }
        }

        public async Task Server_ReactOnForbiddenFrequencies(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableFreqForbidden).ToList<TableFreqForbidden>();
            await UpdateTable(NameTable.TableFreqForbidden, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnKnownFrequencies(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableFreqKnown).ToList<TableFreqKnown>();
            await UpdateTable(NameTable.TableFreqKnown, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnImportantFrequencies(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableFreqImportant).ToList<TableFreqImportant>();
            await UpdateTable(NameTable.TableFreqImportant, records).ConfigureAwait(false);
        }


        public async Task Server_ReactOnAsp(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableASP).ToList<TableASP>();
            await UpdateTable(NameTable.TableASP, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnTextMessage(object data)
        {
            try
            {
                _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        chatBuble?.SetMessage(data as string);
                        newWindow.DrawReceivedMessage(rossAddress, data as string);
                    });

                if (clientDB == null) return;
                var message = new TableChatMessage() { SenderAddress = rossAddress, ReceiverAddress = ownAddress, Time = DateTime.Now, Status = ChatMessageStatus.Delivered, Text = data as string};
                await clientDB.Tables[NameTable.TableChat].AddAsync(message).ConfigureAwait(false);
            }
            catch
            {
            }
        }

        public async Task Server_ReactOnTableSectorsRangesElint(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableSectorsRangesRecon).ToList<TableSectorsRangesRecon>();
            await UpdateTable(NameTable.TableSectorsRangesRecon, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnSectorsRangesJamming(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableSectorsRangesSuppr).ToList<TableSectorsRangesSuppr>();
            await UpdateTable(NameTable.TableSectorsRangesSuppr, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnTableFwsJamming(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableSuppressFWS).ToList<TableSuppressFWS>();
            await UpdateTable(NameTable.TableSuppressFWS, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnTableFhssJamming(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableSuppressFHSS).ToList<TableSuppressFHSS>();
            await UpdateTable(NameTable.TableSuppressFHSS, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnTableFhssExcludedJamming(object data)
        {
            var records = (data as RepeatedField<Any>).ConvertToDBModel(NameTable.TableFHSSExcludedFreq).ToList<TableFHSSExcludedFreq>();
            await UpdateTable(NameTable.TableFHSSExcludedFreq, records).ConfigureAwait(false);
        }

        public async Task Server_ReactOnMode(object data)
        {
            MainPanel.MPanel.Buttons buttons = MainPanel.MPanel.Buttons.Preparation;
            switch ((int)data)
            {
                case 0:
                    buttons = MainPanel.MPanel.Buttons.Preparation;
                    break;
                case 1:
                    buttons = MainPanel.MPanel.Buttons.RadioIntelligence;
                    break;
                case 2:
                    buttons = MainPanel.MPanel.Buttons.RadioSuppression;
                    break;
                default:
                    buttons = MainPanel.MPanel.Buttons.Preparation;
                    break;
            }
            //Task.Run(() => MPanel_OnEnumButtonClick(this, buttons));
            //TODO: мб ожидание установки режима

            DispatchIfNecessary(() =>
            {
                MPanel_OnEnumButtonClick(this, buttons);
            });
        }

        public async Task Server_ReactOnLocalTime(object data)
        {
            var time = (data as Timestamp).ToDateTime();
            SYSTEMTIME st = new SYSTEMTIME();
            st.wYear = (short)time.Year;
            st.wMonth = (short)time.Month;
            st.wDay = (short)time.Day;
            st.wHour = (short)(time.Hour); 
            st.wMinute = (short)(time.Minute);
            st.wSecond = (short)time.Second;

            SetSystemTime(ref st);
        }

        public async Task Server_SendMessage(object data)
        {
            if(!_isRossConnected || clientDB == null) return;
            
            var message = new TableChatMessage() { SenderAddress = ownAddress, ReceiverAddress = 255, Time = DateTime.Now, Status = ChatMessageStatus.Sent, Text = data as string };
            await clientDB.Tables[NameTable.TableChat].AddAsync(message).ConfigureAwait(false);
            OnSendMessage?.Invoke(this, (string)data);
        }

        public async Task Server_ConfirmLastMessage()
        {
            try
            {
                _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,(ThreadStart)delegate()
                {
                    newWindow.ConfirmSentMessage(this.rossAddress);
                });

                if (this.clientDB == null)
                    return;

                await Task.Delay(100); //TODO: придумать что-то поуниверсальней
                var last = lChatMessages.Last(t => t.ReceiverAddress == rossAddress);
                last.Status = ChatMessageStatus.Delivered;
                await clientDB.Tables[NameTable.TableChat].ChangeAsync(last).ConfigureAwait(false);
            }
            catch
            { }
        }

        public async Task Server_OnClientConnectionChanged(bool isConnected)
        {
            _ = Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (isConnected)
                {
                    PCControlConnection.ShowConnectClient();
                }
                else
                {
                    PCControlConnection.ShowDisconnectClient();
                }
            });
        }

        public async Task<object> GetAsp(CancellationToken token = default)
        {
            var forSend = new List<TableASP>(this.lASP);
            var ownAsp = forSend.FirstOrDefault(t => t.ISOwn);
            if (ownAsp != null)
            {
                forSend.FirstOrDefault(x => x.ISOwn == true).Letters = lettersForRoss;
            }
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(forSend).ConvertToProto(NameTable.TableASP); //TODO: check
            return data;
        }

        private byte[] lettersForRoss;
        private void FillLettersForRoss(int letterCount, short[] power, byte[] errors)
        {
            lettersForRoss = new byte[letterCount];
            for (int i = 0; i < letterCount; i++)
            {
                lettersForRoss[i] = power[i] > 0 ? (byte)2 : (byte)(errors[i] | errors[i+1]);
            }
        }

        public async Task<object> GetFwsElint()
        {
            var fromDb = await clientDB.Tables[NameTable.TempFWS].LoadAsync<TempFWS>();
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(fromDb).ConvertToProto(NameTable.TempFWS);
            return data;
        }

        public async Task<object> GetFwsElintDistribution()
        {
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(lReconFWS).ConvertToProto(NameTable.TableReconFWS);
            return data;
        }

        public async Task<object> GetFhssElint()
        {
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(lReconFHSS).ConvertToProto(NameTable.TableReconFHSS);
            return data;
        }

        private IList<TempSuppressFWS> ListStateSuppFWS; // Cостояние ФРЧ на РП
        public async Task<object> GetFwsJamming()
        {
            //throw new NotImplementedException();
            var fromDb = await clientDB.Tables[NameTable.TempSuppressFWS].LoadAsync<TempSuppressFWS>();
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(fromDb).ConvertToProto(NameTable.TempSuppressFWS);
            return data;
        }

        public async Task<object> GetFhssJamming()
        {
            var fromDb = await clientDB.Tables[NameTable.TempSuppressFHSS].LoadAsync<TempSuppressFHSS>();
            var data = ClassDataCommon.ConvertToListAbstractCommonTable(fromDb).ConvertToProto(NameTable.TempSuppressFHSS);
            return data;
        }

        public async Task<object> GetCoordinates()
        {
            var data = lASP.First(t=>t.ISOwn).Coordinates;
            var result = Any.Pack(TypesConverter.ConvertToProto(data));
            return result;
        }

        public async Task<object> GetAntennasDirection()
        {
            var asp = lASP.FirstOrDefault(t => t.ISOwn);
            var antennas = new AntennasMessage();
            if (asp != null)
            {
                var fillList = this.FillAntennas(asp, new List<int>() { asp.LPA13, asp.LPA24, asp.LPA13, asp.LPA24, 0, 0, 0, 0, 0, 0 });
                antennas.Lpa.AddRange(fillList);
            }
            antennas.Communication.AddRange(new List<int>() { 0, 0 });
            var result = Any.Pack(antennas);
            return result;
        }

        //Запрос на квазиодновременное пеленгование
        public async Task<QuasiSimultaneousDF> GetQuasiSimultaneousDF(int stationId, double frequencyKHz, float bandKHz)
        {
            try
            {
                if (bandKHz <= 0) bandKHz = 3.6f;

                var minFrequencyKHz = frequencyKHz - bandKHz;
                var maxFrequencyKHz = frequencyKHz + bandKHz;

                var phAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? 3 : basicProperties.Global.NumberAveragingPhase;
                var plAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? 3 : basicProperties.Global.NumberAveragingBearing;

                var answer = await dsp.QuasiSimultaneouslyDF((int)minFrequencyKHz, (int)maxFrequencyKHz, (byte)phAvCount, (byte)plAvCount);
                //var answer = await dsp.QuasiSimultaneouslyDFX10(iFmin, iFmax, 3, 3);

                short matedBearing = -1;

                if (answer?.Header.ErrorCode == 0)
                {
                    if (answer.Source.Direction != -1)
                    {
                        if (answer.LinkedStationResults.Count() > 0)
                        {
                            matedBearing = (short)(answer.LinkedStationResults[0].Direction * 10);
                        }
                    }
                }

                return new QuasiSimultaneousDF(stationId, answer.Source.Frequency, (float)(answer.Source.Direction == -1 ? (361) : Math.Round(answer.Source.Direction / 10.0d)),
                    (float)(matedBearing == -1 ? (361) : Math.Round(matedBearing / 10.0d)));
            }
            catch
            {
                return new QuasiSimultaneousDF(stationId, 0, 0,0);
            }
        }

        //Запрос на исполнительное пеленгование
        public async Task<ExecutiveDF> GetExecutiveDF(int stationId, double frequencyKHz, float bandKHz)
        {
            try
            {
                if (bandKHz <= 0) bandKHz = 3.6f;

                var minFrequencyMHz = (frequencyKHz - bandKHz) / 1000d;
                var maxFrequencyMHz = (frequencyKHz + bandKHz) / 1000d;

                var phAvCount = (basicProperties.Global.NumberAveragingPhase <= 0) ? 3 : basicProperties.Global.NumberAveragingPhase;
                var plAvCount = (basicProperties.Global.NumberAveragingBearing <= 0) ? 3 : basicProperties.Global.NumberAveragingBearing;
                
                var answer = await dsp.ExecutiveDF(minFrequencyMHz, maxFrequencyMHz, phAvCount, plAvCount);
               
                return new ExecutiveDF(stationId, answer.Frequency, (float)(answer.Direction == -1 ? (361) : Math.Round(answer.Direction / 10.0d))); //TODO: проверить частоту
            }
            catch
            {
                return new ExecutiveDF(0, 0, 0); 
            }
        }

        private List<int> FillAntennas(TableASP asp, List<int> result)
        {
            if (basicProperties.Local.ARD1.State)
                CheckDeviceRSD(basicProperties.Local.ARD1.Device, result, asp);

            if (basicProperties.Local.ARD2.State)
                CheckDeviceRSD(basicProperties.Local.ARD1.Device, result, asp);

            if (basicProperties.Local.ARD3.State)
                CheckDeviceRSD(basicProperties.Local.ARD1.Device, result, asp);

            return result;
        }

        private List<int> CheckDeviceRSD(DeviceRSD ard, List<int> all, TableASP asp)
        {
            switch (ard)
            {
                case DeviceRSD.LPA_5_10:
                    all[4] = all[5] = all[6] = all[7] = all[8] = all[9] = asp.LPA510;
                    break;

                case DeviceRSD.LPA_5_7:
                    all[4] = all[5] = all[6]  = asp.LPA57;
                    break;

                case DeviceRSD.LPA_5_9:
                    all[4] = all[5] = all[6] = all[7] = all[8] = asp.LPA59;
                    break;

                case DeviceRSD.LPA_10б:
                    all[9] = asp.LPA10;
                    break;
                default:
                    break;
            }
            return all;
        }

        public Task EstablishConnections()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<string> OnSendMessage;
    }
}
