﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using TableEvents;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        public void InitMainPanelManager()
        {
            mPanel.OnEnumButtonClick += MPanel_OnEnumButtonClick;

            mPanel.OnMenuClick += MPanel_OnMenuClick;
        }

        public void UpdateLocalProperties4MainPanel(LocalProperties arg)
        {
            //string s = arg.Common.NumberARM.ToString().Substring(3, arg.Common.NumberARM.ToString().Length - 3);
            mPanel.AWPNumber = arg.Common.NumberARM;
            mPanel.AccessAWP = mPanelAccessAWPConverter(arg.Common.AccessARM);

            //DispatchIfNecessary(() =>
            //{
            //   mPanel.SetLanguage(arg.Common.Language.ToString().ToLower());
            //});
        }

        public void UpdateGlobalProperties4MainPanel(GlobalProperties arg)
        {
            //mPanel.SRIText = $"СП {arg.Number}";
            //mPanel.RoleText = RoleRusConverter(arg.Role);
        }

        public MainPanel.MPanel.AccessType mPanelAccessAWPConverter(TypeAccessARM AccessARM)
        {
            switch (AccessARM)
            {
                case TypeAccessARM.Admin:
                    return MainPanel.MPanel.AccessType.Admin;
                case TypeAccessARM.Commander:
                    return MainPanel.MPanel.AccessType.Commandor;
                case TypeAccessARM.Operator:
                    return MainPanel.MPanel.AccessType.Operator;
                default:
                    return MainPanel.MPanel.AccessType.Operator;
            }
        }

        public MainPanel.MPanel.StationRole RoleConverter(RoleStation roleStation)
        {
            switch (roleStation)
            {
                case RoleStation.Autonomic:
                    return MainPanel.MPanel.StationRole.Autonomic;
                case RoleStation.Master:
                    return MainPanel.MPanel.StationRole.Master;
                case RoleStation.Slave:
                    return MainPanel.MPanel.StationRole.Slave;
                default:
                    return MainPanel.MPanel.StationRole.Autonomic;
            }
        }

        public void UpdateTableASP4MainPanel(List <TableASP> lTableASP)
        {

            int ind = lTableASP.FindIndex(x => x.ISOwn == true);
            if (ind != -1)
            {
                mPanel.SRINumber = lTableASP[ind].Id;
                mPanel.RoleStation = RoleConverter(lTableASP[ind].Role);
            }
            else
            {
                mPanel.SRINumber = 111;
            }

            //mPanel.SRIText = $"СП {lTableASP[0].Id}";
            //mPanel.RoleText = RoleRusConverter(lTableASP[0].Role);
        }

        private async void MPanel_OnEnumButtonClick(object sender, MainPanel.MPanel.Buttons buttons)
        {
            switch (buttons)
            {
                case MainPanel.MPanel.Buttons.Preparation:
                    {
                        //Установка режима работы с эфира
                        var answer00 = await dsp.SetReceiversChannel(0);

                        //установка режима
                        var answer = await dsp.SetMode(0);

                        if (answer?.Header.ErrorCode == 0)
                        {
                            mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
                            pLibrary.Mode = 0;
                            AttCheck(0);

                            //Для сохранения в Docx файл
                            TableSave.Mode(pLibrary.Mode, (byte)basicProperties.Local.Common.FileType);
                        }

                        break;
                    }
                case MainPanel.MPanel.Buttons.RadioIntelligence:
                    {
                        //Установка режима работы с эфира
                        var answer00 = await dsp.SetReceiversChannel(0);

                        //установка фильтов для обнаружения ИРИ
                        var answer = await dsp.SetFilters(pLibrary.Threshold, 0, 0, 0); //Шифр 18
                        
                        //установка режима
                        int Mode = DesiredRadioReconMode();
                        answer = await dsp.SetMode(Mode);
                        AttCheck(Mode);

                        if (answer?.Header.ErrorCode == 0)
                        {
                            mPanel.Highlight = MainPanel.MPanel.Buttons.RadioIntelligence;
                            pLibrary.Mode = Mode;

                            if (Mode == 2)
                            {
                                byte desireDetectionFHSS = DesireDetectionFHSS();
                                answer = await dsp.SetSearchFHSS(desireDetectionFHSS);
                            }

                            //Для сохранения в Docx файл
                            TableSave.Mode(pLibrary.Mode, (byte)basicProperties.Local.Common.FileType);
                        }

                        break;
                    }
                case MainPanel.MPanel.Buttons.RadioSuppression:
                    {

                        //Установка режима работы с эфира
                        var answer00 = await dsp.SetReceiversChannel(0);

                        //выбор желаемого режима
                        int Mode = DesiredRadioSupprMode();
                        AttCheck(Mode);

                        //задание установок выбранного режима, ну а тут конкретно для голосовой помехи
                        if (Mode == 5)
                        {
                            int FrequencykHz = (int)(VoiceDisturb.Frequency * 1000);
                            byte DeviationCode = VoiceDisturb.Deviation;
                            var VoiceJammingMode = (VoiceDisturb.TakeFileIndex == 0) ? DspDataModel.RadioJam.VoiceJammingMode.FromFile : DspDataModel.RadioJam.VoiceJammingMode.RealtimeCapture;
                            string FilePath = (VoiceDisturb.FileName != null) ? VoiceDisturb.FileName : "";

                            var answerVoiceJamming = await dsp.SetVoiceJammingSettings(FrequencykHz, DeviationCode, VoiceJammingMode, FilePath);
                        }

                        //установка режима
                        var answer = await dsp.SetMode(Mode);

                        if (answer?.Header.ErrorCode == 0)
                        {
                            mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                            pLibrary.Mode = Mode;

                            //Для сохранения в Docx файл
                            TableSave.Mode(pLibrary.Mode, (byte)basicProperties.Local.Common.FileType);
                            if (pLibrary.Mode == 3 || pLibrary.Mode == 4)
                                ListFormer(lSuppressFWS, null);
                            else if (pLibrary.Mode == 6)
                                ListFormer(null, lSuppressFHSS);
                        }

                        break;
                    }
            }
        }

        GridLength SavedGridLength;
        GridLength SavedGridLengthTwo;

        private void MPanel_OnMenuClick(object sender)
        {
            if (BaseField.ColumnDefinitions[0].Width != new GridLength(0))
            {
                SavedGridLength = BaseField.ColumnDefinitions[0].Width;
                SavedGridLengthTwo = BaseField.ColumnDefinitions[2].Width;
                BaseField.ColumnDefinitions[0].Width = new System.Windows.GridLength(0);
                Lp_Splitter(false);
            }
            else
            {
                if(SavedGridLength.Value == 0)
                {
                    BaseField.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);   
                    BaseField.ColumnDefinitions[2].Width = new GridLength(1.4, GridUnitType.Star);
                }
                else
                {
                    BaseField.ColumnDefinitions[0].Width = SavedGridLength;   
                    BaseField.ColumnDefinitions[2].Width = SavedGridLengthTwo;
                }
                Lp_Splitter(true);
            }
        }

        private void Lp_Splitter(bool value)
        {
            if (value)
            {
                gridGeneralSplitterV.IsEnabled = true; // Lp Splitter
            }
            else
            {
                gridGeneralSplitterV.IsEnabled = false; // Lp Splitter
            }

        }

        private void ModeToMainPanelHighlight(byte Mode)
        {
            switch (Mode)
            {
                case 0:
                    mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
                    break;
                case 1:
                case 2:
                    mPanel.Highlight = MainPanel.MPanel.Buttons.RadioIntelligence;
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                    mPanel.Highlight = MainPanel.MPanel.Buttons.RadioSuppression;
                    break;
                default:
                    mPanel.Highlight = MainPanel.MPanel.Buttons.Preparation;
                    break;
            }
        }
    }
}
