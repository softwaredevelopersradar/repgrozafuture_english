﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public void ChangeLangVD()
        {
            var getLang = basicProperties.Local.Common.Language.ToString();
            VoiceDisturb.Language(getLang);
        }
    }
}
