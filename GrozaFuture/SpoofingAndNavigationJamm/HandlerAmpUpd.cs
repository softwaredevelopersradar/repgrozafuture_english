﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow : Window
    {
        private bool _waitAnswerAmp = false;

        System.Timers.Timer tmrGetStatusAmplifier = new System.Timers.Timer();

        //List<State> stateAmp = new List<State>();

        private void ComAmp_OnConfirmStatus(object sender, ConfirmStatusEventArgs confirmStatusEventArgs)
        {
            Console.WriteLine(confirmStatusEventArgs.ParamAmp);
            List<StateNAVControl.StateModel> Params = new List<StateNAVControl.StateModel>();
            var error = StateNAVControl.Led.Red;
            List<StateNAVControl.Led> errors = new List<StateNAVControl.Led>();

            foreach(var ant in confirmStatusEventArgs.ParamAmp)
            {
                Console.WriteLine(ant.Antenna);
            }

            if (confirmStatusEventArgs.ParamAmp != null)
            {
                if (confirmStatusEventArgs.ParamAmp.Length == 2)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        StateNAVControl.StateModel stateModel = new StateNAVControl.StateModel();
                        switch (confirmStatusEventArgs.ParamAmp[i].Power)
                        {
                            case 0:
                                stateModel.Pow = StateNAVControl.Led.Gray;
                                break;

                            case 1:
                                stateModel.Pow = StateNAVControl.Led.Green;
                                break;

                            default:
                                stateModel.Pow = StateNAVControl.Led.Gray;
                                break;
                        }

                        // synthezator
                        switch (confirmStatusEventArgs.ParamAmp[i].Snt)
                        {
                            case 0:
                                stateModel.Snt = StateNAVControl.Led.Gray;
                                break;

                            case 1:
                                stateModel.Snt = StateNAVControl.Led.Green;
                                break;

                            default:
                                stateModel.Snt = StateNAVControl.Led.Gray;
                                break;
                        }


                        // radiation
                        switch (confirmStatusEventArgs.ParamAmp[i].Rad)
                        {
                            case 0:
                                stateModel.Rad = StateNAVControl.Led.Gray;

                                stateModel.Temperature = 0;
                                stateModel.Amperage = 0;
                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        error = StateNAVControl.Led.Gray;
                                        break;

                                    case 1:
                                        error = StateNAVControl.Led.Red;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            case 1:

                                stateModel.Rad = StateNAVControl.Led.Green;
                                stateModel.Temperature = confirmStatusEventArgs.ParamAmp[i].Temp;
                                stateModel.Amperage = (float)((double)confirmStatusEventArgs.ParamAmp[i].Current / 10.0);

                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        error = StateNAVControl.Led.Gray;
                                        break;

                                    case 1:
                                        error = StateNAVControl.Led.Red;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            default:
                                stateModel.Rad = StateNAVControl.Led.Gray;

                                stateModel.Temperature = 0;
                                stateModel.Amperage = 0;
                                break;
                        }

                        Params.Add(stateModel);
                        errors.Add(error);
                    }

                    if ((ComSHS)sender == comAmp)
                    {
                        _waitAnswerAmp = false;
                        //UpdateStates(StateAmp);
                        spoof.SetBPSSParams(Params, StateNAVControl.Type.Empty, errors, checkedLiters);

                        //if (confirmStatusEventArgs.ParamAmp.Length > 3)
                        //    JammingDrone_UpdateAntennaState(_jamManager1, (AntennaMode)(confirmStatusEventArgs.ParamAmp[3].Antenna + 1));
                    }
                }
            }
        }

        private void ComAmp_OnConfirmStatusAntenna(object sender, ConfirmStatusEventArgs confirmStatusEventArgs)
        {
            List<StateNAVControl.StateModel> Params = new List<StateNAVControl.StateModel>();
            var error = StateNAVControl.Led.Red;
            List<StateNAVControl.Led> errors = new List<StateNAVControl.Led>();

            if (confirmStatusEventArgs.ParamAmp != null)
            {
                if (confirmStatusEventArgs.ParamAmp.Length == 5)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        StateNAVControl.StateModel stateModel = new StateNAVControl.StateModel();
                        switch (confirmStatusEventArgs.ParamAmp[i].Power)
                        {
                            case 0:
                                stateModel.Pow = StateNAVControl.Led.Gray;
                                break;

                            case 1:
                                stateModel.Pow = StateNAVControl.Led.Green;
                                break;

                            default:
                                stateModel.Pow = StateNAVControl.Led.Gray;
                                break;
                        }

                        // synthezator
                        switch (confirmStatusEventArgs.ParamAmp[i].Snt)
                        {
                            case 0:
                                stateModel.Snt = StateNAVControl.Led.Gray;
                                break;

                            case 1:
                                stateModel.Snt = StateNAVControl.Led.Green;
                                break;

                            default:
                                stateModel.Snt = StateNAVControl.Led.Gray;
                                break;
                        }


                        // radiation
                        switch (confirmStatusEventArgs.ParamAmp[i].Rad)
                        {
                            case 0:
                                stateModel.Rad = StateNAVControl.Led.Gray;                                

                                stateModel.Temperature = 0;
                                stateModel.Amperage = 0;
                                break;

                            case 1:

                                stateModel.Rad = StateNAVControl.Led.Green;
                                stateModel.Temperature = confirmStatusEventArgs.ParamAmp[i].Temp;
                                stateModel.Amperage = (float)((double)confirmStatusEventArgs.ParamAmp[i].Current / 10.0);

                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        error = StateNAVControl.Led.Red;
                                        break;

                                    case 1:
                                        error = StateNAVControl.Led.Green;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            default:
                                stateModel.Rad = StateNAVControl.Led.Gray;

                                stateModel.Temperature = 0;
                                stateModel.Amperage = 0;
                                break;
                        }                        

                        Params.Add(stateModel);
                        errors.Add(error);
                    }

                    if ((ComSHS)sender == comAmp)
                    {
                        _waitAnswerAmp = false;
                        
                        spoof.SetBPSSParams(Params, StateNAVControl.Type.Empty, errors, checkedLiters);

                    }
                }
            }
        }
        /*
        private void ComAmp_OnConfirmStatusAntenna(object sender, ConfirmStatusEventArgs confirmStatusEventArgs)
        {
            List<State> StateAmp = new List<State>();
            if (confirmStatusEventArgs.ParamAmp != null)
            {
                if (confirmStatusEventArgs.ParamAmp.Length == 5)
                {

                    for (int i = 0; i < 5; i++)
                    {

                        State stateLetter = new State();

                        // power
                        switch (confirmStatusEventArgs.ParamAmp[i].Power)
                        {
                            case 0:
                                stateLetter.Power = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Power = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Power = StateMode.Unknown;
                                break;
                        }

                        // synthezator
                        switch (confirmStatusEventArgs.ParamAmp[i].Snt)
                        {
                            case 0:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Synthesizer = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;
                        }


                        // radiation
                        switch (confirmStatusEventArgs.ParamAmp[i].Rad)
                        {
                            case 0:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;

                            case 1:

                                stateLetter.Radiation = StateMode.Ok;
                                stateLetter.Temperature = confirmStatusEventArgs.ParamAmp[i].Temp;
                                stateLetter.Current = (float)((double)confirmStatusEventArgs.ParamAmp[i].Current / 10.0);

                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        stateLetter.Error = StateMode.Unknown;
                                        break;

                                    case 1:
                                        stateLetter.Error = StateMode.Ok;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            default:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;
                        }

                        StateAmp.Add(stateLetter);

                    }

                    if ((ComSHS)sender == comAmp)
                    {
                        stateAmp = StateAmp;
                        _waitAnswerAmp = false;
                        UpdateStates(StateAmp);
                        //spoof.SetBPSSParams(Params);
                        //if (confirmStatusEventArgs.ParamAmp.Length > 3)
                        //    JammingDrone_UpdateAntennaState(_jamManager1, (AntennaMode)(confirmStatusEventArgs.ParamAmp[3].Antenna + 1));
                    }
                }

            }
        }
        */

        private void ComAmp_OnConfirmGetParam(object sender, ConfirmGetParamEventArgs[] confirmGetParamEventArgs)
        {
            if ((ComSHS)sender == comAmp)
            {
                ResetFlag();
                //SendGetStatusAmp();
            }
        }

        private void ComAmp_OnConfirmSet(object sender, ConfirmSetEventArgs confirmSetEventArgs)
        {
            if ((ComSHS)sender == comAmp)
            {
                ResetFlag();
                //SendGetStatusAmp();
            }
        }

        private void ComAmp_OnConnect(object sender, bool value)
        {
            if ((ComSHS)sender == comAmp)
            {
                TumanConnection.ShowConnect();
                SendGetStatusAmp();
            }

            InitializeTimerGetStatusAmplifier();
            tmrGetStatusAmplifier.Enabled = true;
        }

        private void ComAmp_OnDisconnect(object sender, bool value)
        {
            if ((ComSHS)sender == comAmp)
                TumanConnection.ShowDisconnect();

            ComAmp_OnConfirmStatus((ComSHS)sender, new ConfirmStatusEventArgs(0, new TParamAmp[2]));
        }

        private void SendGetStatusAmp()
        {
            //_waitAnswerAmp = comAmp.SendStatusAntennaState(0);
            _waitAnswerAmp = comAmp.SendStatus(0);
        }

        private void ResetFlag()
        {
            _waitAnswerAmp = false;
        }

        private void InitializeTimerGetStatusAmplifier()
        {
            tmrGetStatusAmplifier.Elapsed += TickTimerGetStatusAmp;
            tmrGetStatusAmplifier.Interval = 3000;
            tmrGetStatusAmplifier.AutoReset = true;
        }

        private void TickTimerGetStatusAmp(object sender, System.Timers.ElapsedEventArgs e)
        {

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                try
                {
                    //comAmp.SendStatus(0);
                    if (!_waitAnswerAmp)
                    {
                        comAmp.SendStatus(0);
                    }
                }
                catch { }
            });

        }

        private void SendRadiatOffAmp(byte Letter)
        {
            _waitAnswerAmp = comAmp.SendRadiatOff(Letter);
        }

        /*
        private void UpdateStates(List<State> states)
        {
            List<StateNAVControl.StateModel> Params = new List<StateNAVControl.StateModel>()
                {
                    StateModels(states, 3),                    
                };
            spoof.SetBPSSParams(Params);
        }

        private StateNAVControl.StateModel StateModels(List<State> state, int id) 
        {
            StateNAVControl.StateModel stateModel = new StateNAVControl.StateModel();

            switch (state[id].Synthesizer)
            {
                case StateMode.Unknown:
                    stateModel.Snt = StateNAVControl.Led.Gray;
                    break;
                case StateMode.Ok:
                    stateModel.Snt = StateNAVControl.Led.Green;
                    break;
                case StateMode.Error:
                    stateModel.Snt = StateNAVControl.Led.Red;
                    break;
                default:
                    stateModel.Snt = StateNAVControl.Led.Empty;
                    break;
            }

            switch (state[id].Radiation)
            {
                case StateMode.Unknown:
                    stateModel.Rad = StateNAVControl.Led.Gray;
                    break;
                case StateMode.Ok:
                    stateModel.Rad = StateNAVControl.Led.Green;
                    break;
                case StateMode.Error:
                    stateModel.Rad = StateNAVControl.Led.Red;
                    break;
                default:
                    stateModel.Rad = StateNAVControl.Led.Empty;
                    break;
            }

            switch (state[id].Power)
            {
                case StateMode.Unknown:
                    stateModel.Pow = StateNAVControl.Led.Gray;
                    break;
                case StateMode.Ok:
                    stateModel.Pow = StateNAVControl.Led.Green;
                    break;
                case StateMode.Error:
                    stateModel.Pow = StateNAVControl.Led.Red;
                    break;
                default:
                    stateModel.Pow = StateNAVControl.Led.Empty;
                    break;
            }

            return new StateNAVControl.StateModel()
            {
                Temperature = (short)state[id].Temperature,
                Amperage = (float)state[id].Current / 10f,
                Snt = stateModel.Snt,
                Rad = stateModel.Rad,
                Pow = stateModel.Pow,
            };
            
        }
        */
    }
}
