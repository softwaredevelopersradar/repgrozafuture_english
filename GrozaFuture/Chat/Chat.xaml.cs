﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using UserControl_Chat;

namespace GrozaFuture
{
    using System.Linq;

    /// <summary>
    /// Interaction logic for Chat.xaml
    /// </summary>
    public partial class Chat : Window
    {
        public Chat()
        {
            InitializeComponent();
        }

	    public event EventHandler<List<Message>> OnReturnApprovedMessages;
        public List<StationClassForChat> SideMenuList;

        public void SetStations()
        {
            List<StationClassForChat> testList = new List<StationClassForChat>()
            {
                new StationClassForChat(255,"CP", true)
            };
            curChat.InitStations(testList);

            // += DrawMessageToChat;
            Events.OnGetStationsMessage += DrawMessageToChat;
            Events.OnSendStationsMessage += ReturnApprovedMessages;
            Events.OnClosingChat += HideWindow;

        }

        public void UpdateSideMenu(List<TableASP> ASPList)
        {
            try
            {
                SideMenuList = new List<StationClassForChat>();
                ASPList.ForEach(ASPMember =>
                {
                    if(!ASPMember.ISOwn)
                    SideMenuList.Add(new StationClassForChat(ASPMember.Id, ASPMember.CallSign, true));
                });
                SideMenuList.Add(new StationClassForChat(255, "ПУ", true));
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    curChat.UpdateSideMenuMembers(SideMenuList);
                });
            }
            catch (Exception)
            { }
        }

	    public void ConfirmSentMessage(int NumJammer)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                curChat.ConfirmMessage(new Message() { Id = NumJammer });
            });
        }


        private void ReturnApprovedMessages(List<Message> stationsMessages)
        {
            var foWork = new List<Message>();
            foreach (Message curStationsMessage in stationsMessages)
            {
                if (SideMenuList.FirstOrDefault(t => t.Id == curStationsMessage.Id) == null)
                    continue;
                //curStationsMessage.IsTransmited = true;
                curStationsMessage.SenderName = curStationsMessage.SenderName;
                curStationsMessage.MessageFontSize = 20;
                curStationsMessage.SenderNameFontSize = 15;
                foWork.Add(curStationsMessage);
                //curStationsMessage.IsTransmited = false;
            }
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                curChat.DrawMessageToChat(foWork);
            });
		
		    OnReturnApprovedMessages?.Invoke(this, foWork);
        }

        public void DrawMessageToChat(List<Message> stationsMessages)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                curChat.DrawMessageToChat(stationsMessages);
            });
        }

	    public void DrawReceivedMessage(int address, string message)
        {
            List<Message> curMessages = new List<Message>();
            curMessages.Add(new Message
            {
                MessageFiled = message,
                Id = address,
                IsTransmited = true,
                IsSendByMe = Roles.Received

            });

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                curChat.DrawMessageToChat(curMessages);
            });
            
        }



        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
            Events.ClosingChat();
        }

        private void HideWindow()
        {
            this.Visibility = Visibility.Hidden;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

	    public void SetLanguage(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/UIChat/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/UIChat/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;

                   
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
    }
}
