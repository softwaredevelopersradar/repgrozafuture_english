﻿using Newtonsoft.Json.Bson;
using SpoofingControl;
using SpoofingControl.Models;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace GrozaFuture
{
    /// <summary>
    /// Interaction logic for Spoof.xaml
    /// </summary>
    public partial class Spoof : Window
    {
        /*
        #region Properties

        public AntennaModel Antenna => SpoofControl.Antenna;

        public CoordinatesModel SpoofParameters => SpoofControl.SpoofParameters;

        public StateModel1 State1 { get => SpoofControl.State1; set => SpoofControl.State1 = value; }
        public StateModel2 State2 { get => SpoofControl.State2; set => SpoofControl.State2 = value; }

        public ImageModel ImageAngle { get => SpoofControl.ImageAngle; set => SpoofControl.ImageAngle = value; }

        public OrientationEnum Orientation { get => SpoofControl.Orientation; set => SpoofControl.Orientation = value; }

        #endregion
        */

        public Spoof()
        {
            InitializeComponent();

            InitSpoofControl();

            //SpoofControl.CurrentTheme = Theme.Orange;

            //SpoofControl.ChangedCoordinates += (sender, args) => { OnChangeCoord?.Invoke(this, args); };
            //SpoofControl.ChangedAntennaDirect += OnChangedAntennaDirect;
            //SpoofControl.ChangedNavigation += SpoofControl_ChangedNavigation;
        }

        #region Events
        public event EventHandler<StateNavi> OnStateNavigation;

        public event EventHandler<StateSpoof> OnStateSpoof;

        public event EventHandler<StateAnten> OnStateAnten;

        public event EventHandler<WorkingModeModel> OnSelectionWorkingMode;

        public event EventHandler OnGetStatus;

        public event EventHandler<StateCoordinates> OnGetCoordinates;
        #endregion

        #region New_Spoof_Control

        private void InitSpoofControl()
        {
            spoofControl.OnAntenBtnsClick += SpoofControl_OnAntenBtnsClick;
            spoofControl.OnGetValueSpoof += SpoofControl_OnGetValueSpoof;
            spoofControl.OnNaviBtnsClick += SpoofControl_OnNaviBtnsClick;
            spoofControl.OnSelectionWorkingMode += SpoofControl_OnSelectionWorkingMode;
            spoofControl.OnGetStatus += SpoofControl_OnGetStatus;
            spoofControl.OnGetValueSpoofRealCoordinates += SpoofControl_OnGetValueSpoofRealCoordinates;
        }

        private void SpoofControl_OnGetValueSpoofRealCoordinates(StateCoordinates stateCoordinates)
        {
            OnGetCoordinates?.Invoke(this, stateCoordinates);
        }

        private void SpoofControl_OnGetStatus(object sender, EventArgs e)
        {
            OnGetStatus?.Invoke(sender, e);
        }

        private void SpoofControl_OnSelectionWorkingMode(object sender, WorkingModeModel e)
        {
            OnSelectionWorkingMode?.Invoke(this, e);
        }

        private void SpoofControl_OnNaviBtnsClick(StateNavi naviState)
        {
            OnStateNavigation?.Invoke(this, naviState);
        }

        private void SpoofControl_OnGetValueSpoof(StateSpoof spoofState)
        {
            OnStateSpoof?.Invoke(this, spoofState);
        }

        private void SpoofControl_OnAntenBtnsClick(StateAnten antenState)
        {
            OnStateAnten?.Invoke(this, antenState);
        }

        public void UpdateCoodrdinates(double latitude, double longitude)
        {
            spoofControl.UpdateRealCoordinates(latitude, longitude);
        }

        public void UpdateFakeCoordinates(double latitude, double longitude)
        {
            spoofControl.UpdateFakeCoordinates(latitude, longitude);
        }

        public StateSpoof GetSpoofingParameters()
        {
            return new StateSpoof(
                spoofControl.GetStateSpoof().Altitude,
                spoofControl.GetStateSpoof().Direction,
                spoofControl.GetStateSpoof().Speed,
                spoofControl.GetStateSpoof().Range);            
        }

        public StateCoordinates GetStateCoordinates()
        {
            return new StateCoordinates(spoofControl.GetStateCoordinates().Latitude,
                                        spoofControl.GetStateCoordinates().Longitude,
                                        SpoofingControl.Enums.ECoordinates.EMPTY);
        }

        public void SetBPSSParams(List<StateNAVControl.StateModel> state, StateNAVControl.Type type, List<StateNAVControl.Led> lErrors, bool[] liters)
        {
            spoofControl.SetBPSSParams(state, type, lErrors, liters);
        }
        #endregion

        #region Old_Spoof_Control

        //public event EventHandler<CoordinatesModel> OnChangeCoord;
        
        public delegate void SpoofControlOnChangedAntennaDirect(int AntennaDirect);
        public event SpoofControlOnChangedAntennaDirect AntennaDirect;

        private void OnChangedAntennaDirect(object sender, int e)
        {
            AntennaDirect?.Invoke(e);
        }

        //public delegate void SpoofControlChangedNavigation(NavigationModel NavigationModel);
        //public event SpoofControlChangedNavigation NavigationModel;

        //private void SpoofControl_ChangedNavigation(object sender, NavigationModel e)
        //{
        //    NavigationModel?.Invoke(e);
        //}

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        #endregion

        #region Window_Hide
        public delegate void Event(object sender);
        public event Event NeedHide;

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Hide();
                NeedHide?.Invoke(this);
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            NeedHide?.Invoke(this);
        }

        #endregion

        public void SetLanguage(string param)
        {
            bClose1.ToolTip = (param.ToLower().Contains("Rus")) ? "Закрыть" : "Close";
                       
            spoofControl.UpdateLanguage(param);
        }

    }
}
