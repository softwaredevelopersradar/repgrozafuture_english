﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Protocols;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        partial void Dsp_ShaperModeUpdate(ShaperConditionUpdateEvent answer) 
        {
            if (FHSStateWindow.IsVisible)
            {
                Task.Run(() =>
                {
                    FHSStateWindow.UpdateState(answer.Condition);
                    FHSStateWindow.UpdateErrors(answer.Errors, answer.Condition.Length / 2);
                });
            }
                
        }

        partial void dsp_ShaperVoltageUpdate(ShaperVoltageUpdateEvent answer)
        {
            if (FHSStateWindow.IsVisible)
            {
                Task.Run(() =>
                {
                    FHSStateWindow.UpdateVoltage(answer.Voltage, answer.LiterCount);
                    FHSStateWindow.UpdateErrors(answer.Errors, answer.LiterCount);
                });
            }
        }

        partial void dsp_ShaperPowerUpdate(ShaperPowerUpdateEvent answer)
        {
            if (FHSStateWindow.IsVisible)
            {
                Task.Run(() =>
                {
                    FHSStateWindow.UpdatePower(answer.Power, answer.LiterCount);
                    FHSStateWindow.UpdateErrors(answer.Errors, answer.LiterCount);
                    FillLettersForRoss(answer.LiterCount, answer.Power, answer.Errors);
                });
            }
           
        }

        partial void dsp_ShaperTemperatureUpdate(ShaperConditionUpdateEvent answer)
        {
            if (FHSStateWindow.IsVisible)
            {
                Task.Run(() =>
                {
                    FHSStateWindow.UpdateTemperature(answer.Condition, answer.Condition.Length / 5);
                    FHSStateWindow.UpdateErrors(answer.Errors, answer.Condition.Length / 5);
                });
            }
            
        }

        partial void dsp_ShaperAmperageUpdate(ShaperConditionUpdateEvent answer)
        {
            if (FHSStateWindow.IsVisible)
            {
                Task.Run(() =>
                {
                    FHSStateWindow.UpdateCurrent(answer.Condition, answer.Condition.Length / 5);
                    FHSStateWindow.UpdateErrors(answer.Errors, answer.Condition.Length / 5);
                });
            }
            
        }
    }
}
