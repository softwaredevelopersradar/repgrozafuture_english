﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace GrozaFuture
{
    public class VisibleTBConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((byte)value)
            {
                case 1:
                    return Visibility.Collapsed;
                case 2:
                    return Visibility.Visible;
                default:
                    return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Visibility)value == Visibility.Visible) return (byte)2;
            return (byte)1;
        }
    }
}
