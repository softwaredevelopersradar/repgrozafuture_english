﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        
        public void InitVoiceDisturb()
        {
            VoiceDisturb.OnTakeFileCheck += new UserControlVolume.UserControlVolInt.MyControlEventHandler(TakeFileCheck);
            VoiceDisturb.OnTakeOnlineCheck += new UserControlVolume.UserControlVolInt.MyControlEventHandler(TakeOnlineCheck);
            VoiceDisturb.OnButPlayMouseDown += new UserControlVolume.UserControlVolInt.MyControlEventHandler(ButPlayMouseDown);
            VoiceDisturb.OnButPlayMouseUp += new UserControlVolume.UserControlVolInt.MyControlEventHandler(ButPlayMouseUp);
            VoiceDisturb.OnButOpenFile += new UserControlVolume.UserControlVolInt.MyControlEventHandler(ButOpenFile);
            VoiceDisturb.OnTextInput += new UserControlVolume.UserControlVolInt.TextChangedEventHandler(Text_Input);
            VoiceDisturb.OnComboboxMode += new UserControlVolume.UserControlVolInt.SelectionChangedEventHandler(ComboboxDeviation);
        }

        private void TakeFileCheck()
        {
            VoiceDisturb.TakeFileIndex = 0;
        }

        private void TakeOnlineCheck()
        {
            VoiceDisturb.TakeFileIndex = 1;
        }

        private void ButPlayMouseDown()
        {
            DspVoiceEmit(true);
        }

        private void ButPlayMouseUp()
        {
            DspVoiceEmit(false);
        }

        private void ButOpenFile()
        {

        }

        private void Text_Input(object sender, TextCompositionEventArgs e)
        {

        }

        private void ComboboxDeviation()
        {
            Int16 i = VoiceDisturb.Deviation;
        }
    }
}
