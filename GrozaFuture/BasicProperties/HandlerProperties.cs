﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using YamlDotNet.Serialization;

namespace GrozaFuture
{

    public partial class MainWindow
    {
        public void InitLocalProperties()
        {
            var localProperties = YamlLoad();

            try
            {
                basicProperties.Local = localProperties;
                //basicProperties.Local.Update(localProperties);
                UpdateLocalProperties4MainPanel(localProperties);
                endPoint = localProperties.DbServer.IpAddress + ":" + localProperties.DbServer.Port;
                ComPort = localProperties.ARONE1.ComPort;
                BaudRate = localProperties.ARONE1.PortSpeed;
                CurrentDataBank = localProperties.ARONE1.CurrentDataBank;
                CurrentFileType = basicProperties.Local.Common.FileType.ToString();

                ComPortSec = localProperties.ARONE2.ComPort;
                BaudRateSec = localProperties.ARONE2.PortSpeed;
                CurrentDataBankSec = localProperties.ARONE2.CurrentDataBank;

                SetControlsLanguage();
                ChangeLangugeReceiverControls();
                ChangeLangVD();
                AudioPlayer.AudioPlayerControl.Language(basicProperties.Local.Common.Language.ToString());

                basicProperties.DefaultEvent += BasicProperties_DefaultEvent;

                ChangeSpoofingVisibilityToGlobalProperties(localProperties);

                UpdatePanoramaManager(localProperties);
                UpdateButtonsCRRD(localProperties);

                UpdateFrequencyUnits(localProperties);
            }
            catch { }
        }

        private void BasicProperties_DefaultEvent(object sender, ControlProperties.BasicProperties.PropertiesType propertiesType)
        {
            switch (propertiesType)
            {
                case ControlProperties.BasicProperties.PropertiesType.Global:

                    var defaultGlobalProperties = YamlLoad<GlobalProperties>("DefaultGlobalProperties.yaml");

                    if (defaultGlobalProperties == null)
                    {
                        var defaultRangeRadioRecon = EnumRangeRadioRecon.Range_30_3000;
                        var defaultRangeJamming = EnumRangeJamming.Range_30_3000;

                        if (basicProperties.Local.Common.IsVisibleEN == true)
                        {
                            defaultRangeRadioRecon = EnumRangeRadioRecon.Range_30_6000;
                            defaultRangeJamming = EnumRangeJamming.Range_30_6000;
                        }

                        if (basicProperties.Local.Common.IsVisibleAZ == true)
                        {
                            defaultRangeRadioRecon = EnumRangeRadioRecon.Range_30_6000;
                            defaultRangeJamming = EnumRangeJamming.Range_30_3000;
                        }

                        defaultGlobalProperties = new GlobalProperties()
                        {

                            RangeRadioRecon = defaultRangeRadioRecon,
                            RangeJamming = defaultRangeJamming,

                            DetectionFHSS = 0,
                            TypeRadioRecon = EnumTypeRadioRecon.WithoutBearing,
                            TypeRadioSuppr = EnumTypeRadioSuppr.FWS,

                            PingIntervalFHSAP = 5000,

                            NumberAveragingPhase = 3,
                            NumberAveragingBearing = 3,
                            HeadingAngle = 0,
                            SignHeadingAngle = true,

                            NumberChannels = 4,
                            NumberIri = 10,
                            OperationTime = 15,
                            Priority = 1,
                            Threshold = -80,
                            TimeRadiateFWS = 5000,

                            SectorSearch = 10,
                            TimeSearch = 300,

                            FFTResolution = 1,
                            TimeRadiateFHSS = 900,
                        };

                        YamlSave<GlobalProperties>(defaultGlobalProperties, "DefaultGlobalProperties.yaml");
                    }

                    basicProperties.Global = defaultGlobalProperties;
                    break;

                case ControlProperties.BasicProperties.PropertiesType.Local:

                    var defaultLocalProperties = YamlLoad<LocalProperties>("DefaultLocalProperties.yaml");

                    if (defaultLocalProperties == null)
                    {
                        defaultLocalProperties = new LocalProperties();
                        {
                            defaultLocalProperties.ADSB.State = true;
                            defaultLocalProperties.ADSB.IpAddress = "192.168.0.11";
                            defaultLocalProperties.ADSB.Port = 30005;

                            defaultLocalProperties.ARD1.State = true;
                            defaultLocalProperties.ARD1.Address = 1;
                            defaultLocalProperties.ARD1.ComPort = "COM10";
                            defaultLocalProperties.ARD1.PortSpeed = 9600;

                            defaultLocalProperties.ARD2.State = true;
                            defaultLocalProperties.ARD2.Address = 1;
                            defaultLocalProperties.ARD2.ComPort = "COM9";
                            defaultLocalProperties.ARD2.PortSpeed = 9600;

                            defaultLocalProperties.ARD3.State = true;
                            defaultLocalProperties.ARD3.Address = 1;
                            defaultLocalProperties.ARD3.ComPort = "COM7";
                            defaultLocalProperties.ARD3.PortSpeed = 9600;

                            defaultLocalProperties.ARONE1.State = true;
                            defaultLocalProperties.ARONE1.ComPort = "COM5";
                            defaultLocalProperties.ARONE1.PortSpeed = 19200;

                            defaultLocalProperties.CmpPA.State = true;
                            defaultLocalProperties.CmpPA.ComPort = "COM12";
                            defaultLocalProperties.CmpPA.PortSpeed = 9600;

                            defaultLocalProperties.CmpRR.State = true;
                            defaultLocalProperties.CmpRR.ComPort = "COM8";
                            defaultLocalProperties.CmpRR.PortSpeed = 9600;
                            defaultLocalProperties.CmpRR.CompassСorrection = 90;

                            defaultLocalProperties.DbServer.IpAddress = "192.168.0.102";
                            defaultLocalProperties.DbServer.Port = 8302;

                            defaultLocalProperties.DspServer.IpAddress = "192.168.0.102";
                            defaultLocalProperties.DspServer.Port = 10001;

                            defaultLocalProperties.EdServer.IpAddress = "192.168.0.102";
                            defaultLocalProperties.EdServer.Port = 10009;
                            defaultLocalProperties.EdServer.State = true;

                            defaultLocalProperties.Tuman.State = false;
                            defaultLocalProperties.Tuman.ComPort = "COM11";
                            defaultLocalProperties.Tuman.PortSpeed = 9600;

                            defaultLocalProperties.PCProperties.PortPPC = 9101;
                            defaultLocalProperties.PCProperties.PortRouter = 9101;
                            defaultLocalProperties.Spoofing.Port = 9114;
                        }

                        YamlSave<LocalProperties>(defaultLocalProperties, "DefaultLocalProperties.yaml");
                    }

                    basicProperties.Local = defaultLocalProperties;

                    break;

            }

            basicProperties.PerformApply();
        }

        public async void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            UpdateLocalProperties4MainPanel(arg);

            endPoint = arg.DbServer.IpAddress + ":" + arg.DbServer.Port;
            ComPort = arg.ARONE1.ComPort;
            BaudRate = arg.ARONE1.PortSpeed;
            CurrentDataBank = arg.ARONE1.CurrentDataBank;

            ComPortSec = arg.ARONE2.ComPort;
            BaudRateSec = arg.ARONE2.PortSpeed;
            CurrentDataBankSec = arg.ARONE2.CurrentDataBank;
            YamlSave(arg);

            ChangeSpoofingVisibilityToGlobalProperties(arg);

            await UpdateCmpRR(basicProperties.Global);

            //ReInitAPD(arg);
            ReInitPostControl(arg);

            UpdatePanoramaManager(arg);
            UpdateButtonsCRRD(arg);

            UpdateFrequencyUnits(arg);

            PropViewCoords.ViewCoords = ViewCoordToByte(arg.CoordinatesProperty.View);
        }

        private void ChangeSpoofingVisibilityToGlobalProperties(LocalProperties arg)
        {
            basicProperties.Global.SpoofingVisibility = arg.Spoofing.State;

            if (arg.Common.AccessARM == TypeAccessARM.Admin)
            {
                //spoof.SpoofControl.View = SpoofingControl.View.Pro;
            }
            else
            {
                //spoof.SpoofControl.View = SpoofingControl.View.Normal;
            }
        }

        public async void HandlerGlobalProperties(object sender, GlobalProperties arg)
        {
            await UpdateCmpRR(arg);
            UpdatePanoramaManager(arg);
            UpdateGlobalProperties4MainPanel(arg);
            UpdateRanges(arg);
        }

        private async Task UpdateCmpRR(GlobalProperties arg)
        {
            if (arg.SignHeadingAngle)
            {
                if (clientDB != null && clientDB.Tables != null)
                {
                    TempGNSS tempGNSS = (await clientDB?.Tables[NameTable.TempGNSS].LoadAsync<TempGNSS>()).FirstOrDefault();

                    if (tempGNSS != null)
                    {
                        int headingAngle = (int)Math.Round(tempGNSS.CmpRR) + basicProperties.Local.CmpRR.CompassСorrection;

                        if (headingAngle >= 360)
                        {
                            headingAngle -= 360;
                        }

                        arg.HeadingAngle = headingAngle;
                    }
                }
            }
            if (clientDB != null && clientDB.Tables != null)
            {
                await clientDB?.Tables[NameTable.GlobalProperties].AddAsync(arg);
            }
        }
    }
}