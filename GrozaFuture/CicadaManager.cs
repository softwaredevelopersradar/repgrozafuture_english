﻿using CicadaWebSocketClientProject;
using ModelsTablesDBLib;
using System;
using System.Linq;
using System.Windows;
using ValuesCorrectLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        CicadaWebSocketClient cicadaWebSocketClient = new CicadaWebSocketClient();

        private async void CicadaConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!cicadaWebSocketClient.IsConnected)
                {
                    cicadaWebSocketClient.OnConnected += CicadaWebSocketClient_OnConnected;
                    await cicadaWebSocketClient.IQConnectToWebServer(basicProperties.Local.Cicada.IpAddress, basicProperties.Local.Cicada.Port);
                }
                else
                {
                    cicadaWebSocketClient.DisconnectFromWebServer();
                }
            }
            catch { }
        }

        private void CicadaWebSocketClient_OnConnected(bool onConnected)
        {
            try
            {
                if (onConnected)
                {
                    cicadaWebSocketClient.PelengEvent += CicadaWebSocketClient_PelengEvent;
                    cicadaWebSocketClient.ZvukEvent += CicadaWebSocketClient_ZvukEvent;
                    cicadaWebSocketClient.MestoEvent += CicadaWebSocketClient_MestoEvent;

                    CicadaControlConnection.ShowConnect();
                }
                else
                {
                    cicadaWebSocketClient.OnConnected -= CicadaWebSocketClient_OnConnected;

                    cicadaWebSocketClient.PelengEvent -= CicadaWebSocketClient_PelengEvent;
                    cicadaWebSocketClient.ZvukEvent -= CicadaWebSocketClient_ZvukEvent;
                    cicadaWebSocketClient.MestoEvent -= CicadaWebSocketClient_MestoEvent;

                    CicadaControlConnection.ShowDisconnect();
                }
            }
            catch { }
            
        }

        private void CicadaWebSocketClient_PelengEvent(Peleng.Root root)
        {
            double FreqKHz = 0;
            float Deviation = 0;

            try
            {
                Coord lastCoordinates = new Coord
                {
                    Latitude = -1,
                    Longitude = -1,
                    Altitude = -1
                };

                // координаты поста
                //Coord lastCoordinates = new Coord
                //{
                //    Latitude = root.data[root.data.Count() - 1].nav.latitude,
                //    Longitude = root.data[root.data.Count() - 1].nav.longitude,
                //    Altitude = -1
                //};

                float Bearing = -1;
                short lastLevel = -1;
                float Std = 0;


                if (root != null)
                {
                    if (root.ws != null)
                    {
                        FreqKHz = root.ws.frequency / 1000d;

                        Deviation = root.ws.band / 1000f;

                        if (root.ws.jsondata != null)
                        {
                            Bearing = (float)root.ws.jsondata.MeanBearing;
                            Std = (float)root.ws.jsondata.BearingSKO;
                        }
                    }

                    if (root.data != null && root.data.Count > 0)
                    {
                        if (root.data[root.data.Count() - 1].nav != null)
                        {
                            //lastCoordinates = new Coord
                            //{
                            //    Latitude = root.data[root.data.Count() - 1].nav.latitude,
                            //    Longitude = root.data[root.data.Count() - 1].nav.longitude,
                            //    Altitude = -1
                            //};

                            lastLevel = (short)(root.data[root.data.Count() - 1].level * -1);
                        }
                    }
                }

                TableReconFWS tableReconFWS = new TableReconFWS
                {
                    Id = 0,
                    Sender = SignSender.Cicada,
                    FreqKHz = FreqKHz,
                    Time = DateTime.Now.ToLocalTime(),
                    Deviation = Deviation,
                    Coordinates = lastCoordinates,
                    ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                                {
                                    new TableJamDirect
                                    {
                                        JamDirect = new JamDirect
                                        {
                                            NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                            Bearing = Bearing,
                                            Level = lastLevel,
                                            Std = Std,
                                            DistanceKM = -1,
                                            IsOwn = true
                                        }
                                    }
                                }
                };

                UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
            }
            catch { }
           
        }

        private void AddRecToCicadaFWS_TD(Zvuk.Root root)
        {
            double FreqKHz = 0;
            float Deviation = 0;

            try
            {
                Coord lastCoordinates = new Coord
                {
                    Latitude = -1,
                    Longitude = -1,
                    Altitude = -1
                };

                if (root != null)
                {
                    if (root.ws != null)
                    {
                        FreqKHz = root.ws.frequency / 1000d;

                        Deviation = root.ws.band / 1000f;
                    }
                }

                TableReconFWS tableReconFWS = new TableReconFWS
                {
                    Id = 0,
                    Sender = SignSender.Cicada,
                    FreqKHz = FreqKHz,
                    Time = DateTime.Now.ToLocalTime(),
                    Deviation = Deviation,
                    Coordinates = lastCoordinates,
                    Modulation = ModulationKondorStrToEnum(root.dataInfo.demodulation),
                    ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                    {
                        new TableJamDirect
                        {
                            JamDirect = new JamDirect
                            {
                                NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                Bearing = -1,
                                Level = -1,
                                Std = -1,
                                DistanceKM = -1,
                                IsOwn = true
                            }
                        }
                    }
                };

                UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
            }
            catch {}
        }

        private void CicadaWebSocketClient_ZvukEvent(Zvuk.Root root)
        {
            double FreqMHz = 0;
            float Deviation = 0;

            try
            {
                if (root != null)
                {
                    if (root.ws != null)
                    {
                        FreqMHz = root.ws.frequency / 1e6d;
                        Deviation = root.ws.band / 1000f;
                    }
                }

                //CRRX1
                var type = basicProperties.Local.ARONE1.Type.ToString();

                switch (type)
                {
                    case "AR_6000":
                        ControlAR6000First.FrequencyFromPanorama(FreqMHz);
                        break;
                    case "AR_ONE":
                        AroneConnetion.FrequencyFromPanorama(FreqMHz);
                        break;
                    default:
                        break;
                }

                AddRecToCicadaFWS_TD(root);
            }
            catch { }
           
        }

        private void CicadaWebSocketClient_MestoEvent(Mesto.Root root)
        {
            try
            {
                double FreqKHz = root.ws.frequency / 1000d;
                float Deviation = 0;

                Coord lastCoordinates = new Coord
                {
                    Latitude = -1,
                    Longitude = -1,
                    Altitude = -1
                };

                float Bearing = -1;
                short lastLevel = -1;
                float Std = 0;

                if (root != null)
                {
                    if (root.data != null && root.data.Count > 0)
                    {
                        if (root.data[root.data.Count() - 1] != null)
                        {
                            lastCoordinates = new Coord
                            {
                                Latitude = root.data[root.data.Count() - 1].latitude,
                                Longitude = root.data[root.data.Count() - 1].longitude,
                                Altitude = -1
                            };
                        }
                    }
                }

                if (lastCoordinates.Latitude != -1 && lastCoordinates.Longitude != -1)
                {

                    TableReconFWS tableReconFWS = new TableReconFWS
                    {
                        Id = 0,
                        Sender = SignSender.Cicada,
                        FreqKHz = FreqKHz,
                        Time = DateTime.Now.ToLocalTime(),
                        Deviation = Deviation,
                        Coordinates = lastCoordinates,
                        ListJamDirect = new System.Collections.ObjectModel.ObservableCollection<TableJamDirect>
                                {
                                    new TableJamDirect
                                    {
                                        JamDirect = new JamDirect
                                        {
                                            NumberASP = TableEvents.PropNumberASP.SelectedNumASP,
                                            Bearing = Bearing,
                                            Level = lastLevel,
                                            Std = Std,
                                            DistanceKM = -1,
                                            IsOwn = true
                                        }
                                    }
                                }
                    };

                    UcTemsFWS_OnAddFWS_TD(this, tableReconFWS);
                }
            }
            catch { }
           
        }
    }
}
