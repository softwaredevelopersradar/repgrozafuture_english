﻿using DLL_ADSB_Library;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TempADSBLibrary;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        ADSB_Decoder ADSBDecoder;
        bool connected;

        TimerCallback tm;
        Timer timer;

        Dictionary<string, DateTime> planeLifeTimeCollection;
        DateTime curDateStep;


        private void ADSBConnection_Click(object sender, RoutedEventArgs e)
        {
            TempEvents.OnIsConnected += ChangeConnectionStatus;
            try
            {
                if (connected != false)
                {
                    //ADSBDecoder.Disconnect();
                    DataDecoder.CloseConnection();
                    connected = false;
                    tm = null;
                    timer = null;
                }
                else
                {
                    //Task.Run(() => ADSBDecoder.ConnectToADSB());
                    //Events.OnReceivePlaneData += AddPlaneToTable;
                    //Events.OnReceiveShortPlaneData += AddPlaneWithShortDataToTable;
                    //var localProperties = YamlLoad<LocalProperties>("LocalProperties.yaml");
                    //DataDecoder.ConnectADSB(localProperties.ADSB.IpAddress, localProperties.ADSB.Port);
                    Ping ping = new Ping();
                    ping.Send("192.168.0.11");
                    DataDecoder.ConnectADSB(basicProperties.Local.ADSB.IpAddress, basicProperties.Local.ADSB.Port);
                    TempEvents.OnReceivePlaneData += AddPlaneToTable;
                    TempEvents.OnReceiveShortPlaneData += AddPlaneWithShortDataToTable;
                    planeLifeTimeCollection = new Dictionary<string, DateTime>();
                    connected = true;
                    tm = new TimerCallback(TestRelevanceOfPlanes);
                    timer = new Timer(tm, null, 60000, 60000);
                    curDateStep = DateTime.Now;
                }
            }
            catch (Exception exp)
            {
                ADSBControlConnection.ShowDisconnect();
                MessageBox.Show(exp.Message);
            }

        }



        private void ChangeConnectionStatus(bool connected)
        {
            Dispatcher.Invoke(() =>
            {
                if (connected)
                    ADSBControlConnection.ShowConnect();
                else
                    ADSBControlConnection.ShowDisconnect();
            });
        }

        private bool ValidateADSBProperties(LocalProperties localProperties)
        {
            if (localProperties.ADSB.IpAddress.Equals("192.168.0.11") && localProperties.ADSB.Port == 30005)
                return true;
            else
                return false;
        }



        private void AddPlaneWithShortDataToTable(string iCAO, double altitude)
        {
            try
            {
                if (clientDB != null)
                {
                    AddPlaneToBD(iCAO, altitude);

                    if (!planeLifeTimeCollection.Keys.Contains(iCAO))
                        planeLifeTimeCollection.Add(iCAO, DateTime.Now);
                    else
                        planeLifeTimeCollection[iCAO] = DateTime.Now;

                }
            }
            catch
            { }

        }

        private async void AddPlaneToBD(string iCAO, double altitude)
        {
            await clientDB?.Tables[NameTable.TempADSB].AddAsync(new TempADSB()
            {
                Id = iCAO.GetHashCode(),
                ICAO = iCAO,
                Coordinates = new Coord()
                {
                    Altitude = altitude,
                },
                Time = DateTime.Now.ToString()
            });
        }

        private async void AddPlaneToTable(string iCAO, double latitude, double longitude, double altitude)
        {
            try
            {
                if (clientDB != null)
                {
                    if (!CheckCoordinates(latitude, longitude))
                        return;

                    if (clientDB.IsConnected())
                    {
                        await clientDB?.Tables[NameTable.TempADSB].AddAsync(new TempADSB()
                        {
                            ICAO = iCAO,
                            Id = iCAO.GetHashCode(),
                            Coordinates = new Coord()
                            {
                                Latitude = latitude,
                                Longitude = longitude,
                                Altitude = altitude
                            },
                            Time = DateTime.Now.ToString()
                        });
                    }
                    if (!planeLifeTimeCollection.Keys.Contains(iCAO))
                        planeLifeTimeCollection.Add(iCAO, DateTime.Now);
                    else
                        planeLifeTimeCollection[iCAO] = DateTime.Now;
                }
            }
            catch
            { }
        }


        private bool CheckCoordinates(double latitude, double longitude)
        {
            if (latitude > 90 || latitude < 0)
                return false;
            if (longitude < 0 || longitude > 180)
                return false;
            return true;
        }

        protected async void TestRelevanceOfPlanes(object obj)
        {
            try
            {
                curDateStep = DateTime.Now;
                List<TempADSB> listTableADSB = new List<TempADSB>(await clientDB.Tables[NameTable.TempADSB].LoadAsync<TempADSB>());

                //var objAirPl = ;
                //listTableADSB = objAirPl;

                List<string> ICAOlist = new List<string>(planeLifeTimeCollection.Keys);
                for (int i = 0; i < ICAOlist.Count; i++)
                {
                    if (!listTableADSB.Any(item => item.ICAO == ICAOlist[i]))
                    {
                        planeLifeTimeCollection.Remove(ICAOlist[i]);
                    }
                    else
                    {
                        if (curDateStep - planeLifeTimeCollection[ICAOlist[i]] >= TimeSpan.FromMinutes(3))
                        {

                            await clientDB?.Tables[NameTable.TempADSB].DeleteAsync(new TempADSB()
                            {
                                Id = ICAOlist[i].GetHashCode(),
                                ICAO = ICAOlist[i],
                            });

                            planeLifeTimeCollection.Remove(ICAOlist[i]);
                        }
                    }
                    
                }

            }
            catch (Exception e)
            {  }

        }


    }
}
