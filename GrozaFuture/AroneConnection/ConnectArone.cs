﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace GrozaFuture
{
    public partial class MainWindow
    {       
        public int IzOpen { get; set; }

        private int IzOpenToInt(bool open) 
        {
            var intOpen = 0;
            if (open) { intOpen = 1; }
            else { intOpen = 0; }
            return intOpen;
        }
        public int BaudRate
        {
            get => basicProperties.Local.ARONE1.PortSpeed;
            set { AroneConnetion.BaudRate = value; }
        }
        public string ComPort
        {
            get => basicProperties.Local.ARONE1.ComPort;
            set { AroneConnetion.PortAdress = value; }

        }
        
        private void ARONEconnection_Click(object sender, RoutedEventArgs e)
        {
            var typeUp = basicProperties.Local.ARONE1.Type.ToString();

            switch (typeUp)
            {
                case "AR_6000":
                    ControlAR6000First.PortAdress = ComPort;
                    ControlAR6000First.BaudRate = BaudRate;

                    ControlAR6000First.ConnectionToMain();
                    ControlAR6000First.OnComIsOpen += ControlAR6000First_OnComIsOpen;
                    ControlAR6000First.ComIsOpen();
                    if (IzOpen == 1)
                    {
                        ARONEconnection.ShowConnect();
                    }
                    else { ARONEconnection.ShowDisconnect(); }
                    break;
                case "AR_ONE":
                    AroneConnetion.IdArOne = 0;
                    ReceiverTable.DllArOne = 0;
                    AroneConnetion.PortAdress = ComPort;
                    AroneConnetion.BaudRate = BaudRate;
                    ReceiverTable.DeviceNumber = 0;
                    //InitReceiversDataExchange();
                    AroneConnetion.ConnectionToMain();
                    AroneConnetion.OnComIsOpen += AroneConnetion_OnComIsOpen;
                    AroneConnetion.ComIsOpen();
                    if (IzOpen == 1)
                    {
                        ARONEconnection.ShowConnect();
                    }
                    else { ARONEconnection.ShowDisconnect(); }
                    break;
                default:
                    break;
            }
        }

        private void AroneConnetion_OnComIsOpen(object sender, WpfControlLibraryBut.ComIsOpenEventArgs e)
        {
            IzOpen = e.IsOpen;
        }

        private void ControlAR6000First_OnComIsOpen(object sender, WpfControlLibraryAR6000.ComIsOpenEventArgs e)
        {
            IzOpen = IzOpenToInt(e.IsOpen);
        }
    }
}
