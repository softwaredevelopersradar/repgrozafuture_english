﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void SetLanguageTab(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.Eng:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslationRightControls/TranslatorRightTab.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslationRightControls/TranslatorRightTab.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.Azr:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslationRightControls/TranslatorRightTab.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslationRightControls/TranslatorRightTab.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                              where d.Source != null && d.Source.OriginalString.StartsWith("/GrozaFuture;component/Languages/TranslationRightControls/TranslatorRightTab.")
                                              select d).First();

                if (oldDict != null)
                {
                    int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                    Resources.MergedDictionaries.Remove(oldDict);
                    Resources.MergedDictionaries.Insert(ind, dict);

                }
                else
                {
                    Resources.MergedDictionaries.Add(dict);
                }                
            }
            catch (Exception ex)
            { }
        }
             
    }
}
