﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        private bool IsVisibleArone { get; set; }

        public bool IzOpenSec { get; set; }

        private bool IzOpenToBool(int open) 
        {
            var boolOpen = false;
            if (open == 1) { boolOpen = true; }
            else { boolOpen = false; }
            return boolOpen;
        }
        public int BaudRateSec
        {
            get => basicProperties.Local.ARONE2.PortSpeed;
            set { ControlAR6000Sec.BaudRate = value; }
        }
        public string ComPortSec
        {
            get => basicProperties.Local.ARONE2.ComPort;
            set { ControlAR6000Sec.PortAdress = value; }

        }

        private void ARONE2connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            var typeUp = basicProperties.Local.ARONE2.Type.ToString();

            switch (typeUp)
            {
                case "AR_6000":
                    ControlAR6000Sec.PortAdress = ComPortSec;
                    ControlAR6000Sec.BaudRate = BaudRateSec;

                    ControlAR6000Sec.ConnectionToMain();
                    ControlAR6000Sec.OnComIsOpen += ControlAR6000_OnComIsOpen;
                    ControlAR6000Sec.ComIsOpen();
                    if (IzOpenSec)
                    {
                        ARONE2connection.ShowConnect();
                    }
                    else { ARONE2connection.ShowDisconnect(); }
                    break;
                case "AR_ONE":
                    ControlArOneSec.IdArOne = 1;
                    ReceiverTableArOneSec.DllArOne = 1;
                    ControlArOneSec.PortAdress = ComPortSec;
                    ControlArOneSec.BaudRate = BaudRateSec;

                    ReceiverTableArOneSec.DeviceNumber = 2;

                    ControlArOneSec.ConnectionToMain();
                    ControlArOneSec.OnComIsOpen += ControlArOneSec_OnComIsOpen;
                    ControlArOneSec.ComIsOpen();
                    if (IzOpenSec)
                    {
                        ARONE2connection.ShowConnect();
                    }
                    else { ARONE2connection.ShowDisconnect(); }
                    break;
                default:
                    break;
            }
        }

        private void ControlAR6000_OnComIsOpen(object sender, WpfControlLibraryAR6000.ComIsOpenEventArgs e)
        {
            IzOpenSec = e.IsOpen;
        }

        private void ControlArOneSec_OnComIsOpen(object sender, WpfControlLibraryBut.ComIsOpenEventArgs e)
        {
            IzOpenSec = IzOpenToBool(e.IsOpen);
        }
    }
}
