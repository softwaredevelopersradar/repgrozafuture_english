﻿using ModelsTablesDBLib;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private byte ShiftTypeRadioRecon = 1;
        private byte ShiftTypeRadioSuppr = 3;

        private void InitDefaultRIButtons()
        {
        }

        private int DesiredRadioReconMode()
        {
            return (byte)basicProperties.Global.TypeRadioRecon + ShiftTypeRadioRecon;
        }

        private int DesiredRadioReconMode2()
        {
            return (BearingToggleButton.IsChecked.Value) ? (byte)2 : (byte)1;
        }

        private int DesiredRadioSupprMode()
        {
            if (basicProperties.Global.TypeRadioSuppr == ModelsTablesDBLib.EnumTypeRadioSuppr.Voice) return 5;
            return (byte)basicProperties.Global.TypeRadioSuppr + ShiftTypeRadioSuppr;
        }

        private byte DesireDetectionFHSS()
        {
            byte desireDetectionFHSS = Convert.ToByte(basicProperties.Global.DetectionFHSS);
            return (byte)((desireDetectionFHSS > 0) ? 1 : 0);
        }

        private async void BearingToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (pLibrary.Mode == 0 || pLibrary.Mode == 1 || pLibrary.Mode == 2)
            {
                if (pLibrary.Mode == 1 || pLibrary.Mode == 2)
                {
                    var answer = await dsp.SetMode(DesiredRadioReconMode2());
                    if (answer?.Header.ErrorCode == 0)
                    {
                        pLibrary.Mode = DesiredRadioReconMode2();
                        AttCheck(DesiredRadioReconMode2());
                    }
                }
                if (BearingToggleButton.IsChecked.Value)
                {
                    //SearchFHSSToggleButton.IsEnabled = true;
                    basicProperties.Global.TypeRadioRecon = ModelsTablesDBLib.EnumTypeRadioRecon.Bearing;

                    clientDB?.Tables[NameTable.GlobalProperties].Add(basicProperties.Global);
                }
                else
                {
                    if (SearchFHSSToggleButton.IsChecked.Value)
                    {
                        var answerSetSearchFHSS = await dsp.SetSearchFHSS(0);
                    }

                    SearchFHSSToggleButton.IsChecked = false;

                    basicProperties.Global.TypeRadioRecon = ModelsTablesDBLib.EnumTypeRadioRecon.WithoutBearing;
                    basicProperties.Global.DetectionFHSS = 0;

                    clientDB?.Tables[NameTable.GlobalProperties].Add(basicProperties.Global);


                    //await Task.Delay(5);
                    //SearchFHSSToggleButton.IsChecked = false;
                    //SearchFHSSToggleButton.IsEnabled = false;
                }
            }
            else
            {
                BearingToggleButton.IsChecked = !BearingToggleButton.IsChecked;
            }
        }

        private async void ManageRIButtons(int mode)
        {
            if (mode == 1)
            {
                if (SearchFHSSToggleButton.IsChecked.Value)
                {
                    var answerSetSearchFHSS = await dsp.SetSearchFHSS(0);
                }
                SearchFHSSToggleButton.IsChecked = false;
                //SearchFHSSToggleButton.IsEnabled = false;

                BearingToggleButton.IsChecked = false;
            }
            if (mode == 2)
            {
                if (SearchFHSSToggleButton.IsEnabled == false)
                    SearchFHSSToggleButton.IsEnabled = true;

                BearingToggleButton.IsChecked = true;
            }
        }

        private async void SearchFHSSToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (pLibrary.Mode == 0 || pLibrary.Mode == 1 || pLibrary.Mode == 2)
            {
                if (BearingToggleButton.IsChecked.Value)
                {
                    //byte desireDetectionFHSS = DesireDetectionFHSS();
                    byte desireDetectionFHSS = (SearchFHSSToggleButton.IsChecked.Value) ? (byte)1 : (byte)0;
                    var answer = await dsp.SetSearchFHSS(desireDetectionFHSS);
                    basicProperties.Global.DetectionFHSS = desireDetectionFHSS;
                    clientDB?.Tables[NameTable.GlobalProperties].Add(basicProperties.Global);
                }
                else
                {
                    SearchFHSSToggleButton.IsChecked = !SearchFHSSToggleButton.IsChecked;
                }
            }
            else
            {
                SearchFHSSToggleButton.IsChecked = !SearchFHSSToggleButton.IsChecked;
            }
        }

        public void UpdateGlobalProperties4LeftRIButtons(GlobalProperties arg)
        {
            BearingToggleButton.IsChecked = Convert.ToBoolean(arg.TypeRadioRecon);
            SearchFHSSToggleButton.IsChecked = Convert.ToBoolean(arg.DetectionFHSS);
        }

    }
}
