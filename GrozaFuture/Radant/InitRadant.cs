﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using CompassModelLibrary;
using CompassModelLibrary.Com;
using CompassModelLibrary.Enums;
using CompassModelLibrary.Interface;
using ModelsTablesDBLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private ComParam standartParam = new ComParam()
        {
            Parity = System.IO.Ports.Parity.None,
            DataBits = 8,
            StopBits = System.IO.Ports.StopBits.One
        };

        private RadantWindow radantWindow;

        Dictionary<string, WPFControlConnection.ConnectionControl> ARDControls;
        private Dictionary<string, bool> IsConnectRadant;

        private CompassDeviceFactory factory;

        private void InitRadant(Languages language)
        {
            radantWindow = new RadantWindow();
            radantWindow.NeedHide += RadantWindow_NeedHide;
            factory = new CompassDeviceFactory(basicProperties.Local);

            radantWindow.RadantObj.Radants = factory.CreateDeviceObject();

            if (basicProperties.Local.ARD1.Device.ToString() == basicProperties.Local.ARD2.Device.ToString() ||
                basicProperties.Local.ARD1.Device.ToString() == basicProperties.Local.ARD3.Device.ToString() ||
                basicProperties.Local.ARD2.Device.ToString() == basicProperties.Local.ARD3.Device.ToString())
            {
                basicProperties.Local.ARD1.Device = DeviceRSD.RRS_PC;
                basicProperties.Local.ARD2.Device = DeviceRSD.RRS_Lincked;
                basicProperties.Local.ARD3.Device = DeviceRSD.LPA_5_10;
            }

            IsConnectRadant = new Dictionary<string, bool>
            {
                {basicProperties.Local.ARD1.Device.ToString(), false },
                {basicProperties.Local.ARD2.Device.ToString(), false },
                {basicProperties.Local.ARD3.Device.ToString(), false }
            };

            ARDControls = new Dictionary<string, WPFControlConnection.ConnectionControl>
            {
                { basicProperties.Local.ARD1.Device.ToString(), ARD1Connection },
                { basicProperties.Local.ARD2.Device.ToString(), ARD2Connection },
                { basicProperties.Local.ARD3.Device.ToString(), ARD3Connection }
            };

            SetLanguageRadant(language);
        }

        private void RadantWindow_NeedHide(object sender, EventArgs e)
        {
            OButton.IsChecked = false;
        }

        private void Radant_Click(object sender, RoutedEventArgs e)
        {
            if (radantWindow.IsVisible)
                radantWindow.Hide();
            else
                radantWindow.Show();
        }

        #region HandlerEvents

        private void Radant_OnWriteByte(object sender, ByteEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var radant = (IComBRD)sender;
                ARDControls[radant.properties.Device.ToString()].ShowWrite();
            }));
        }

        private void Radant_OnReadByte(object sender, ByteEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var radant = (IComBRD)sender;
                ARDControls[radant.properties.Device.ToString()].ShowRead();
            }));
        }

        private void Radant_OnOpenPort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var radant = (IComBRD)sender;
                IsConnectRadant[radant.properties.Device.ToString()] = true;
                ARDControls[radant.properties.Device.ToString()].ShowConnect();
            }));
        }

        private void Radant_OnClosePort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var radant = (IComBRD)sender;
                IsConnectRadant[radant.properties.Device.ToString()] = false;
                ARDControls[radant.properties.Device.ToString()].ShowDisconnect();

            }));
        }

        #endregion

        private void ChangeCourseAngle(float course)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                radantWindow.RadantObj.UpdateCourseAngle(course);                
            }));
        }

        private void ArdOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            TableASP table = lASP.FirstOrDefault(x => x.ISOwn == true);
            if (table == null)
            {
                return;
            }
            var device = (CompassModel) sender;
            switch (device.Name)
            {
                case "RRS_Lincked":
                    table.RRS1 = Convert.ToInt16(device.Direct);
                    break;
                case "RRS_PC":
                    table.RRS2 = Convert.ToInt16(device.Direct);
                    break;
                case "SSTU":
                    table.BPSS = Convert.ToInt16(device.Direct);
                    break;
                case "LPA_5_10":
                    table.LPA510 = Convert.ToInt16(device.Direct);
                    break;
                case "LPA_5_7":
                    table.LPA57 = Convert.ToInt16(device.Direct);
                    break;
                case "LPA_5_9":
                    table.LPA59 = Convert.ToInt16(device.Direct);
                    break;
                case "LPA_10б":
                    table.LPA10 = Convert.ToInt16(device.Direct);
                    break;
            }
            clientDB.Tables[NameTable.TableASP].Change(table);
        }

        private void ARD3Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            this.EditBRDDeviceEventHandlers("ARD3");
        }

        private void ARD2Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            this.EditBRDDeviceEventHandlers("ARD2");
        }
        private void ARD1Connection_ButServerClick(object sender, RoutedEventArgs e)
        {
            this.EditBRDDeviceEventHandlers("ARD1");
        }

        private void EditBRDDeviceEventHandlers(string nameDevice)
        {
            var ard =
                this.radantWindow.RadantObj.Radants.First(radant => radant.NameDevice == nameDevice);
            if (ard == null)
                return;
            if (ard.State)
            {
                ard.Device.ClosePort();
                ard.Device.OnOpenPort -= this.Radant_OnOpenPort;
                ard.Device.OnClosePort -= this.Radant_OnClosePort;
                ard.Device.OnReadByte -= this.Radant_OnReadByte;
                ard.Device.OnWriteByte -= this.Radant_OnWriteByte;
                ard.State = false;
                ard.PropertyChanged -= this.ArdOnPropertyChanged;
            }
            else
            {
                ard.Device.OnOpenPort += this.Radant_OnOpenPort;
                ard.Device.OnClosePort += this.Radant_OnClosePort;
                ard.Device.OnReadByte += this.Radant_OnReadByte;
                ard.Device.OnWriteByte += this.Radant_OnWriteByte;
                ard.Device.OpenPort();
                ard.State = true;
                ard.PropertyChanged += this.ArdOnPropertyChanged;
            }
        }

        public void SetLanguageRadant(ModelsTablesDBLib.Languages language)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                radantWindow?.SetLanguage(language);
            }));
        }
    }
}
