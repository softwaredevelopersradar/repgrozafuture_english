﻿using ControlProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GrozaFuture.AudioPlayer
{
    /// <summary>
    /// Логика взаимодействия для APlayerWindow.xaml
    /// </summary>
    public partial class APlayerWindow : Window
    {
        public event EventHandler HidePlayer = (sender, obj) => { };

        public APlayerWindow()
        {
            InitializeComponent();
            AudioPlayerControl.Expanded += AudioPlayerControl_Expanded;
            AudioPlayerControl.Collapsed += AudioPlayerControl_Collapsed;
        }

        private void AudioPlayerControl_Collapsed(object sender, EventArgs e)
        {
            PlayerWindow.Width = 440;
        }

        private void AudioPlayerControl_Expanded(object sender, EventArgs e)
        {
            PlayerWindow.Width = 640;
        }

        private void WAudioPlayer_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AudioPlayerControl.DisposeWave();
            e.Cancel = true;
            Visibility = Visibility.Hidden;            
        }

        private void PlayerClose_Click(object sender, RoutedEventArgs e)
        {
            AudioPlayerControl.DisposeWave();
            Hide();
            HidePlayer(this, null);            
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
