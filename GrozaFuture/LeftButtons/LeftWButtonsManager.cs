﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AwsToConsoleTransferLib;
using BPSS_DLL;
using ClientDataBase;
using ModelsTablesDBLib;
using SpoofingControl;

namespace GrozaFuture
{
    using GainControl;

    public partial class MainWindow
    {

        private Att att = new Att();
        private Gain gain=new Gain();

        private void InitAtt()
        {
            this.att.OnGainValueChange += Att_OnGainValueChange;
            this.att.OnGainRequest += Att_OnGainRequest;
            att.AttValueChange += Att_AttValueChange;
            att.NeedHide += Att_NeedHide;
            att.NeedGetRequest += Att_NeedGetRequest;
        }
        //отправляет numbSSR(номер ЕПО) серверу и устанавливает уровень усиления по номеру ЕПО
        private async void Att_OnGainRequest(int numbSSR)
        {
            if (att.IsVisible)
            {
                //var answer1 = await dsp.GetAmplifierValues(numbSSR);
                //float fAmplevel = 0;
                //if (answer1?.Header.ErrorCode == 0)
                //{
                //    int Amplevel = answer1.Settings[0];
                //    fAmplevel = Amplevel / 2.0f;
                //}
                var answer2 = await dsp.GetAttenuatorsValues(numbSSR);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }

                att.SetGainValue(fAttlevel, numbSSR);
                //att.InitValues(numbSSR, fAttlevel, fAmplevel);
            }
        }
        //Принисает 
        private async void Att_OnGainValueChange(float curAmp, int numbSSR)
        {
            bool IsConstAttenuatorEnabled = false;
            //if (curAmp < 10)
            //{
            //    IsConstAttenuatorEnabled = false;
            //}
            //if (curAmp == 10)
            //{
            //    curAmp = 0;
            //    IsConstAttenuatorEnabled = true;
            //}
            //if (curAmp > 10)
            //{
            //    curAmp = curAmp - 10;
            //    IsConstAttenuatorEnabled = true;
            //}
            var answer = await dsp.SetAttenuatorsValue(numbSSR, (float)curAmp, IsConstAttenuatorEnabled);


            /////////////////////////////////////////////////////////////////////////////////
            if (answer?.Header.ErrorCode == 0)
            {
                var answer2 = await dsp.GetAttenuatorsValues(numbSSR);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }

                att.SetGainValue(fAttlevel, numbSSR);
            /////////////////////////////////////////////////////////////////////////////////
            }
        }

        private void Att_NeedHide(object sender)
        {
            AToggleButton.IsChecked = false;
        }

        private async void Att_AttValueChange(object sender, int BandBumber, double AttValue)
        {
            bool IsConstAttenuatorEnabled = false;
            if (AttValue < 10)
            {
                IsConstAttenuatorEnabled = false;
            }
            if (AttValue == 10)
            {
                AttValue = 0;
                IsConstAttenuatorEnabled = true;
            }
            if (AttValue > 10)
            {
                AttValue = AttValue - 10;
                IsConstAttenuatorEnabled = true;
            }
            var asnwer = await dsp.SetAttenuatorsValue(BandBumber, (float)AttValue, IsConstAttenuatorEnabled);
        }

        private async void PLibrary_NbarIndex(object sender, int index)
        {
            if (att.IsVisible)
            {
                var answer1 = await dsp.GetAmplifierValues(index);
                float fAmplevel = 0;
                if (answer1?.Header.ErrorCode == 0)
                {
                    int Amplevel = answer1.Settings[0];
                    fAmplevel = Amplevel / 2.0f;
                }
                var answer2 = await dsp.GetAttenuatorsValues(index);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }
                att.InitValues(index, fAttlevel, fAmplevel);
            }
        }

        private async void Att_NeedGetRequest(object sender, int BandBumber)
        {
            if (att.IsVisible)
            {
                var answer1 = await dsp.GetAmplifierValues(BandBumber);
                float fAmplevel = 0;
                if (answer1?.Header.ErrorCode == 0)
                {
                    int Amplevel = answer1.Settings[0];
                    fAmplevel = Amplevel / 2.0f;
                }
                var answer2 = await dsp.GetAttenuatorsValues(BandBumber);
                float fAttlevel = 0;
                if (answer2?.Header.ErrorCode == 0)
                {
                    var Attlevel = answer2.Settings[0].AttenuatorValue;
                    fAttlevel = Attlevel / 2.0f;
                    var cAtt = Convert.ToBoolean(answer2.Settings[0].IsConstAttenuatorEnabled);
                    fAttlevel += (cAtt) ? 10 : 0;
                }
                att.InitValues(BandBumber, fAttlevel, fAmplevel);
            }
        }

        private void AToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (pLibrary.Mode != 2)
            {
                AToggleButton.IsChecked = !AToggleButton.IsChecked;
                if (basicProperties.Local.Common.Language.ToString() == "Rus")
                    MessageBox.Show("Управление усилителями доступно только в режиме Радиоразведка с пеленгованием!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                else
                    MessageBox.Show("Control of amplifiers is available only in the Radio intelligence mode with direction finding!", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (AToggleButton.IsChecked.Value)
                att.Show();
            else
                att.Hide();
        }

        public void AttCheck(DspDataModel.Tasks.DspServerMode Mode)
        {
            if (att.IsVisible == true && Mode != DspDataModel.Tasks.DspServerMode.RadioIntelligenceWithDf)
            {
                AToggleButton.IsChecked = false;
                att.Hide();
            }
        }

        public void AttCheck(int Mode)
        {
            if (att.IsVisible == true && Mode != 2)
            {
                AToggleButton.IsChecked = false;
                att.Hide();
            }
        }        

        private void SpoofToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (SpoofToggleButton.IsChecked.Value)
            {
                spoof.Show();

                var getParams = spoof.GetSpoofingParameters();
                var getCoordinates = spoof.GetStateCoordinates();

                var spoofParams = new ControlParameters((short)getParams.Altitude,
                                            (int)getParams.Speed,
                                            (short)getParams.Direction,
                                            CodeRange(getParams.Range));

                var spoofCoord = new CoordinatesParameters(getCoordinates.Latitude, getCoordinates.Longitude);

                SetCoordinates(spoofCoord);
                SetControlParams(spoofParams);


                List<StateNAVControl.StateModel> Params = GenerateDefaultBPSSParams();
                List<StateNAVControl.Led> errors = new List<StateNAVControl.Led>() { StateNAVControl.Led.Red }
                ;
                DispatchIfNecessary(() =>
                {

                    //spoof.SetBPSSParams(Params, StateNAVControl.Type.SPUF, errors, checkedLiters);
                });

                //Запрос состояния
                //bpss.SendStatus(0);

                TumanConnection.ShowWrite();

                //Запрос состояния через 5секунд
                Task.Run(() => BppsPoolingAsync5s());
            }
            else
            {
                spoof.Hide();
            }
        }

        private void SpoofingConnection_Click(object sender, RoutedEventArgs e)
        {
            if (!IsSpoofConnected)
            {
                //connect
                SpoofManagerConnect(basicProperties.Local.Spoofing.Port, basicProperties.Local.Spoofing.IpAddress);

                //Запрос состояния антенны
                bpss.SendStatusAntenna();
            }
            else
            {
                //disconnect
                SpoofManagerDisconnect();
            }
        }
    }
}
