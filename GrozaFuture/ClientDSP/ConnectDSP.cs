﻿using ModelsTablesDBLib;
using Protocols;
using System;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        public async void ConnectDSP(string IP, int port)
        {
            await dsp.ConnectToBearingDSP(IP, port);
        }

        public void DisconnectDSP()
        {
            dsp.DisconnectFromBearingDSP();
        }

        private bool isDSPConnected = false;

        partial void Dsp_IsConnected(bool isConnected)
        {
            DispatchIfNecessary(() =>
            {
                IsDspConnected = isConnected;
                isDSPConnected = isConnected;
                if (isConnected) ServerControlConnection.ShowConnect();
                else ServerControlConnection.ShowDisconnect();

                if (isConnected) USRPControlConnection.ShowConnect();
                else USRPControlConnection.ShowDisconnect();

                if (isConnected) GNSSControlConnection.ShowConnect();
                else GNSSControlConnection.ShowDisconnect();

                if (isConnected == false) FHSControlConnection.ShowDisconnect();

                ConnectionToOmni(isConnected);
            });
        }

        partial void Dsp_IsRead(bool isRead)
        {
            DispatchIfNecessary(() =>
            {
                ServerControlConnection.ShowRead();
            });
        }

        partial void Dsp_IsWrite(bool isWrite)
        {
            DispatchIfNecessary(() =>
            {
                ServerControlConnection.ShowWrite();
            });
        }

        partial void Dsp_ShaperStateUpdate(ShaperStateUpdateEvent answer)
        {
            DispatchIfNecessary(() =>
            {
                if (answer.IsShaperConnected)
                    FHSControlConnection.ShowConnect();
                else
                    FHSControlConnection.ButServerColor = FHSControlConnection.ColorButNone;
            });
        }

        private void ServerConnection_Click(object sender, RoutedEventArgs e)
        {
            if (!isDSPConnected)
            {
                DispatchIfNecessary(async () =>
                {
                    ServerControlConnection.IsEnabled = false;
                    var IPAddress = basicProperties.Local.DspServer.IpAddress;
                    var Port = basicProperties.Local.DspServer.Port;
                    //await dsp.ConnectToBearingDSP(IPAddress, Port);
                    var answer = await dsp.IQConnectToServer(IPAddress, Port);
                    if (answer != "OK")
                    {
                        switch (basicProperties.Local.Common.Language)
                        {
                            case ModelsTablesDBLib.Languages.Eng:
                                MessageBox.Show(answer, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                                break;
                            case ModelsTablesDBLib.Languages.Rus:
                                MessageBox.Show("Неверный IP-адрес или Сервер не найден", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                                break;
                            default:
                                MessageBox.Show(answer, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                                break;
                        }
                    }
                    ServerControlConnection.IsEnabled = true;

                    //Запросить режим
                    var answerMode = await dsp.GetMode();
                    if (answerMode?.Header.ErrorCode == 0)
                    {
                        ModeToMainPanelHighlight((byte)answerMode.Mode);
                        pLibrary.Mode = (byte)answerMode.Mode;
                        if (pLibrary.Mode == 1)
                        {
                            BearingToggleButton.IsChecked = false;
                            basicProperties.Global.TypeRadioRecon = ModelsTablesDBLib.EnumTypeRadioRecon.WithoutBearing;
                        }
                        if (pLibrary.Mode == 2)
                        {
                            BearingToggleButton.IsChecked = true;
                            basicProperties.Global.TypeRadioRecon = ModelsTablesDBLib.EnumTypeRadioRecon.Bearing;
                        }
                        //if (pLibrary.Mode == 3 || pLibrary.Mode == 4 || pLibrary.Mode == 5 || pLibrary.Mode == 6)
                        //{
                        //    ucSuppressFWS.ToggleButtonEnable();
                        //}
                        AttCheck(answerMode.Mode);
                        InitSave();
                    }

                    //Запросить порог
                    var answerThreshold = await dsp.GetFilters();
                    if (answerThreshold?.Header.ErrorCode == 0)
                    {
                        pLibrary.Threshold = (answerThreshold.Threshold < 0) ? (-1) * answerThreshold.Threshold : answerThreshold.Threshold;
                    }

                    //Запросить состояние поиска ППРЧ
                    var answerSearchFHSS = await dsp.GetSearchFHSS();
                    if (answerThreshold?.Header.ErrorCode == 0)
                    {
                        switch (answerSearchFHSS.SearchFhss)
                        {
                            case 0:
                                basicProperties.Global.DetectionFHSS = 0;
                                break;
                            case 1:
                                basicProperties.Global.DetectionFHSS = 1;
                                break;
                            case 255:
                                basicProperties.Global.DetectionFHSS = 0;
                                break;
                            default:
                                break;
                        }
                    }

                    try
                    {
                        clientDB?.Tables[NameTable.GlobalProperties].Add(basicProperties.Global);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Source + ex.Message.ToString());
                    }
                });
            }
            else
            {
                DisconnectDSP();
                pLibrary.Mode = 0;
                AttCheck(0);
            }
        }
    }
}
