﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        // BANK FOR CRRX_1
        public int CurrentDataBank
        {
            get => basicProperties.Local.ARONE1.CurrentDataBank;
            set 
            {
                var type = basicProperties.Local.ARONE1.Type.ToString();
                switch (type) 
                {
                    case "AR_6000":
                        ReceiverTableFirst.Bank = value;
                        break;
                    case "AR_ONE":
                        ReceiverTable.Bank = value;
                        break;
                    default:
                        break;
                }
            }
        }

        private void ReceiverTableFirst_OnUpdateBank()
        {
            ReceiverTableFirst.Bank = CurrentDataBank;
        }

        public void OnUpdateBank()
        {
            ReceiverTable.Bank = CurrentDataBank;
        }


        //BANK FOR CRRX_2

        public int CurrentDataBankSec 
        {
            get => basicProperties.Local.ARONE2.CurrentDataBank;
            set 
            {
                var type = basicProperties.Local.ARONE2.CurrentDataBank.ToString();
                switch (type)
                {
                    case "AR_6000":
                        ReceiverTableSec.Bank = value;
                        break;
                    case "AR_ONE":
                        ReceiverTableArOneSec.Bank = value;
                        break;
                    default:
                        break;
                }
            }
        }

        private void ReceiverTableSec_OnUpdateBank()
        {
            ReceiverTableSec.Bank = CurrentDataBankSec;
        }

        private void ReceiverTableArOneSec_OnUpdate()
        {
            ReceiverTableArOneSec.Bank = CurrentDataBankSec;
        }
    }
}
