﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using CoopProtocolMCS1MCS2;
using ModelsTablesDBLib;

namespace GrozaFuture
{
    public partial class MainWindow
    {
        static public TCPServer ServerHS;

        void initServerHS()
        {
            ServerHS = new TCPServer(1, 2);

            ServerHS.OnCreate += new TCPServer.ConnectEventHandler(CreateDerverHS);
            ServerHS.OnReadByte += new TCPServer.ByteEventHandler(ReadByteHS);
            ServerHS.OnWriteByte += new TCPServer.ByteEventHandler(WriteByteHS);
            ServerHS.OnConnect += new TCPServer.ConnectEventHandler(ConnectClientHS);
            ServerHS.OnDisconnect += new TCPServer.ConnectEventHandler(DisconnectClientHS);
            ServerHS.OnDestroy += new TCPServer.ConnectEventHandler(DestroyServerHS);

            //TO DO: смс-ки от Апп-2
            ServerHS.OnReceiveTextCmd += new CoopProtocolMCS1MCS2.TCPServer.CmdTextEventHandler(ReceiveTextCmdHS);

            ServerHS.OnReceiveFriequenciesForSuppressionCmd += ServerHS_OnReceiveFriequenciesForSuppressionCmd;
            ServerHS.OnReceiveExecuteDFCmd += ServerHS_OnReceiveExecuteDFCmd;
            ServerHS.OnReceiveCoordRequesCmd += ServerHS_OnReceiveCoordRequesCmd;

            //TO DO: Старт Сервера в зависимости от Локал Пропертиес
            switch (basicProperties.Local.AP_2Properties.TypeOfConection)
            {
                case TypeOfConectionAP_2.PPC:
                    ServerHS.CreateServer(basicProperties.Local.AP_2Properties.IpAddressPPC, basicProperties.Local.AP_2Properties.PortPPC);
                    break;
                case TypeOfConectionAP_2._3G:
                    ServerHS.CreateServer(basicProperties.Local.AP_2Properties.IpAddress3G4GRouter, basicProperties.Local.AP_2Properties.Port3G4GRouter);
                    break;
            }
        }

        void deinitServerHS()
        {
            ServerHS.OnCreate -= new TCPServer.ConnectEventHandler(CreateDerverHS);
            ServerHS.OnReadByte -= new TCPServer.ByteEventHandler(ReadByteHS);
            ServerHS.OnWriteByte -= new TCPServer.ByteEventHandler(WriteByteHS);
            ServerHS.OnConnect -= new TCPServer.ConnectEventHandler(ConnectClientHS);
            ServerHS.OnDisconnect -= new TCPServer.ConnectEventHandler(DisconnectClientHS);
            ServerHS.OnDestroy -= new TCPServer.ConnectEventHandler(DestroyServerHS);

            //TO DO: смс-ки от Апп-2
            ServerHS.OnReceiveTextCmd -= new CoopProtocolMCS1MCS2.TCPServer.CmdTextEventHandler(ReceiveTextCmdHS);

            ServerHS.OnReceiveFriequenciesForSuppressionCmd -= ServerHS_OnReceiveFriequenciesForSuppressionCmd;
            ServerHS.OnReceiveExecuteDFCmd -= ServerHS_OnReceiveExecuteDFCmd;
            ServerHS.OnReceiveCoordRequesCmd -= ServerHS_OnReceiveCoordRequesCmd;
        }

        void ServerHS_OnReceiveCoordRequesCmd()
        {
            //Добыть координаты из листа ASP
            //Брать только координаты своей станции
            var OwnStation = lASP.Where(x => x.ISOwn == true).FirstOrDefault();

            ServerHS.SendAnsweringCmdWithCoord(OwnStation.Coordinates.Latitude, OwnStation.Coordinates.Longitude);
        }

        async void ServerHS_OnReceiveExecuteDFCmd(int fr1, int fr2)
        {
            var answer = await dsp.ExecutiveDfRequest(fr1, fr2, 3);
            if (answer != null)
                if (answer.Header.ErrorCode == 0)
                    ServerHS.SendAnsweringCmdWithRecivedBearing((int)Math.Round(answer.Direction / 10.0d), (short)Math.Round(answer.StandardDeviation / 10.0d));
                else
                    ServerHS.SendAnsweringCmdWithRecivedBearing(361, (short)Math.Round(answer.StandardDeviation / 10.0d));
            else
                ServerHS.SendAnsweringCmdWithRecivedBearing(361, 361);
        }

        byte LetterFindHS(int iFreq)
        {
            byte bLetter = 10;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_1 & iFreq < RangesLetters.FREQ_START_LETTER_2)
                bLetter = 1;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_2 & iFreq < RangesLetters.FREQ_START_LETTER_3)
                bLetter = 2;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_3 & iFreq < RangesLetters.FREQ_START_LETTER_4)
                bLetter = 3;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_4 & iFreq < RangesLetters.FREQ_START_LETTER_5)
                bLetter = 4;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_5 & iFreq < RangesLetters.FREQ_START_LETTER_6)
                bLetter = 5;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_6 & iFreq < RangesLetters.FREQ_START_LETTER_7)
                bLetter = 6;
            if (iFreq >= RangesLetters.FREQ_START_LETTER_7 & iFreq < RangesLetters.FREQ_START_LETTER_8)
                bLetter = 7;
            return bLetter;
        }

        void ServerHS_OnReceiveFriequenciesForSuppressionCmd(int iDuration, StructSupprFWS[] tSupprFWS)
        {
            try
            {
                //TO DO: iDuration

                for (int i = 0; i < tSupprFWS.Length; i++)
                {
                    TableSuppressFWS tableSuppressFWS = new TableSuppressFWS
                    {
                        Id = 0,
                        Sender = SignSender.PC,
                        NumberASP = 0,
                        FreqKHz = tSupprFWS[i].iFreq,
                        Bearing = tSupprFWS[i].sBearing,
                        Letter = TableOperations.DefinitionParams.DefineLetter(tSupprFWS[i].iFreq),
                        Threshold = (short)(tSupprFWS[i].bThreshold * -1),
                        Priority = tSupprFWS[i].bPrioritet,
                        Coordinates = new Coord
                        {
                            Latitude = -1,
                            Longitude = -1,
                            Altitude = -1
                        },
                        InterferenceParam = new InterferenceParam
                        {
                            Manipulation = tSupprFWS[i].bManipulation,
                            Modulation = tSupprFWS[i].bModulation,
                            Deviation = tSupprFWS[i].bDeviation,
                            Duration = tSupprFWS[i].bDuration //4
                        },
                    };

                    UcTemsFWS_OnAddFWS_RS(this, tableSuppressFWS);
                }
            }
            catch (SystemException) { }

            ServerHS.SendConfirmFriequenciesForSuppression(0);
        }

        void CreateDerverHS()
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowConnect();
            });
        }
        void ConnectClientHS()
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowConnectClient();
            });
        }
        void DisconnectClientHS()
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowDisconnectClient();
            });
        }
        void DestroyServerHS()
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowDisconnectClient();
                Ap2ControlConnection.ShowDisconnect();
            });
        }
        void ReadByteHS(byte[] mass)
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowRead();
            });
        }
        void WriteByteHS(byte[] mass)
        {
            DispatchIfNecessary(() =>
            {
                Ap2ControlConnection.ShowWrite();
            });
        }

        void ReceiveTextCmdHS(string strText)
        {
            List<UserControl_Chat.Message> messages = new List<UserControl_Chat.Message>();

            var linked = lASP.Where(x => x.ISOwn == false).FirstOrDefault();

            UserControl_Chat.Message message = new UserControl_Chat.Message()
            {
                Id = (linked == null) ? 0 : linked.Id,
                MessageFiled = strText,
                IsSendByMe = UserControl_Chat.Roles.Received,
                IsTransmited = true,
            };
            messages.Add(message);
            DispatchIfNecessary(() =>
            {
                newWindow.curChat.DrawMessageToChat(messages);
            });

            ServerHS.SendConfirmText(0);
        }

        private void Ap2Connection_Click(object sender, RoutedEventArgs e)
        {
            if (ServerHS != null)
            {
                ServerHS.DestroyServer();
                deinitServerHS();
                ServerHS = null;
            }
            else
            {
                initServerHS();
            }
        }
    }
}
