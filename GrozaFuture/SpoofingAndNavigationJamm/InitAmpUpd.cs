﻿using SHS_DLL;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow : Window
    {
        ComSHS comAmp;

        private void ConnectAmp()
        {
            try
            {
                _waitAnswerAmp = false;


                if (comAmp != null)
                    DisconnectAmp();

                comAmp = new ComSHS(4, 5);

                comAmp.OnConnect += ComAmp_OnConnect;
                comAmp.OnDisconnect += ComAmp_OnDisconnect;


                comAmp.OnConfirmSet += ComAmp_OnConfirmSet;
                comAmp.OnConfirmGetParam += ComAmp_OnConfirmGetParam;
                comAmp.OnConfirmStatus += ComAmp_OnConfirmStatus;
                comAmp.OnConfirmStatusAntenna += ComAmp_OnConfirmStatusAntenna;

                comAmp.OpenPort(basicProperties.Local.Tuman.ComPort,
                                basicProperties.Local.Tuman.PortSpeed,
                                System.IO.Ports.Parity.None,
                                8,
                                System.IO.Ports.StopBits.One);
            }

            catch { }
        }


        private void DisconnectAmp()
        {
            try
            {
                _waitAnswerAmp = false;

                if (comAmp != null)
                {
                    comAmp.ClosePort();

                    comAmp.OnConnect -= ComAmp_OnConnect;
                    comAmp.OnDisconnect -= ComAmp_OnDisconnect;


                    comAmp.OnConfirmSet -= ComAmp_OnConfirmSet;
                    comAmp.OnConfirmGetParam -= ComAmp_OnConfirmGetParam;
                    comAmp.OnConfirmStatus -= ComAmp_OnConfirmStatus;
                    comAmp.OnConfirmStatusAntenna -= ComAmp_OnConfirmStatusAntenna;


                    comAmp = null;
                }
            }

            catch { }
        }
    }
}
