﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GrozaFuture
{
    public partial class MainWindow
    {

        private void ReInitPostControl(LocalProperties arg, bool isManual = false)
        {
            switch (arg.PCProperties.Type)
            {
                case TypePC.Garant:
                    PCControlConnection.LEDClientVisible = false;
                    //if (_isBerezinaConnected)
                    //    DeInitIExGRZ_BRZ();
                    //if (_isRossConnected)
                    //    ShutDownRoss();
                    ReInitAPD(arg, isManual);
                    break;
                case TypePC.Berezina:
                    PCControlConnection.LEDClientVisible = false;
                    //if (_isADPConnected)
                    //    DeInitAPD();
                    //if (_isRossConnected)
                    //    ShutDownRoss();
                    ReInitBRZ(arg, isManual);
                    break;

                
                case TypePC.Ross:
                    PCControlConnection.LEDClientVisible = true;
                    //if (_isADPConnected)
                    //    DeInitAPD();
                    //if (_isBerezinaConnected)
                    //    DeInitIExGRZ_BRZ();
                    InitializeRoss(arg);
                    break;
                default:
                    break;
                    

            }
        }

        private void PCConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_isADPConnected)
                    DeInitAPD();
                else if (_isRossConnected)
                    ShutDownRoss();
                else if (_isBerezinaConnected)
                    DeInitIExGRZ_BRZ();
                else
                {
                    ReInitPostControl(basicProperties.Local, true);
                }
            }
            catch (ClientDataBase.Exceptions.ExceptionClient exceptClient)
            {
                MessageBox.Show(exceptClient.Message);
            }
        }
    }
}
